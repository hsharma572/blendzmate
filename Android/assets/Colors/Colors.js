const Colors = {
    PrimaryBackGroundColor:'#F7F7F7',
    InactiveTintColor:'#66757f',
    ActiveTintColor:'#953594',
    white:'white',
    ButtonColor:'#45b7dd'
}

export default Colors;