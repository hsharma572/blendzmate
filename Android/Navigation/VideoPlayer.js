import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import Video from 'react-native-video';
import Orientation from 'react-native-orientation-locker';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import PlayerControls from './videoCompoents/PlayerControls';
import ProgressBar from './videoCompoents/ProgressBar';


export default function VideoPlayer (props) {
  const videoRef = React.createRef();
console.log(props.navigation.getParam('data', ''),'video');
  const [state, setState] = useState({
    fullscreen: false,
    play: true,
    currentTime: 0,
    duration: 0,
    showControls: true,
    URl:props.navigation.getParam('data', '').data
  });

  useEffect(() => {
    Orientation.addOrientationListener(handleOrientation);
    return () => {
      Orientation.unlockAllOrientations()
      Orientation.removeOrientationListener(handleOrientation);

    };
  }, []);

  return (
    <View style={state.fullscreen ? styles.fullscreenVideoCont :styles.container}>
      <TouchableWithoutFeedback onPress={showControls}>
        <View>
          <Video
            ref={videoRef}
            source={{
              uri:state.URl,
            }}
            style={state.fullscreen ? styles.fullscreenVideo : styles.video}
            controls={false}
            resizeMode={'contain'}
            onLoad={onLoadEnd}
            onProgress={onProgress}
            maxBitRate={1000000} 
            onEnd={onEnd}
            paused={!state.play}
          />
          {state.showControls && (
            <View style={styles.controlOverlay}>
            <View style={{  flex: 1,
    flexDirection: 'row',
    marginTop:10,
   justifyContent:'space-between'}}>
            <TouchableOpacity
                onPress={handleBack}
                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                style={styles.BackBtton}>
                <MaterialCommunityIcons name="keyboard-backspace" size={20} color="white" />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleFullscreen}
                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                style={styles.fullscreenButton}>
                {!state.fullscreen ? <MaterialCommunityIcons name="phone-rotate-landscape" size={20} color="white" /> :   <MaterialCommunityIcons name="phone-rotate-portrait" size={20} color="white" />}
              </TouchableOpacity>
              </View>
              <PlayerControls
                onPlay={handlePlayPause}
                onPause={handlePlayPause}
                playing={state.play}
                showPreviousAndNext={false}
                showSkip={true}
                skipBackwards={skipBackward}
                skipForwards={skipForward}
              />
              <ProgressBar
                currentTime={state.currentTime}
                duration={state.duration > 0 ? state.duration : 0}
                onSlideStart={handlePlayPause}
                onSlideComplete={handlePlayPause}
                onSlideCapture={onSeek}
              />
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    </View>
  );

  function handleOrientation(orientation) {
    orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT'
      ? (setState(s => ({...s, fullscreen: true})), StatusBar.setHidden(true))
      : (setState(s => ({...s, fullscreen: false})),
        StatusBar.setHidden(false));
  }

  function handleFullscreen() {
    state.fullscreen
      ? Orientation.unlockAllOrientations()
      : Orientation.lockToLandscapeLeft();
      
  }
  function handleBack(){
    props.navigation.goBack();
  }
  function handlePlayPause() {
    // If playing, pause and show controls immediately.
    if (state.play) {
      setState({...state, play: false, showControls: true});
      return;
    }

    setState({...state, play: true});
    setTimeout(() => setState(s => ({...s, showControls: false})), 2000);
  }

  function skipBackward() {
    videoRef.current.seek(state.currentTime - 10);
    setState({...state, currentTime: state.currentTime - 10});
  }

  function skipForward() {

    videoRef.current.seek(state.currentTime + 10);
    setState({...state, currentTime: state.currentTime + 10});
  }

  function onSeek(data) {
    videoRef.current.seek(data.seekTime);
    setState({...state, currentTime: data.seekTime});
  }

  function onLoadEnd(data) {
    setState(s => ({
      ...s,
      duration: data.duration,
      currentTime: data.currentTime,
    }));
  }

  function onProgress(data) {
    setState(s => ({
      ...s,
      currentTime: data.currentTime,
    }));
  }

  function onEnd() {
    setState({...state, play: false});
    videoRef.current.seek(0);
  }

  function showControls() {
    state.showControls
      ? setState({...state, showControls: false})
      : setState({...state, showControls: true});
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0,0.9)',
    justifyContent:'center'
  },
  video: {
    height: Dimensions.get('window').width * (9 / 16),
    width: Dimensions.get('window').width,
    backgroundColor: 'black',
  },
  fullscreenVideoCont:{
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    backgroundColor:'black'
  },
  fullscreenVideo: {
     
    height: Dimensions.get('window').width,
    width: Dimensions.get('window').height,
   
  },
  text: {
    marginTop: 30,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: 'justify',
  },
  BackBtton:{
   
    paddingLeft: 10,
  },
  fullscreenButton: {
    paddingRight: 10,
  },
  controlOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000000c4',
    justifyContent: 'space-between',
  },
});