import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, Switch } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import RadioForm from 'react-native-simple-radio-button';

var Profile_item = [
    { label: 'Everyone', value: 'Everyone' },
    { label: 'Followers', value: 'Followers' },
    { label: 'Nobody', value: 'Nobody' }
];

var DirectMessage_item = [
    { label: 'Everyone', value: 'Everyone' },
    { label: 'People I Follow', value: 'People I Follow' }
];



export default class Privacy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: false,
            ViewProfile: '',
            ViewProfileView: false,
            DirectMessage: '',
            DirectMessageView: false
        };
    }

    render() {
        const { Label, TxtInpt } = Styles;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20 }}>
                <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}> Account privacy setting </Text>
                <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                    <Text style={Label}>Who can view your profile?</Text>

                    <TouchableOpacity onPress={() => this.setState({ ViewProfileView: true })}>
                        <TextInput
                            value={this.state.ViewProfile}
                            editable={false}
                            placeholder="Everyone"
                            underlineColorAndroid="transparent"
                            style={TxtInpt}
                        />
                    </TouchableOpacity>
                    {
                        this.state.ViewProfileView
                            ?
                            <View style={{ marginTop: 10, width: '100%', backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={Profile_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ ViewProfile: value }, () => { setTimeout(() => { this.setState({ ViewProfileView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Who can direct message you?</Text>

                    <TouchableOpacity onPress={()=>this.setState({ DirectMessageView: true })}>
                        <TextInput
                            value={this.state.DirectMessage}
                            editable={false}
                            placeholder="Everyone"
                            underlineColorAndroid="transparent"
                            style={TxtInpt}
                        />
                    </TouchableOpacity>
                    {
                        this.state.DirectMessageView
                            ?
                            <View style={{ marginTop: 10, width: '100%', backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={DirectMessage_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ DirectMessage: value }, () => { setTimeout(() => { this.setState({ DirectMessageView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label, { marginTop: 15 }]}>Show your profile in search engines</Text>
                        <Switch
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }], marginTop: 15, marginLeft: 10 }}
                            value={this.state.value}
                            onValueChange={value => this.setState({ value: value })} />
                    </View>

                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, borderWidth: 1, borderColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
