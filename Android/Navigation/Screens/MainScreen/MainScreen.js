import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, StatusBar } from 'react-native';
import TopTabBar from '../../Component/TopTabBar';
import Colors from '../../../assets/Colors/Colors';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Styles from './style';
import PubSub from 'pubsub-js';
import SearchUserScreen from '../SearchUserScreen/SearchUserScreen';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import SearchUserWebView from '../SearchUserWebView/SearchUserWebView';
import {NavigationConstants} from '../../commonValues';
import url from '../../Component/url';
import IconBadge from 'react-native-icon-badge';

class MainScreen extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      SearchKeyWord: '',
      resultData:'',
    };
  }

  componentDidMount() {
    PubSub.subscribe('_goToCommentScreen', (msg, data) => {
      this.props.navigation.navigate('CommentScreen', { data: data })
    })
    PubSub.subscribe('_goToVideo', (msg, data) => {
      this.props.navigation.navigate('Video', { data: data })
    })
    PubSub.subscribe('_goToLoginScreen', (msg, data) => {
      this.props.navigation.navigate('loginStack')
    })
    PubSub.subscribe('AllSuggestion', () => {
      this.props.navigation.navigate('AllSuggestion')
    })
    PubSub.subscribe('OpenStories', (msg, data) => {
      this.props.navigation.navigate('StoryDetail', { data: data.data })
    })

    PubSub.subscribe('GoToEdit', (msg, data) => {
      this.props.navigation.navigate('EditPost', { data: data })
    })

    this.getNotification();
    this._interval = setInterval(() => {
      this.getNotification();
    }, 3000);

  }
  getNotification=()=>{
    let time=new Date().getTime();
    console.log('returnData', time,NavigationConstants.accessToken);
    fetch(`${url.url2}/aj/main/update-data?hash=hshshs&notifications=1&new_messages=1&stories=0&chats=1&_=${time}`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${NavigationConstants.accessToken}&server_key=${url.server_key}`
  })
      .then((response) => response.json())
      .then((responseJson) => {
         
          this.setState({ resultData: responseJson })
      })
      .catch((error) => {
          
      })

  }



  componentWillUnmount() {
    clearInterval(this._interval);
    PubSub.unsubscribe('_goToCommentScreen')
    PubSub.unsubscribe('_goToVideo')
    PubSub.unsubscribe('_goToLoginScreen')
    PubSub.unsubscribe('AllSuggestion')
    PubSub.unsubscribe('OpenStories')
    PubSub.unsubscribe('GoToEdit')

    // PubSub.unsubscribe('GoToProperties')
  }

  render() {
    console.log('helllllllooooo')
    const { serachView, topView, topFirstView, ImageSearchView, searchInput } = Styles;
    return (
      <View style={{ flex: 1, backgroundColor: Colors.PrimaryBackGroundColor }}>
        <View style={topView}>
          <View style={topFirstView}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
            <View style={ImageSearchView}>
              <View style={{ height: 30, width: 30, backgroundColor: 'white' }}>
                {/* <Image source={{uri:'http://blendsmate-com.stackstaging.com/media/img/logo.jpg'}} style={{height:'100%',width:'100%'}}/> */}
              </View>
              <View style={serachView}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('SearchUserScreen', { user: this.state.SearchKeyWord })}>
                  <Econ name="search" size={14} color={Colors.InactiveTintColor} />
                </TouchableOpacity>
                <TextInput style={searchInput}
                  placeholder="Search.."
                  onChangeText={(text) => this.setState({ SearchKeyWord: text })}
                  placeholderTextColor={Colors.InactiveTintColor}
                  onSubmitEditing={() => this.props.navigation.navigate('SearchUserScreen', { user: this.state.SearchKeyWord })}
                />
              </View>
            </View>
            <View>
            </View>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')}>

                              <IconBadge
            MainElement={
              <Econ name="bell" size={18} color={Colors.InactiveTintColor} />
            }
            BadgeElement={
              <Text style={{color:'#FFFFFF',fontSize:8}}>{this.state.resultData.notif}</Text>
            }
            IconBadgeStyle={
              {width:7,
              height:12,
              top:-5,
              left:8,
              borderRadius:10,
              backgroundColor: '#f72d2d'}
            }

            Hidden={this.state.resultData.notif==0}
            />
            </TouchableOpacity>
          </View>
        </View>
        <TopTabBar  screenProps={this.state.resultData} />
      </View>
    );
  }
}
//<TopTabBar  screenProps={this.props.navigation.getParam('data')} />

const MainScreenStack = createStackNavigator({
  SearchUserScreen: SearchUserScreen,
  MainScreen: MainScreen,
  SearchUserWebView:SearchUserWebView
}, {
  initialRouteName: 'MainScreen',
  headerMode: 'none'
})

export default createAppContainer(MainScreenStack)
