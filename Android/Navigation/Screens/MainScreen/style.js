import Colors from '../../../assets/Colors/Colors';
import { width, height, totalSize } from 'react-native-dimension';
const Styles = {
    serachView:{
        height: height(5),
        alignItems: 'center',
        borderRadius: height(5),
        width: width(70),
        backgroundColor: Colors.PrimaryBackGroundColor,
        marginLeft: 10,
        paddingHorizontal: 10,
        flexDirection: 'row'
    },
    searchInput:{ height: 30, width: width(68), fontSize: 9, paddingVertical: 5, marginLeft: 5, fontFamily: 'OpenSans-Bold' },
    topView:{
        height: height(7), 
        paddingHorizontal: 10, 
        paddingVertical: 5, 
        backgroundColor: Colors.white
    },
    topFirstView:{
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        marginRight: 10
    },
    ImageSearchView:{
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        width: 100 
    }
}

export default Styles;
