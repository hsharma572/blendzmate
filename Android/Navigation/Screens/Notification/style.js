const Styles = {
    Label:{
        fontSize: 11,
        color:'#333',
        fontFamily: 'OpenSans-Regular',
    },
    TxtInpt:{
        height:35,
        width:'100%',
        fontSize:11,
        fontFamily:'OpenSans-Regular',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
    }
}

export default Styles;