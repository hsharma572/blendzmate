import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, Switch } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';

export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            liked:'',
            commented:'',
            replied:'',
            likedComment:'',
            followed:'',
            mentioned:''
        };
    }

    render() {
        const { Label, TxtInpt } = Styles;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20 }}>
                <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}> Notification settings </Text>
                <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                    <Text style={[Label, { fontFamily: 'OpenSans-Bold' }]}>Receive notifications when some one :</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label, { marginTop: 10 }]}>Liked my post</Text>
                        <Switch
                             style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }],marginTop: 10, marginLeft: 150 }}
                            value={this.state.liked}
                            onValueChange={value => this.setState({ liked: value })} />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label]}>Commented on my post</Text>
                        <Switch
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }], marginLeft: 95 }}
                            value={this.state.commented}
                            onValueChange={value => this.setState({ commented: value })} />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label]}>replied to my comment</Text>
                        <Switch
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }], marginLeft: 99 }}
                            value={this.state.replied}
                            onValueChange={value => this.setState({ replied: value })} />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label]}>liked my comment</Text>
                        <Switch
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }], marginLeft: 124 }}
                            value={this.state.likedComment}
                            onValueChange={value => this.setState({ likedComment: value })} />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label]}>Followed me</Text>
                        <Switch
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }], marginLeft: 154 }}
                            value={this.state.followed}
                            onValueChange={value => this.setState({ followed: value })} />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[Label]}>Mentioned me</Text>
                        <Switch
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }], marginLeft: 144 }}
                            value={this.state.mentioned}
                            onValueChange={value => this.setState({ mentioned: value })} />
                    </View>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, borderWidth: 1, borderColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
