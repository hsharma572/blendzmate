import React, { Component } from 'react';
import { View, Text, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';
import SearchedUserList from '../../Component/SearchedUserList';
import url from '../../Component/url';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import Colors from '../../../assets/Colors/Colors';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class SearchUserScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: this.props.navigation.getParam('user'),
            isLoading: false,
            SearchedUser: '',
            access_Token: '',
            ErrorMessage: '',
            isErrorModalOpen: false
        };
    }

    componentDidMount() {
        AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
            if (error === null) {
                this.setState({ access_Token: result.replace(/"/g, '').trim() }, () => this.get_User())
            }
            else {
                alert('Something Went wrong...!!')
            }
        })
    }

    get_User() {
        this.setState({ isLoading: true })
        fetch(`${url.url2}/endpoints/v1/user/search`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: `access_token=${this.state.access_Token}&server_key=${url.server_key}&word=${this.state.keyword}`
        })
            .then((response) => response.json())
            .then((res) => {
                console.log('searched user response:', res)
                if (res.code === '200') {
                    this.setState({ SearchedUser: res.data.users, isLoading: false }, () => { console.log('bbbbb:', this.state.SearchedUser) })
                }
                else {
                    this.setState({ ErrorMessage: res.errors.error_text, isLoading: false })
                }
            })
            .catch((err) => {
                console.log(err)
                // this.setState({ isLoading: false })
                this.setState({ ErrorMessage: 'Something went wrong..!', isLoading: false, isErrorModalOpen: true })
            })
    }

    render() {
        return (
            <View style={{flex:1, backgroundColor:Colors.PrimaryBackGroundColor}}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20 ,backgroundColor:Colors.PrimaryBackGroundColor}}>
                    <Icon name='arrow-left' size={13} color='black' onPress={()=>this.props.navigation.goBack()}/>
                </View>
                {this.state.isLoading
                    ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size='small' color='blue' animating />
                    </View>
                    :
                    <View style={{ flex: 1, padding: 20, backgroundColor: Colors.PrimaryBackGroundColor }}>

                        <FlatList
                            data={this.state.SearchedUser}
                            renderItem={(data) => {
                                return <SearchedUserList
                                    navigation={(username) => this.props.navigation.navigate('SearchUserWebView', {
                                        username: username,
                                        token:this.state.access_Token
                                    })}
                                    data={data} />
                            }}
                        />

                        <Modal isVisible={this.state.isErrorModalOpen}>
                            <View
                                style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >
                                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
                                <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
                                <TouchableOpacity
                                    onPress={() => { this.setState({ isErrorModalOpen: !this.state.isErrorModalOpen }) }}
                                    style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                                    activeOpacity={1}>
                                    <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>


                    </View>
                }
            </View>
        );
    }
}
