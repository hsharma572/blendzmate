const Styles = {
    ImageView:{
        height:300,
        backgroundColor:'#333',
        width:'100%',
    },
    HeaderView:{
        height:40,
        width:'100%',
        alignItems: 'center',
        justifyContent:'space-between',
        paddingHorizontal: 15,
        flexDirection: 'row',
        marginTop: 5,
    },
    UserDetailView:{
        height:70,
        width:'100%',
        flexDirection: 'row',
        paddingHorizontal: 20,
        //paddingVertical: 20,
        //backgroundColor:'grey',
        backgroundColor:'white'
    }
}

export default Styles;
