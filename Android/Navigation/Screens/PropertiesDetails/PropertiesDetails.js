import React, { Component } from 'react';
import { View, Text, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Econ1 from 'react-native-vector-icons/FontAwesome';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Colors from '../../../assets/Colors/Colors';

export default class PropertiesDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { ImageView, HeaderView, UserDetailView } = Styles;
        return (
            <ScrollView contentContainerStyle={{ paddingBottom: 20 }} showsVerticalScrollIndicator={false}>
                <View style={ImageView}>
                    <View style={HeaderView}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Econ name="arrow-left" size={15} color="white" />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Econ name="times" size={15} color="white" />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, paddingHorizontal: 40 }}>
                        <ImageBackground source={require('../../../assets/Images/placeholder.jpg')} style={{ height: '100%', width: '100%' }} />
                    </View>
                </View>

                <View style={UserDetailView}>
                    <Econ1 name="user" size={28} color='grey' style={{ marginTop: 20 }} />
                    <View style={{ marginLeft: 15, marginTop: 15 }}>
                        <Text style={{ fontFamily: 'OpenSans-Bold', fontSize: 13 }}>admin</Text>
                        <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular' }}>4 months ago</Text>
                    </View>
                </View>

                <View style={{ height: 40, width: '100%', paddingHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Regular' }}>Ahemdabad, Ahemdabad, Gujarat - 360003</Text>
                </View>

                <View style={{backgroundColor:Colors.PrimaryBackGroundColor,paddingBottom:15}}>
                    <View style={{ height: 200, width: '100%' }}>
                        <MapView
                            style={{ height: '100%', width: '100%' }}
                            provider='google'
                            showsUserLocation={true}
                            initialRegion={{
                                latitude: 37.78825,
                                longitude: -122.4324,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                        >
                            <Marker
                                coordinate={{ latitude: 37.78825, longitude: -122.4324 }}
                            />
                        </MapView>
                    </View>

                    <Text style={{ marginLeft: 15, marginTop: 10, fontFamily: 'OpenSans-Bold', fontSize: 15 }}>$0/M</Text>
                    <Text style={{ marginLeft: 15, marginTop: 5, fontFamily: 'OpenSans-Regular', fontSize: 13 }}>Amenities</Text>
                    <TouchableOpacity
                        style={{
                            height: 30, width: 40, marginLeft: 15, marginTop: 10,
                            borderRadius: 15, borderWidth: 1,
                            borderColor: 'blue', alignItems: 'center', justifyContent: 'center'
                        }}>
                        <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular' }}>123</Text>
                    </TouchableOpacity>
                    <Text style={{ marginLeft: 15, marginTop: 10, fontFamily: 'OpenSans-Regular', fontSize: 13 }}>Rules</Text>
                    <TouchableOpacity
                        style={{
                            height: 30, width: 40, marginLeft: 15, marginTop: 10,
                            borderRadius: 15, borderWidth: 1,
                            borderColor: 'blue', alignItems: 'center', justifyContent: 'center'
                        }}>
                        <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular' }}>123</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
