import Colors from '../../../assets/Colors/Colors';

const Styles = {
    topView:{
        height:50,
        width:'100%',
        backgroundColor:Colors.white,
        marginTop:20,
        flexDirection: 'row',
        alignItems: 'center',
        elevation:1,
        borderRadius: 5,
    },
    fourView:{
        flex:1,
        height:'100%',
        alignItems: 'center',
        justifyContent:'center',
        //backgroundColor:'grey'
    },
    suggestionView:{
        height:245,
        width:'100%',
        alignSelf: 'center',
        marginTop:20,
        backgroundColor:Colors.white,
        paddingVertical: 15,
        elevation:1,
        borderRadius:5
    },
    suggestionTextView:{
        height:20,
        width:'100%',
        //backgroundColor:'grey',
        paddingLeft: 15,
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent:'space-between',
        //elevation:1
    },
    profileView:{
        height:220,
        //backgroundColor:'grey',
        width:'100%',
        padding: 20,
        flexDirection: 'row',
    },
    cardView:{
        height:170,
        width:'48%',
        //backgroundColor:'green',
        borderRadius:10,
        elevation:1,
        alignItems: 'center',
        justifyContent:'center'
    },
    instagramView:{
        width:'100%',
        height:360,
        backgroundColor:Colors.white,
        marginTop:25,
        borderRadius:5,
        elevation:1
    },
    linearGradient:{
        height:30,
        width:70,
        borderRadius:15,
        justifyContent:'center',
        alignItems: 'center',
        marginTop:10,
        alignSelf: 'center',
    },
    storieView:{
        height:100,
        width:'100%',
        backgroundColor:Colors.white,
        marginTop:20,
        flexDirection: 'row',
    },
    feedHeader:{
        height:60,
        width:'100%',
        backgroundColor:Colors.white,
        //padding: 30,
        alignItems: 'center',
        justifyContent: 'center',
    }
}

export default Styles;