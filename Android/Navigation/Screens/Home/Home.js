import React, { Component } from 'react';
import { View, Text, Platform, AppState, TouchableOpacity,Image, FlatList, ImageBackground, TextInput, ActivityIndicator,PermissionsAndroid } from 'react-native';
import Styles from './style';
import Colors from '../../../assets/Colors/Colors';
import Econ from 'react-native-vector-icons/FontAwesome';
import Econ1 from 'react-native-vector-icons/FontAwesome5';
import Stories from '../../Component/Stories';
import Feed from '../../Component/feed/feed';
import Modal from "react-native-modal";
import ImagePicker from 'react-native-image-crop-picker';
import ImageGrid from '../../Component/ImageGrid';
import Dailymotion from 'react-dailymotion';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import { NavigationConstants } from '../../commonValues';
import url from '../../Component/url';
import SuggestionView from '../../Component/SuggestionView/SuggestionView';
import PubSub from 'pubsub-js';
import ImagePicker1 from 'react-native-image-picker';
import { withNavigation } from 'react-navigation';
import { NavigationEvents } from 'react-navigation';
import { width, height } from 'react-native-dimension';
import Toast from 'react-native-simple-toast';
import Geolocation from 'react-native-geolocation-service';
const options = {
  title: 'Select Avatar',
  //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPhotosVisible: false,
      images: [],
      isVideoVisible: false,
      video: '',
      isGifVisible: false,
      isLinkVisible: false,
      dailyMotionView: false,
      YoutubeView: false,
      youtubeLink: 'https://www.dailymotion.com/video/x7k2qi1?playlist=x675j5',
      dailmotionId: '',
      isMoreOpen: false,
      isFetching:false,
      id: '',
      access_Token: NavigationConstants.accessToken,
      PostData: '',
      UserSuggestion: '',
      commentPostLoading: false,
      isSuggestionLoading: false,
      StoriesData: '',
      isFeedLoading: '',
      isModalVisible: false,
      videoLink: '',
      isOpenStoryUpload: false,
      UploadStoryImage: '',
      appState: AppState.currentState,
      isStoryLoading: false,
      clearComment: false,imageUploading:false,imageText:'',videoUploading:false,videoText:'',
      modalImage:'',
      openModalForImage:false,
      openModalForReport:false,
      reportPostID:null,
      reportUserID:null,
      repostUserName:null,
      reportAvatar:null,
      blockLoading:false,
      ReportLoading:false
    };
  
    // props.navigation.jumpToIndex(3)
   
  }

  UploadStoryImage() {
    ImagePicker1.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        //const source = { uri: response.uri };

        // You can also display the image using data:
        const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          UploadStoryImage: source,
        });
      }
    });
  }

  OpenGallery() {
    ImagePicker.openPicker({
      multiple: true,
      maxFiles: 10,
      mediaType: 'photo'
    }).then(images => {
      let newImages = '';
      while (images.length > 10) {
        newImages = images.pop();
      }
      this.setState({ images: images.length > 10 ? newImages : images }, () => console.log('image arrr:', this.state.images))
      console.log(images.map(d => 'data:image/jpeg;base64,' + d.path));
    });
  }

  UploadImage() {
    const { access_Token,imageText } = this.state
    let body = new FormData();
    body.append('access_token', access_Token);
    body.append('server_key', url.server_key);
    body.append('caption', imageText);
    
    
   if(this.state.images.length==0){
    Toast.show('select a image before you post', Toast.SHORT);
    return
   }
    let images = [];
    for (i = 0; i < this.state.images.length; i++) {
      body.append('images[]', { uri: this.state.images[i].path, name: this.state.images[i].path.substring(this.state.images[i].path.lastIndexOf('/') + 1), type: this.state.images[i].mime });
      // images.push({uri:this.state.images[i].path,name:this.state.images[i].path.substring(this.state.images[i].path.lastIndexOf('/')+1) ,type:this.state.images[i].mime})
    }
    console.log('image form:', images);
    console.log('body:', body);
    
    this.setState({imageUploading:true,  })

    fetch(`${url.url}/v1/post/new_post_image`, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      //timeout: 2000,
      body: body
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          Toast.show(responseJson.message, Toast.SHORT);
          this.setState({isPhotosVisible:false,imageText:'',images:[],imageUploading:false, PostData: responseJson.data },()=>{
            this.get_Post(false)
          })
        }
        else {
          Toast.show(responseJson.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.log('error', error);
        this.setState({imageUploading:false,  })
      })
  }
  publishVideo= ()=>{
    //http://blendsmate-com.stackstaging.com/aj/posts/upload-post-video?hash=hshshs

    const { access_Token,video,videoText } = this.state
    
    let body = new FormData();
    body.append('access_token', access_Token);
    body.append('server_key', url.server_key);
    body.append('caption', videoText);
  
   if(video){
    Toast.show('select a video to post', Toast.SHORT);
    return;
   }
    body.append("video", {
      name: 'sample.mp4',
      uri: video.path,
      type: video.mime
  });
  if(video.size>50000000){
    Toast.show('video size must be below 50 mb', Toast.SHORT);

    return;
  }
    console.log('body:', body);
    this.setState({videoUploading:true,  })
    console.log(`${url.url}/v1/post/new_post_video`,body);
    fetch(`${url.url}/v1/post/new_post_video`, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      //timeout: 2000,
      body: body
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user post response:sss', responseJson);
        if (responseJson.code === '200') {
          // console.log('home post:', responseJson.data);
          Toast.show(responseJson.message, Toast.SHORT);
          this.setState({isVideoVisible:false,videoText:'',video:'',videoUploading:false, PostData: responseJson.data },()=>{
            this.get_Post(false)
          })
        }
        else {
          Toast.show(responseJson.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.log('error', error);
        this.setState({videoUploading:false,  })
      })
  }

  OpenVideoFromGallery() {
    const options = {
      title: 'Video Picker', 
      mediaType: 'video', 
      storageOptions:{
        skipBackup:true,
        path:'images'
      }
};
    ImagePicker.openPicker(options).then(video => {
      console.log('video url', video);
      this.setState({
        video: video
      })
    });
  }

  matchLink(text) {
    if (text.match(/youtube/g)) {
      this.setState({ YoutubeView: true, youtubeLink: text })
    }
    else {
      let str = text
      let res = str.indexOf('video')
      let id = str.slice(res + 6, res + 13)
      this.setState({ dailmotionId: id, dailyMotionView: true })
    }
  }

  OpenMoreOption(id) {
    this.setState({ id: id, isMoreOpen: !this.state.isMoreOpen })
  }
  hasLocationPermission = async () => {
    if (Platform.OS === 'ios' || (Platform.OS === 'android' && Platform.Version < 23)) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      alert('Location permission denied by user.');
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      alert('Location permission revoked by user.');
    }
    return false;
  };

  async componentDidMount() {
    
    // PubSub.subscribe('GoToProperties', (msg, data) => {
    //   alert('home')
    //   this.props.navigation.navigate('Properties')
    // })
    if(NavigationConstants.locationSelected=="live"){
      this.props.navigation.navigate('Properties') 
    }else if(NavigationConstants.locationSelected=="room"){
      this.props.navigation.navigate('Deals') 
    }else if(NavigationConstants.locationSelected==''){

    }

    const hasLocationPermission = await this.hasLocationPermission();
    if (hasLocationPermission) {
      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({ lat: position.coords.latitude, long: position.coords.longitude })
          console.log('lat', position);
          NavigationConstants.lat=position.coords.latitude;
          NavigationConstants.long=position.coords.longitude;
          NavigationConstants.locationLoaded=true;
        },
        (error) => {
         
          console.log(error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
    } 

   

    AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
      console.log(result.replace(/"/g, '').trim(),'kokokok')
      if (error === null) {
        this.setState({ access_Token: result.replace(/"/g, '').trim() }, () => this.get_Post(true))
        this.FetchStories();
        this.User_Suggestion();
      }
      else {
        alert('Something Went wrong...!!')
      }
    })

    AppState.addEventListener('change', this._handleAppStateChange);
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      console.log('focused');
      this.FetchStories();
      this.get_Post(false)
    });

    

    // PubSub.subscribe('@prevRoute', (_, data) => {
    //   debugger
    //   console.log(data);
      
      // if (data.isRegister) {
      //   isRegister = true
      // } else {
      //   isLogin = true
      // }
    //})

  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      this.FetchStories();
      this.User_Suggestion();
      this.get_Post(false)
    }
    this.setState({ appState: nextAppState });
  };

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.focusListener.remove();
  }

  get_PostReload(){
   
      this.setState({ isFetching: true })
    
    const { access_Token } = this.state
    fetch(`${url.url}/v1/post/fetch_home_posts`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          console.log('home post:', responseJson.data);
          this.setState({ PostData: responseJson.data, isFeedLoading: false, isFetching:false })
        }
        else {
          this.setState({ isFeedLoading: false, isModalVisible: true,isFetching:false })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isFeedLoading: false, isModalVisible: true, isFetching:false })
        //alert('post catch alert')
        //console.log('post error', error);
      })
  }


  get_Post(loading) {
    this.setState({ isFeedLoading: loading })
    
    const { access_Token } = this.state
    fetch(`${url.url}/v1/post/fetch_home_posts`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          console.log('home post:', responseJson.data);
          this.setState({ PostData: responseJson.data, isFeedLoading: false, isFetching:false })
        }
        else {
          console.log(`access_token=${access_Token}&server_key=${url.server_key}`,responseJson,'homeerr');
          this.setState({ isFeedLoading: false, isModalVisible: true,isFetching:false })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        console.log(error,'homeerr');
        this.setState({ isFeedLoading: false, isModalVisible: true, isFetching:false })
        //alert('post catch alert')
        //console.log('post error', error);
      })
  }


  BlockUser(){
    const { access_Token,reportUserID } = this.state;
    this.setState({ blockLoading:true })
    fetch(`${url.url}/v1/user/block`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&users_id=${reportUserID}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(`access_token=${access_Token}&server_key=${url.server_key}&user_id=${reportUserID}`, responseJson);
        if (responseJson.code === '200') {
          Toast.show(responseJson.data.message, Toast.SHORT);
          this.setState({ openModalForReport:false,blockLoading:false })
         
            this.get_Post(false);
            this.FetchStories();
            this.User_Suggestion();
        }
        else {
          this.setState({ blockLoading:false })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ blockLoading:false })
        //alert('post catch alert')
        //console.log('post error', error);
      })
  }

  RepostPost(){
    const { access_Token,reportPostID } = this.state;
    this.setState({ ReportLoading:true })
    fetch(`${url.url}/v1/post/report_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${reportPostID}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('report', responseJson);
        if (responseJson.code === '200') {
          console.log(`access_token=${access_Token}&server_key=${url.server_key}&post_id=${reportPostID}`, responseJson.message);
          Toast.show(responseJson.message, Toast.SHORT);
          this.setState({ openModalForReport:false,ReportLoading:false })
         
            this.get_Post(false);
            this.FetchStories();
            this.User_Suggestion();
        }
        else {
          this.setState({ ReportLoading:false })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ ReportLoading:false })
        //alert('post catch alert')
        //console.log('post error', error);
      })
  }
  
  get_Post2() {
  
    const { access_Token } = this.state
    fetch(`${url.url}/v1/post/fetch_home_posts`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          console.log('home post:', responseJson.data);
          this.setState({ PostData: responseJson.data, isFeedLoading: false })
        }
        else {
          this.setState({ isFeedLoading: false, isModalVisible: true })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isFeedLoading: false, isModalVisible: true })
        //alert('post catch alert')
        //console.log('post error', error);
      })
  }


  User_Suggestion() {
    this.setState({ isSuggestionLoading: true })
    const { access_Token } = this.state
    fetch(`${url.url}/v1/user/fetch_suggestions`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user suggestions response:', responseJson);
        if (responseJson.code === '200') {
          console.log('user suggestion', responseJson.data);
          this.setState({ UserSuggestion: responseJson.data, isSuggestionLoading: false }, () => {
            console.log('state user suggestion:', this.state.UserSuggestion);
          })
        }
        else {
          console.log(responseJson,'homeerr');
          this.setState({ isSuggestionLoading: false, isModalVisible: true })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isSuggestionLoading: false, isModalVisible: true })
        console.log(error,'homeerr');
        //alert('suggestion catch alert', error)
      })
  }

  LikePost(post_id) {
    const { access_Token } = this.state
    fetch(`${url.url}/v1/post/like_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${post_id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('post like response:', responseJson);
        if (responseJson.code === '200') {
          this.LikedData(post_id, responseJson.is_liked)
        }
        else {
          this.setState({ isModalVisible: true })
          // alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isModalVisible: true })
        //alert('catch alert')
      })
  }

  LikedData = (post_id, LikeCount) => {
    console.log('id', post_id)
    const NewData = this.state.PostData.map(item => {
      if (item.post_id === post_id) {
        item.is_liked = !item.is_liked,
          item.likes = LikeCount
        console.log('funcrion:', item)
        return item;
      }
      return item
    })
    this.setState({ PostData: NewData })
  }

  PostComment(cmt, post_id) {
    const { access_Token } = this.state
    this.setState({clearComment:false})
    fetch(`${url.url}/v1/post/add_comment`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&text=${cmt}&post_id=${post_id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('post like response:', responseJson);
        if (responseJson.code === '200') {
          this.setState({clearComment:true})
          this.get_Post2()
        }
        else {
          this.setState({ isModalVisible: true })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isModalVisible: true })
        //alert('catch alert')
      })
  }

  CommentedData = (post_id) => {
    console.log('id', post_id)
    const NewData = this.state.PostData.map(item => {
      if (item.post_id === post_id) {
        item.comments.length = item.comments.length + 1
        console.log('funcrion:', item)
        return item;
      }
      return item
    })
    this.setState({ PostData: NewData })
  }

  FollowUser(id,type) {
    const { access_Token } = this.state
    this.setState({ isFollowSpinner: true })
    console.log(`access_token=${access_Token}&server_key=${url.server_key}&follow_id=${id}`,'FollowUser');
    fetch(`${url.url}/v1/user/follow`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&follow_id=${id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('FollowUser', responseJson);
        if (responseJson.code === '200') {
          this.setState({ isFollowSpinner: false,openModalForReport:false,  }, () => {
            if(type=="suggest"){
              setTimeout(() => {
                this.User_Suggestion();
              }, 1000);
            }else{
              Toast.show('Unfollowed successfully', Toast.SHORT);
             
                this.get_Post(false);
             
            }
           
          })
          //alert('you follow...!')
        }
        else {
          this.setState({ isFollowSpinner: false, isModalVisible: true })
          // alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isFollowSpinner: false, isModalVisible: true })
        console.log(error,'FollowUser');
      })
  }

  FetchStories() {
    const { access_Token } = this.state
    this.setState({ isStoryLoading: true })
    fetch(`${url.url}/v1/story/fetch_stories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('stories response:', responseJson);
        if (responseJson.code === '200') {
          this.setState({ StoriesData: responseJson.data, isStoryLoading: false })
        }
        else {
          console.log(`access_token=${access_Token}&server_key=${url.server_key}`,responseJson,'homeerr');
          this.setState({ isStoryLoading: false, isModalVisible: true })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isStoryLoading: false, isModalVisible: true })
        console.log(error,'homeerr');
      })
  }

  DeletePost(id) {
    // alert(id)
    const { access_Token } = this.state
    console.log(`access_token=${access_Token}&server_key=${url.server_key}&post_id=${id}`)
    fetch(`${url.url}/v1/post/delete_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Delete post response:', responseJson);
        debugger
        if (responseJson.code === '200') {
          this.get_Post(false)
          Toast.show(responseJson.message, Toast.SHORT);

        }
        else {
          Toast.show(responseJson.errors.error_text, Toast.SHORT);
        }
      })
      .catch((error) => {
        Toast.show('something went wrong..!! ERDEL', Toast.SHORT);
      })
  }

  MakeFavourite(id) {
    const { access_Token } = this.state
    fetch(`${url.url}/v1/post/add_to_favorite`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('favourite post response:', responseJson);
        if (responseJson.code === '200') {
          this.FavouritedPost(id)
        }
        else {
          this.setState({ isFollowSpinner: false, isModalVisible: true })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isFollowSpinner: false, isModalVisible: true })
        //alert('catch alert')
      })
  }


  FavouritedPost = (id) => {
    //console.log('id', post_id)
    const NewData = this.state.PostData.map(item => {
      if (item.post_id === id) {
        item.is_saved = !item.is_saved
        console.log('funcrion:', item)
        return item;
      }
      return item
    })
    this.setState({ PostData: NewData })
  }

  render() {

    const { topView, fourView, storieView } = Styles;

    return (
        
      <View style={{ backgroundColor: Colors.PrimaryBackGroundColor,  }} >

<Modal
              animationIn='slideInUp'
              animationInTiming={500}
              animationOut='slideOutDown'
              animationOutTiming={500}
              onBackdropPress={() => this.setState({ isPhotosVisible: false })}
              isVisible={this.state.isPhotosVisible}>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ height: 450, width: '100%', backgroundColor: 'white', borderRadius: 8, padding: 15 }}>
                  <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
                    <Econ name="user" size={22} color="grey" />
                    <Text style={{ marginLeft: 20, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'grey' }}>admin</Text>
                    <Econ name="caret-right" size={12} color="grey" style={{ marginLeft: 10, marginTop: 3 }} />
                    <Econ name="image" size={12} color="rgba(237,56,51,1)" style={{ marginLeft: 10, marginTop: 3 }} />
                    <Text style={{ marginLeft: 10, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'rgba(237,56,51,1)' }}>Image</Text>
                  </View>

                  <View style={{ height: 40, width: '100%', alignSelf: 'center', flexDirection: 'row', backgroundColor: 'white', marginTop: 15 }}>
                    <TextInput
                      placeholder="Add post caption, #hashtag, or @mention?"
                      style={{ width: '90%', height: '100%', backgroundColor: 'white', fontSize: 11, fontFamily: 'OpenSans-Regular' }}
                      onChangeText={(imageText) => this.setState({imageText})}
        value={this.state.imageText}
                       />

                    <View style={{ width: '10%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                      <Econ1 name="laugh" size={18} color="grey" />
                    </View>

                  </View>


                  <View style={{ height: 115, width: '100%', flexDirection: 'row' }}>
                    <FlatList
                      numColumns={5}
                      data={this.state.images}
                      renderItem={(data) => { return <ImageGrid data={data.item} key={data.item} /> }}
                    />
                  </View>

                  <TouchableOpacity
                    onPress={() => this.OpenGallery()}
                    style={{
                      height: 120, width: '100%',
                      borderWidth: 1, borderRadius: 3,
                      alignItems: 'center', justifyContent: 'center',
                      borderStyle: 'dashed', borderColor: 'grey', marginTop: 20
                    }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Econ name="image" size={25} color="grey" />
                      <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Regular', marginTop: 10, color: 'grey' }}>Choose up to 10 images</Text>
                    </View>

                  </TouchableOpacity>

                  <View style={{ alignSelf: 'flex-end', width: 220, backgroundColor: 'white', height: 35, position: 'absolute', bottom: 10, right: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                      onPress={() => this.setState({ isPhotosVisible: false })}
                      style={{ height: '100%', width: 100, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'grey', borderRadius: 5 }}>
                      <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'grey' }}>CLOSE</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        if(!this.state.imageUploading){
                          this.UploadImage()
                        }
                        
                      }}
                      style={{ height: '100%', width: 100, backgroundColor: 'rgba(237,56,51,1)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'rgba(237,56,51,1)', borderRadius: 5 }}>
                        {
                          this.state.imageUploading?(
                            <ActivityIndicator size='small' color="white" />
                          ):(<Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>PUBLISH</Text>)
                        }
                        
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
            
          <Modal
            animationIn='slideInUp'
            animationInTiming={500}
            animationOut='slideOutDown'
            animationOutTiming={500}
            onBackdropPress={() => this.setState({ isVideoVisible: false })}
            isVisible={this.state.isVideoVisible}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: 450, width: '100%', backgroundColor: 'white', borderRadius: 8, padding: 15 }}>
                <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
                  <Econ name="user" size={22} color="grey" />
                  <Text style={{ marginLeft: 20, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'grey' }}>admin</Text>
                  <Econ name="caret-right" size={12} color="grey" style={{ marginLeft: 10, marginTop: 3 }} />
                  <Econ name="image" size={12} color="rgba(237,56,51,1)" style={{ marginLeft: 10, marginTop: 3 }} />
                  <Text style={{ marginLeft: 10, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'rgba(237,56,51,1)' }}>Video</Text>
                </View>

                <View style={{ height: 40, width: '100%', alignSelf: 'center', flexDirection: 'row', backgroundColor: 'white', marginTop: 15 }}>
                  <TextInput
                    placeholder="Add post caption, #hashtag, or @mention?"
                    style={{ width: '90%', height: '100%', backgroundColor: 'white', fontSize: 11, fontFamily: 'OpenSans-Regular' }} 
                    onChangeText={(videoText) => this.setState({videoText})}
        value={this.state.videoText}
                    />

                  <View style={{ width: '10%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                    <Econ1 name="laugh" size={18} color="grey" />
                  </View>

                </View>


                <View style={{ height: 115, width: '100%', flexDirection: 'row' }}>
                  <ImageBackground source={{ uri: this.state.video.path }} style={{ height: 120, width: '100%', marginTop: 20 }} />
                </View>

                <TouchableOpacity
                  onPress={() => this.OpenVideoFromGallery()}
                  style={{
                    height: 120, width: '100%',
                    borderWidth: 1, borderRadius: 3,
                    alignItems: 'center', justifyContent: 'center',
                    borderStyle: 'dashed', borderColor: 'grey', marginTop: 50
                  }}>
                  <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Econ name="image" size={25} color="grey" />
                    <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Regular', marginTop: 10, color: 'grey' }}>Choose a Video</Text>
                  </View>

                </TouchableOpacity>

                <View style={{ alignSelf: 'flex-end', width: 220, backgroundColor: 'white', height: 35, position: 'absolute', bottom: 10, right: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                  <TouchableOpacity
                    onPress={() => this.setState({ isVideoVisible: false })}
                    style={{ height: '100%', width: 100, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'grey', borderRadius: 5 }}>
                    <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'grey' }}>CLOSE</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ height: '100%', width: 100, backgroundColor: 'rgba(237,56,51,1)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'rgba(237,56,51,1)', borderRadius: 5 }}
                    onPress={()=>{
                     
                      if(!this.state.videoUploading){
                        this.publishVideo();
                        }
                    }}>
                    {
                      this.state.videoUploading?(
                        <ActivityIndicator size='small' color="white" />
                      ):(
                        <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>PUBLISH</Text>
                      )
                    }
                    
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          <Modal
            animationIn='slideInUp'
            animationInTiming={500}
            animationOut='slideOutDown'
            animationOutTiming={500}
            onBackdropPress={() => this.setState({ isLinkVisible: false })}
            isVisible={this.state.isLinkVisible}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: 450, width: '100%', backgroundColor: 'white', borderRadius: 8, padding: 15 }}>
                <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
                  <Econ name="user" size={22} color="grey" />
                  <Text style={{ marginLeft: 20, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'grey' }}>admin</Text>
                  <Econ name="caret-right" size={12} color="grey" style={{ marginLeft: 10, marginTop: 3 }} />
                  <Econ name="image" size={12} color="rgba(237,56,51,1)" style={{ marginLeft: 10, marginTop: 3 }} />
                  <Text style={{ marginLeft: 10, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'rgba(237,56,51,1)' }}>Embed Video</Text>
                </View>

                <View style={{ height: 40, width: '100%', alignSelf: 'center', flexDirection: 'row', backgroundColor: 'white', marginTop: 15 }}>
                  <TextInput
                    placeholder="Add post caption, #hashtag, or @mention?"
                    style={{ width: '90%', height: '100%', backgroundColor: 'white', fontSize: 11, fontFamily: 'OpenSans-Regular' }} />

                  <View style={{ width: '10%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                    <Econ1 name="laugh" size={18} color="grey" />
                  </View>
                </View>

                <View style={{ height: 150, width: '100%', marginTop: 10 }}>
                  {
                    this.state.YoutubeView
                      ?
                      <ReactPlayer showPlayIcon={false} url={`${this.state.youtubeLink}`} playing={true} style={{ height: 150, width: '100%' }} />
                      :
                      null
                  }
                  {
                    this.state.dailyMotionView
                      ?
                      <Dailymotion
                        video={`${this.state.dailmotionId}`}
                        uiTheme="light"
                        autoplay
                      />
                      :
                      null
                  }
                  {/* <Video
                    style={{ height: '100%', width: '100%' }}
                    source={{ uri: this.state.videoLink }}
                    ref={(ref) => {
                      this.player = ref
                    }}
                    onBuffer={this.onBuffer}
                    onError={this.videoError}
                    style={{ height: 150, width: '100%' }}
                  /> */}
                </View>

                <View style={{ height: 40, width: '100%', flexDirection: 'row', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.1)', borderRadius: 20, position: 'absolute', bottom: 70, alignSelf: 'center' }}>
                  <Econ name="link" size={15} color="rgba(0,0,0,0.3)" style={{ marginLeft: 15 }} />
                  <TextInput
                    onChangeText={(text) => this.setState({ videoLink: text }, () => console.log(this.state.videoLink))}
                    style={{ height: 40, width: 250, marginLeft: 5, fontSize: 12, fontFamily: 'OpenSans-Regular' }} placeholder="YouTube, Dailymotion, Vimeo URL" />
                </View>

                <View style={{ alignSelf: 'flex-end', width: 220, backgroundColor: 'white', height: 35, position: 'absolute', bottom: 10, right: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                  <TouchableOpacity
                    onPress={() => this.setState({ isLinkVisible: false })}
                    style={{ height: '100%', width: 100, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'grey', borderRadius: 5 }}>
                    <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'grey' }}>CLOSE</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ height: '100%', width: 100, backgroundColor: 'rgba(237,56,51,1)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'rgba(237,56,51,1)', borderRadius: 5 }}>
                    <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>PUBLISH</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
       
        {/* <View style={storieView}>
          <View style={{ height: 100, width: 75, alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ height: 55, width: 55, borderRadius: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: 'grey' }}>
              <ImageBackground source={require('../../../assets/Images/placeholder.jpg')}
                style={{
                  height: 50, width: 50,
                  bordreWidth: 1, borderColor: Colors.white,
                  alignItems: 'flex-end',
                  justifyContent: 'flex-end'
                }}
                imageStyle={{ borderRadius: 25 }}>
                <View
                  style={{
                    height: 18,
                    width: 18,
                    borderRadius: 9,
                    backgroundColor: 'white',
                    position: 'absolute', bottom: -3, right: -3,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}>
                  <TouchableOpacity
                    onPress={() => this.setState({ isOpenStoryUpload: true })}
                    style={{
                      height: 16,
                      width: 16,
                      borderRadius: 8,
                      justifyContent: 'center', alignItems: 'center', backgroundColor: 'blue'
                    }}>
                    <Econ name="plus" size={10} color="white" />
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </View>
            <Text style={{ fontSize: 11, marginTop: 5, fontFamily: 'Roboto-Regular' }}>Bhautik</Text>
          </View>

          {
            this.state.isStoryLoading
              ?
              <View style={{ height: 100, width: 200, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size='small' color="blue" />
              </View>
              :
              this.state.StoriesData.length === 0
                ?
                <View style={{ height: 100, width: 200, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: 11, fontFamily: 'Roboto-Bold', color: 'rgba(0,0,0,0.4)' }}>No Data Available</Text>
                </View>
                :
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={this.state.StoriesData}
                  renderItem={(data) => {
                    return <Stories data={data} key={data.key} OpenStories={() => PubSub.publish('OpenStories', { data: data.item })} />
                  }}
                />
          }
        </View> */}

      

        {
          this.state.isFeedLoading
            ?
            <View style={{ height: 100, width: 100, alignSelf: 'center', justifyContent: 'flex-end', alignItems: 'center', }}>
              <ActivityIndicator size="small" color="blue" />
            </View>
            :
            <FlatList
            contentContainerStyle={{padding:width(1)}}
            showsVerticalScrollIndicator={false}
              ListHeaderComponent={()=>{
                return(  
                <View>
                   <View style={topView}>

<TouchableOpacity
//this.setState({ isPhotosVisible: !this.state.isPhotosVisible })
//this.props.navigation.navigate('Properties')
  onPress={() =>this.setState({ isPhotosVisible: !this.state.isPhotosVisible })}
  style={[fourView, { borderTopLeftRadius: 5, borderBottomLeftRadius: 5, }]}>
  <Econ name="camera" size={16} color="green" />
</TouchableOpacity>
<View style={{ height: 40, backgroundColor: Colors.PrimaryBackGroundColor, width: 2 }}>



</View>

<TouchableOpacity
  onPress={() => this.setState({ isVideoVisible: !this.state.isVideoVisible })}
  style={fourView}>
  <Econ1 name="video" size={16} color="blue" />
</TouchableOpacity>



{/* <View style={{ height: 40, backgroundColor: Colors.PrimaryBackGroundColor, width: 1 }}>
</View>

<View style={{ height: 40, backgroundColor: Colors.PrimaryBackGroundColor, width: 1 }}>

</View> */}
{/* <TouchableOpacity
  onPress={() => this.setState({ isLinkVisible: !this.state.isLinkVisible })}
  style={[fourView, { borderTopRightRadius: 10, borderBottomRightRadius: 10 }]}>
  <Econ name="link" size={16} color="orange" />
</TouchableOpacity> */}


</View>

            
                
                <View style={{ height: 260, width: '100%', paddingVertical: 15, borderWidth: 1, borderColor: 'rgba(0,0,0,0.1)', paddingLeft: 10, marginTop: 15, borderRadius: 5 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingRight: 10 }}>
            <Text style={{ fontSize: 14, fontFamily: 'Roboto-Bold' }}>Suggestions For You</Text>
            {/* <TouchableOpacity
              onPress={() => PubSub.publish('AllSuggestion')}
              style={{ flexDirection: 'row' }}
            >
              <Text style={{ fontSize: 14, fontFamily: 'Roboto-Bold', color: 'blue' }}>See all</Text>
              <View style={{ alignItems: 'center', marginTop: 3, marginLeft: 5 }}>
                <Econ name="chevron-right" size={14} color="blue" />
              </View>
            </TouchableOpacity> */}
          </View>
          {
            this.state.isSuggestionLoading
              ?
              <View style={{ height: 100, width: 100, alignSelf: 'center', justifyContent: 'flex-end', alignItems: 'center', }}>
                <ActivityIndicator size="small" color="blue" />
              </View>
              :
              this.state.UserSuggestion.length == 0
                ?
                <View style={{ height: 220, width: 320, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 11, fontFamily: 'Roboto-Bold', color: 'rgba(0,0,0,0.4)' }}>No Data Available</Text>
                </View>
                :
                <FlatList
                  keyExtractor={(data) => data.user_id.toString()}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={this.state.UserSuggestion}
                  renderItem={(data) => {
                    if (data.index > 1) {
                      return
                    }
                    return <SuggestionView reloadData={(type)=>{
                      if(type=="suggest"){
           
                this.User_Suggestion();
             
            }else{
              Toast.show('Unfollowed successfully', Toast.SHORT);
             
                this.get_Post(false);
             
            }
                    }} access_Token={this.state.access_Token}  data={data} key={data.item.user_id} />
                  }}
                />
          }
        </View></View>);
              }}
              data={this.state.PostData}
              onRefresh={() => this.get_PostReload()}
      refreshing={this.state.isFetching}
              keyExtractor={(data) => data.post_id.toString()}
              extraData={this.state}
              ListEmptyComponent={()=>{
                return(
                  <View style={{paddingVertical:10,alignItems:'center'}}>
                    <Text style={{fontSize: 14, fontFamily: 'Roboto-Bold'}}>No Posts Available</Text>
                  </View>
                );
              }}
              renderItem={(data) => {
                return <Feed
                  Favourite={(id) => this.MakeFavourite(id)}
                  access_Token={this.state.access_Token}
                  data={data}
                  // getItemLayout={(data, index) => (
                  //   console.log('index:',index),
                  //   { length: 360, offset: 360 * index, index }
                  // )}
                  isCommentClear={this.state.clearComment}
                  openImage={(img)=>{
                    this.setState({modalImage:img,openModalForImage:true});
                  }}
                  DeletePost={(id) => this.DeletePost(id)}
                  CommentPostLoading={this.state.commentPostLoading}
                  Comment={(cmt, id) => this.PostComment(cmt, id)}
                  likePost={() => this.LikePost(data.item.post_id)}
                  isOpen={this.state.isMoreOpen}
                  key={data.item.post_id}
                  id={this.state.id}
                  OpenReport={(post_id,userID,userName,avatar)=>{ this.setState({reportPostID:post_id,reportUserID:userID,openModalForReport:true,repostUserName:userName,reportAvatar:avatar});}}
                  more={(id) => this.OpenMoreOption(id)} />
              }}
            />
        }

        <Modal
          animationIn='slideInUp'
          animationInTiming={500}
          animationOut='slideOutDown'
          animationOutTiming={500}
          onBackdropPress={() => this.setState({ isOpenStoryUpload: false })}
          isVisible={this.state.isOpenStoryUpload}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ height: 450, width: '100%', backgroundColor: 'white', borderRadius: 8, padding: 15 }}>
              <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
                <Econ name="user" size={22} color="grey" />
                <Text style={{ marginLeft: 20, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'grey' }}>admin</Text>
                <Econ name="caret-right" size={12} color="grey" style={{ marginLeft: 10, marginTop: 3 }} />
                <Econ name="image" size={12} color="rgba(237,56,51,1)" style={{ marginLeft: 10, marginTop: 3 }} />
                <Text style={{ marginLeft: 10, fontFamily: 'OpenSans-Regular', fontSize: 13, color: 'rgba(237,56,51,1)' }}>Story</Text>
              </View>

              <View style={{ height: 40, width: '100%', alignSelf: 'center', flexDirection: 'row', backgroundColor: 'white', marginTop: 15 }}>
                <TextInput
                  placeholder="Add a story caption.."
                  style={{ width: '90%', height: '100%', backgroundColor: 'white', fontSize: 11, fontFamily: 'OpenSans-Regular' }} />

                <View style={{ width: '10%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                  <Econ1 name="laugh" size={18} color="grey" />
                </View>

              </View>


              <View style={{ height: 115, width: '100%', flexDirection: 'row' }}>
                <ImageBackground source={this.state.UploadStoryImage} style={{ height: 120, width: '100%', marginTop: 20 }} />
              </View>

              <TouchableOpacity
                onPress={() => this.UploadStoryImage()}
                style={{
                  height: 120, width: '100%',
                  borderWidth: 1, borderRadius: 3,
                  alignItems: 'center', justifyContent: 'center',
                  borderStyle: 'dashed', borderColor: 'grey', marginTop: 50
                }}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Econ name="image" size={25} color="grey" />
                  <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Regular', marginTop: 10, color: 'grey' }}>Choose a Photo</Text>
                </View>

              </TouchableOpacity>

              <View style={{ alignSelf: 'flex-end', width: 220, backgroundColor: 'white', height: 35, position: 'absolute', bottom: 10, right: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                <TouchableOpacity
                  onPress={() => this.setState({ isOpenStoryUpload: false })}
                  style={{ height: '100%', width: 100, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'grey', borderRadius: 5 }}>
                  <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'grey' }}>CLOSE</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ height: '100%', width: 100, backgroundColor: 'rgba(237,56,51,1)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'rgba(237,56,51,1)', borderRadius: 5 }}>
                  <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>PUBLISH</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Modal isVisible={this.state.isModalVisible}>
          <View
            style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >
            <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
            <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
            <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
            <TouchableOpacity
              onPress={() => { this.setState({ isModalVisible: !this.state.isModalVisible }) }}
              style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
              activeOpacity={1}>
              <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <Modal isVisible={this.state.openModalForImage}
         onBackdropPress={() => this.setState({ openModalForImage: false })}>
          <TouchableOpacity onPress={() => this.setState({ openModalForImage: false })}
            style={{width:width(80),height:height(60), alignSelf: 'center', borderRadius:10 }} >
             <Image
        style={{width:width(80),height:'100%'}}
        source={{
          uri:this.state.modalImage
        }}
        resizeMode={'contain'}
      /></TouchableOpacity>
        </Modal>


        <Modal isVisible={this.state.openModalForReport}
         onBackdropPress={() => this.setState({ openModalForReport: false })}>
          <View style={{backgroundColor:'white',width:width(80),height:height(30), alignItems: 'center',alignSelf:'center' ,borderRadius:10 }}>
            <View style={{ padding:width(4),alignItems:'center',height:height(16),justifyContent:'center'}}>
            {
                           this.state.reportAvatar?(
                                <Image
        style={{height:width(14),width:width(14)}}
        source={{
          uri: this.state.reportAvatar,
        }}
      />
                            ):(<Econ1 name="user" size={width(14)} color="grey" />)
                        }
                      <View style={{marginVertical:5}}><Text style={{
                         fontFamily: 'Roboto-Bold', 
        fontSize: 13, 
                      }}>{this.state.repostUserName}</Text></View>  
            </View>
            <View style={{flexDirection:'row',height:height(7),borderTopWidth:1,borderTopColor:'#f2f2f2'}}>
                      <TouchableOpacity onPress={()=>{if(!this.state.blockLoading){this.BlockUser();}}} style={{flex:1,alignItems:'center',justifyContent:'center',borderRightWidth:1,borderRightColor:'#f2f2f2'}}>
                      {this.state.blockLoading?(
                        <ActivityIndicator size='small' color="black" />
                      ):(<Text style={{color:'#fa5050'}}>Block</Text>)}
                        
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{if(!this.state.isFollowSpinner){this.FollowUser(this.state.reportUserID,'unfollow');}}} style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                      {this.state.isFollowSpinner?(
                        <ActivityIndicator size='small' color="black" />
                      ):(<Text>Unfollow</Text>)}
                        
                      </TouchableOpacity>
              </View>
              <View style={{flexDirection:'row',height:height(7),borderTopWidth:1,borderTopColor:'#f2f2f2'}}>
                  <TouchableOpacity onPress={()=>{
                    if(!this.state.ReportLoading){
                    this.RepostPost();
                    }
                  }} style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                     {this.state.ReportLoading?(
                        <ActivityIndicator size='small' color="black" />
                      ):(<Text>Report</Text>)}
                        
                      </TouchableOpacity>
              </View>
          </View>
        </Modal>

      </View>
        
    );
  }
}
