import React, { PureComponent } from 'react';
import { View, Text, ScrollView, ImageBackground, TouchableOpacity, ActivityIndicator } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import Econ1 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import PubSub from 'pubsub-js';

export default class Settings extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false
        };
    }

    render() {
        const { AvtarView, SettingView, SettingButton, SettingBlock } = Styles;
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#fff' }}>
                <View style={AvtarView}>
                    <ImageBackground
                        imageStyle={{ borderRadius: 50 }}
                        source={require('../../../assets/Images/placeholder.jpg')}
                        style={{ height: 75, width: 75, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ height: 25, width: 25, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fafafa', borderRadius: 15, elevation: 5 }}>
                            <Econ1 name="plus" size={12} color="#333" />
                        </TouchableOpacity>
                    </ImageBackground>
                    <View style={{ height: '100%', justifyContent: 'center', marginLeft: 20 }}>
                        <Text style={{ fontSize: 20, fontFamily: 'OpenSans-Regular', color: '#333' }}>Victoria Smith</Text>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', marginLeft: 5, color: 'rgba(0,0,0,0.3)' }}>smith@domain.com</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={SettingView}>
                    <View style={SettingBlock}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('General')}
                            style={SettingButton}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="cog" size={11} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>General</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('ProfileSetting')}
                            style={[SettingButton]}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="user" size={11} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Profile settings</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('Password')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="lock" size={11} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Password</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('Privacy')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ1 name="shield-alt" size={9} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 13 }}>Privacy</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('Notification')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="bell" size={9} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 13 }}>Notifications</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('VerificationProfile')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="certificate" size={9} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Verification</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('Blocked')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="ban" size={11} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Blocked users</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('Interests')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="fire" size={11} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Interests</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[SettingButton]}
                            onPress={() => this.props.navigation.navigate('DeleteAccount')}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ name="trash" size={11} color="#333" />
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Delete Account</Text>
                            </View>
                            <Econ1 name="chevron-right" size={11} color="#333" />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ backgroundColor: '#fafafa', marginHorizontal: 20, marginTop: 5, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, paddingVertical: 5 }}>
                    <TouchableOpacity style={[SettingButton]}
                        onPress={() => {
                            this.setState({ isLoading: true })
                            AsyncStorage.removeItem(Constants.USER_TOKEN).then(() => {
                                console.log('log out:')
                                //this.props.navigation.navigate('loginStack')
                                PubSub.publish('_goToLoginScreen')
                            })
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Econ1 name="sign-out-alt" size={11} color="#333" />
                            {
                                this.state.isLoading
                                    ?
                                    <ActivityIndicator size='small' color='black' style={{marginLeft:20}}/>
                                    :
                                    <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: "#333", marginLeft: 15 }}>Log out</Text>
                            }
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
