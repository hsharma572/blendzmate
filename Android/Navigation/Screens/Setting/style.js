const Styles = {
    AvtarView:{
        height:140,
        width:'100%',
        padding: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    SettingView:{
        flex: 1,
        width:'100%',
        paddingHorizontal:20,
    },
    SettingButton:{
        height:35,
        width:'100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent:'space-between'
    },
    SettingBlock:{
        borderRadius:5,
        backgroundColor:'#fafafa',
        width:'100%',
        alignItems: 'center',
        paddingVertical:10
    }
}

export default Styles;