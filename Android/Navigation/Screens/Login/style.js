import Color from '../../../assets/Colors/Colors';
import { width, height, totalSize } from 'react-native-dimension';
const Styles = {
    LoginLabel:{
        marginTop:height(3),
        fontFamily: 'Roboto-Bold',
        fontSize: totalSize(2),
        color:'rgba(237,56,51,1)',
        textAlign:'center'
    },
    InputView:{
        width:width(76),
        height:height(6),
        backgroundColor:'white',
        marginTop:height(2),
        borderRadius: 20,
        flexDirection: 'row',
        elevation:2,
        paddingHorizontal: width(6),
        paddingVertical: 10,
        alignItems: 'center',
        alignSelf:'center',
        shadowOffset: { width: 3, height: 3 },
        shadowColor: '#8a795d',
        shadowOpacity: 0.2,
    },
    LoginContainer:{
        height:height(56),
        width:width(88),
        backgroundColor:'white',
        alignSelf: 'center',
        padding: width(4),
        elevation:5,
        borderRadius:8
    },
    LoginButton:{
        height:width(12),
        width:width(12),
        justifyContent:'center',
        alignItems: 'center',
        borderRadius: 40,
        position:'absolute',
        right:25,
        bottom:25,
        backgroundColor:'rgba(69,183,221,1)'
    },
    error: {
        fontSize: 11,
        color: 'red',
        marginTop:3,
        marginLeft:10
    }
}

export default Styles;