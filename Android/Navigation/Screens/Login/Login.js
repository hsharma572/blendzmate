import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import Econ1 from 'react-native-vector-icons/FontAwesome5';
import url from '../../Component/url';
import Modal from 'react-native-modal';
import {Constants} from '../../Component/Contants';
import AsyncStorage from '@react-native-community/async-storage';
import OtpInputs from 'react-native-otp-inputs';
import {width, height, totalSize} from 'react-native-dimension';
import {NavigationConstants} from '../../commonValues';
import Toast from 'react-native-simple-toast';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      badEmail: false,
      badPassword: false,
      isLoading: false,
      isModalVisible: false,
      ErrorMessage: '',
      isErrorModalOpen: false,
      isAutoLoading: true,
      ModalOpen: false,
      user_id: '',
      Access_Token: '',
      hash: '',
      time: '',
      otp: '',
      TypedOtp: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
      console.log('result', result);
      if (result) {
        NavigationConstants.accessToken = result.replace(/['"]+/g, '');
        fetch(`${url.url}/v1/user/fetch_userdata`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          },
          body: `server_key=${url.server_key}&access_token=${result.replace(
            /['"]+/g,
            '',
          )}`,
        })
          .then(res => res.json())
          .then(data => {
            console.log(data, 'username:');

            if (data.code === '200') {


              NavigationConstants.user_id=data.data.user_id;

              AsyncStorage.setItem('UserName', data.data.username, err => {
                console.log(err, 'errororoororo');
                if (err === null) {
                  if (data.data.startup_avatar != 1) {
                    this.props.navigation.navigate('AddPhoto');
                  } else if (data.data.startup_info != 1) {
                    // alert('hello')
                    this.props.navigation.navigate('LookingFor');
                  } else {
                    Toast.show('Login Successfully..!', Toast.SHORT);
                    this.props.navigation.navigate('MainScreen');
                    this.setState({username: '', password: ''});
                  }
                } else {
                  this.setState({
                    ErrorMessage: responseJson.errors.error_text,
                    isModalVisible: true,
                    isLoading: false,
                  });
                }
              });
            } else {
              this.setState({
                ErrorMessage: 'Invalid Session. Please Login And Try Again',
                isLoading: false,
                isErrorModalOpen: true,
                isAutoLoading: false,
              });
            }
          })
          .catch(e => {
            console.log(e);

            this.setState({
              ErrorMessage: 'Something went wrong..!',
              isLoading: false,
              isErrorModalOpen: true,
            });
          });
      } else {
        this.setState({isAutoLoading: false});
      }
    });
  }

  Login() {
    console.log(
      `username=${username}&password=${password}&server_key=${
        url.server_key
      }&device_id=${NavigationConstants.onesingalUserid}`,
    );
    const {username, password} = this.state;
    if (this.validate(username, password)) {
      this.setState({isLoading: true});
      fetch(`${url.url}/v1/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        //timeout: 2000,
        body: `username=${username}&password=${password}&server_key=${
          url.server_key
        }&device_id=${NavigationConstants.onesingalUserid}`,
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(
            'login up response:',
            responseJson,
            `username=${username}&password=${password}&server_key=${
              url.server_key
            }`,
          );

          //this.setState({ isLoading: false })
          if (responseJson.code === '200') {
            NavigationConstants.user_id=responseJson.data.user_id;
            if (responseJson.data.otp) {
              this.setState({
                ModalOpen: true,
                user_id: responseJson.data.user_id,
                Access_Token: responseJson.data.access_token.replace(
                  /['"]+/g,
                  '',
                ),
                time: responseJson.data.time,
                hash: responseJson.data.hash,
              });
            } else {
              AsyncStorage.multiSet(
                [
                  [Constants.USER_LOGIN_STATUS, 'true'],
                  [Constants.USER_ID, responseJson.data.user_id.toString()],
                  [Constants.USER_TOKEN, responseJson.data.access_token],
                ],
                error => {
                  if (error === null) {
                    NavigationConstants.accessToken =
                      responseJson.data.access_token;
                    this.setState({isLoading: false});
                    fetch(`${url.url}/v1/user/fetch_userdata`, {
                      method: 'POST',
                      headers: {
                        'Content-Type':
                          'application/x-www-form-urlencoded;charset=UTF-8',
                      },
                      body: `server_key=${url.server_key}&access_token=${
                        responseJson.data.access_token
                      }`,
                    })
                      .then(res => res.json())
                      .then(data => {
                        console.log(data, 'username:');

                        if (data.code === '200') {
                          AsyncStorage.setItem(
                            'UserName',
                            data.data.username,
                            err => {
                              console.log(err, 'errororoororo');
                              if (err === null) {
                                if (data.data.startup_avatar != 1) {
                                  this.props.navigation.navigate('AddPhoto');
                                } else if (data.data.startup_info != 1) {
                                  // alert('hello')
                                  this.props.navigation.navigate('LookingFor');
                                } else {
                                  Toast.show(
                                    'Login Successfully..!',
                                    Toast.SHORT,
                                  );
                                  this.props.navigation.navigate('MainScreen');
                                  this.setState({username: '', password: ''});
                                }
                              } else {
                                this.setState({
                                  ErrorMessage: responseJson.errors.error_text,
                                  isModalVisible: true,
                                  isLoading: false,
                                });
                              }
                            },
                          );
                        } else {
                          this.setState({
                            ErrorMessage: 'Something went wrong..!',
                            isLoading: false,
                            isErrorModalOpen: true,
                          });
                        }
                      })
                      .catch(e => {
                        console.log(e);

                        this.setState({
                          ErrorMessage: 'Something went wrong..!',
                          isLoading: false,
                          isErrorModalOpen: true,
                        });
                      });
                  } else {
                    this.setState({isLoading: false, isErrorModalOpen: true});
                  }
                },
              );
            }
          } else {
            this.setState(
              {
                ErrorMessage: responseJson.errors.error_text,
                isModalVisible: true,
                isLoading: false,
              },
              () => {
                console.log('error msg:', this.state.ErrorMessage);
              },
            );
          }
        })
        .catch(error => {
          console.log(error);
          this.setState({isLoading: false, isErrorModalOpen: true});
        });
    }
  }

  validate(username, password) {
    let valid = true;
    if (username.length === 0) {
      valid = false;
      this.setState({badEmail: true});
    } else {
      this.setState({badEmail: false});
    }

    if (password.length < 5) {
      valid = false;
      this.setState({badPassword: true});
    } else {
      this.setState({badPassword: false});
    }

    return valid;
  }
  verify() {
    this.setState({isVerifyLoading: true});
    fetch(url.url2 + '/aj/startup/verify-mobile', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      body: `server_key=${url.server_key}&access_token=${
        this.state.Access_Token
      }&time=${this.state.time}&hashed=${this.state.hash}&otp=${
        this.state.TypedOtp
      }`,
    })
      .then(res => res.json())
      .then(data => {
        console.log(
          data,
          `server_key=${url.server_key}&access_token=${
            this.state.Access_Token
          }&time=${this.state.time}&hashed=${this.state.hash}&otp=${
            this.state.TypedOtp
          }`,
        );

        if (data.status === 200 && data.message != 'OTP incorrect') {
          AsyncStorage.multiSet(
            [
              [Constants.USER_LOGIN_STATUS, 'true'],
              [Constants.USER_ID, this.state.user_id.toString()],
              [Constants.USER_TOKEN, JSON.stringify(this.state.Access_Token)],
            ],
            error => {
              if (error === null) {
                this.setState({isLoading: false});
                fetch(`${url.url}/v1/user/fetch_userdata`, {
                  method: 'POST',
                  headers: {
                    'Content-Type':
                      'application/x-www-form-urlencoded;charset=UTF-8',
                  },
                  body: `server_key=${url.server_key}&access_token=${
                    this.state.Access_Token
                  }`,
                })
                  .then(res => res.json())
                  .then(data => {
                    console.log(data.code === '200', 'username:');
                    this.setState({ModalOpen: false, isVerifyLoading: false});
                    if (data.code === '200') {
                      AsyncStorage.setItem(
                        'UserName',
                        data.data.username,
                        err => {
                          console.log(err, 'errororoororo');
                          if (err === null) {
                            if (data.data.startup_avatar != 1) {
                              this.props.navigation.navigate('AddPhoto');
                            } else if (data.data.startup_info != 1) {
                              // alert('hello')
                              this.props.navigation.navigate('Locality');
                            } else {
                              Toast.show('Login Successfully..!', Toast.SHORT);
                              this.props.navigation.navigate('MainScreen');
                              this.setState({username: '', password: ''});
                            }
                          } else {
                            this.setState({
                              ErrorMessage: responseJson.errors.error_text,
                              isModalVisible: true,
                              isLoading: false,
                            });
                          }
                        },
                      );
                    } else {
                      this.setState({
                        ErrorMessage: 'Something went wrong..!',
                        isLoading: false,
                        isErrorModalOpen: true,
                      });
                    }
                  })
                  .catch(e => {
                    console.log(e);

                    this.setState({
                      ErrorMessage: 'Something went wrong..!',
                      isLoading: false,
                      isErrorModalOpen: true,
                    });
                  });
              } else {
                this.setState({isLoading: false, isErrorModalOpen: true});
              }
            },
          );
        } else {
          this.setState(
            {
              SuccesMessage: '',
              ErrorMessage: data.message,
              isModalVisible: true,
              isVerifyLoading: false,
            },
            () => {
              console.log('error msg:', this.state.ErrorMessage);
            },
          );
        }
      })
      .catch(e => {
        this.setState({
          isVerifyLoading: false,
          isErrorModalOpen: true,
        });
      });
  }

  render() {
    const {LoginLabel, InputView, LoginContainer, LoginButton, error} = Styles;
    return this.state.isAutoLoading ? (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image
          style={{height: 100, width: 156}}
          source={require('../../../assets/Images/logo.png')}
        />
        <View style={{marginVertical: height(5)}}>
          <ActivityIndicator size="large" color="blue" />
        </View>
      </View>
    ) : (
      <View style={{flex: 1}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
          style={{flex: 1, justifyContent: 'center'}}>
          <View style={LoginContainer}>
            <ImageBackground
              source={{uri: url.url2 + '/media/img/logo.png'}}
              style={{
                height: height(9),
                width: height(9) * 1.4,
                alignSelf: 'center',
              }}
            />
            <Text style={LoginLabel}>Login</Text>
            <View style={InputView}>
              <Econ name="envelope" size={12} color="rgba(237,56,51,1)" />
              <TextInput
                value={this.state.username}
                onChangeText={text => this.setState({username: text})}
                style={{
                  marginLeft: 15,
                  height: height(6),
                  width: width(78),
                  fontFamily: 'OpenSans-Regular',
                  fontSize: 11,
                }}
                placeholder="Username or email"
              />
            </View>
            {this.state.badEmail ? (
              <Text style={error}>Enter Correct Email</Text>
            ) : null}

            <View style={[InputView, {marginTop: height(2)}]}>
              <Econ name="lock" size={15} color="rgba(237,56,51,1)" />
              <TextInput
                value={this.state.password}
                onChangeText={text => this.setState({password: text})}
                secureTextEntry
                style={{
                  marginLeft: 15,
                  height: height(6),
                  width: width(78),
                  fontFamily: 'OpenSans-Regular',
                  fontSize: 11,
                }}
                placeholder="Your password"
              />
            </View>
            {this.state.badPassword ? (
              <Text style={error}>
                Password must be 8 or more character long
              </Text>
            ) : null}

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ForgotPassword')}>
              <Text
                style={{
                  textAlign: 'right',
                  fontSize: 11,
                  fontFamily: 'Roboto-Bold',
                  marginTop: 20,
                  color: 'rgba(69,183,221,1)',
                }}>
                Forgot your password?
              </Text>
            </TouchableOpacity>

            {/* <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                                style={{ height: 40, width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 40, borderRadius: 20, flexDirection: 'row' }}>
                                <Econ name="facebook" size={12} color="white" />
                                <Text style={{ fontSize: 12, color: 'white', fontFamily: 'OpenSans-Regular', marginLeft: 10 }}>
                                    Facebook
                            </Text>
                            </LinearGradient> */}

            <TouchableOpacity onPress={() => this.Login()} style={LoginButton}>
              {this.state.isLoading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Econ1 name="arrow-right" size={16} color="white" />
              )}
            </TouchableOpacity>

            <Modal isVisible={this.state.isModalVisible}>
              <View
                style={{
                  height: width(45),
                  width: width(45),
                  alignSelf: 'center',
                  backgroundColor: 'white',
                  alignItems: 'center',
                  borderRadius: 5,
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    marginTop: height(2),
                    fontSize: totalSize(2),
                    color: 'black',
                  }}>
                  Oooops!
                </Text>
                <View
                  style={{
                    width: width(15),
                    backgroundColor: 'grey',
                    height: 1,
                    marginTop: height(1.5),
                  }}
                />
                <Text
                  style={{
                    fontSize: totalSize(1.5),
                    fontWeight: 'bold',
                    color: 'rgba(0,0,0,0.3)',
                    marginVertical: height(1.5),
                    textAlign: 'center',
                    paddingHorizontal: 8,
                  }}>
                  {this.state.ErrorMessage}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({isModalVisible: !this.state.isModalVisible});
                  }}
                  style={{
                    height: height(5),
                    width: width(30),
                    backgroundColor: 'red',
                    marginTop: 15,
                    borderRadius: 3,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  activeOpacity={1}>
                  <Text style={{fontSize: totalSize(1.5), color: 'white'}}>
                    Close
                  </Text>
                </TouchableOpacity>
              </View>
            </Modal>

            <Modal isVisible={this.state.isErrorModalOpen}>
              <View
                style={{
                  height: width(45),
                  width: width(45),
                  alignSelf: 'center',
                  backgroundColor: 'white',
                  alignItems: 'center',
                  borderRadius: 5,
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    marginTop: height(2),
                    fontSize: totalSize(2),
                    color: 'black',
                  }}>
                  Oooops!
                </Text>
                <View
                  style={{
                    width: 20,
                    backgroundColor: 'grey',
                    height: 1,
                    marginTop: height(1.5),
                  }}
                />
                <Text
                  style={{
                    fontSize: totalSize(1.5),
                    fontWeight: 'bold',
                    color: 'rgba(0,0,0,0.3)',
                    marginTop: 10,
                    textAlign: 'center',
                    paddingHorizontal: 8,
                  }}>
                  {this.state.ErrorMessage}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      isErrorModalOpen: !this.state.isErrorModalOpen,
                    });
                  }}
                  style={{
                    height: height(5),
                    width: width(30),
                    backgroundColor: 'red',
                    marginTop: 15,
                    borderRadius: 3,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  activeOpacity={1}>
                  <Text style={{fontSize: totalSize(1.5), color: 'white'}}>
                    Close
                  </Text>
                </TouchableOpacity>
              </View>
            </Modal>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                marginTop: 20,
                fontFamily: 'OpenSans-Regular',
                fontSize: 13,
              }}>
              Don't have an account ? Sign up now!
            </Text>
          </TouchableOpacity>
        </LinearGradient>
        <Modal
          isVisible={this.state.ModalOpen}
          animationIn="slideInUp"
          animationInTiming={1000}
          animationOut="slideOutDown"
          animationOutTiming={1000}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <View
              style={{
                height: 310,
                width: 300,
                backgroundColor: 'white',
                borderRadius: 8,
              }}>
              <ImageBackground
                source={require('../../../assets/Images/modal.png')}
                style={{height: 110, width: '100%', marginTop: 20}}
              />

              <Text
                style={{
                  marginTop: 20,
                  textAlign: 'center',
                  fontSize: 9,
                  fontFamily: 'OpenSans-Bold',
                  color: 'rgba(237,56,51,0.80)',
                }}>
                Please enter the OTP you have received on your email
              </Text>

              <View
                style={{
                  height: 60,
                  width: '90%',
                  alignSelf: 'center',
                  marginTop: 15,
                }}>
                <OtpInputs
                  handleChange={code =>
                    this.setState({TypedOtp: code}, () =>
                      console.log(
                        'otp:',
                        code,
                        this.state.TypedOtp,
                        this.state.otp,
                      ),
                    )
                  }
                  defaultValue={''}
                  numberOfInputs={4}
                  focusedBorderColor="rgba(237,56,51,1)"
                  unfocusedBorderColor="rgba(69,183,221,1)"
                  inputStyles={{color: 'rgba(237,56,51,1)'}}
                />
              </View>

              <TouchableOpacity
                style={{flexDirection: 'row', alignSelf: 'center'}}>
                <TouchableOpacity
                  onPress={() => {
                    //have to remove after somethime
                    //this.props.navigation.navigate('AddPhoto')

                    this.verify();
                    // this.setState({ModalOpen:false})
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                    colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                    style={{
                      height: 40,
                      width: 100,
                      borderRadius: 20,
                      justifyContent: 'center',
                      alignSelf: 'center',
                      marginVertical: 15,
                      alignItems: 'center',
                    }}>
                    {this.state.isVerifyLoading ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <Text
                        style={{
                          fontSize: 11,
                          color: 'white',
                          fontFamily: 'OpenSans-Regular',
                        }}>
                        Submit
                      </Text>
                    )}
                  </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    this.setState({ModalOpen: false, isLoading: false});
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                    colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                    style={{
                      height: 40,
                      width: 100,
                      borderRadius: 20,
                      marginLeft: 20,
                      justifyContent: 'center',
                      alignSelf: 'center',
                      marginVertical: 15,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 11,
                        color: 'white',
                        fontFamily: 'OpenSans-Regular',
                      }}>
                      Cancel
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
