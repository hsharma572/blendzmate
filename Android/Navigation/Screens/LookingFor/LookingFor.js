import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Styles from './style';
import { NavigationConstants} from "../../commonValues"

export default class LookingFor extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { MainContainer } = Styles;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={MainContainer}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 1 }} 
                        colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.70)']}
                        style={{ height: 70, width: '100%', padding: 20, alignItems: 'center', justifyContent: 'space-between', borderTopLeftRadius: 8, borderTopRightRadius: 8 }}>
                        <Text style={{ fontSize: 18, color: 'white', letterSpacing: 1, fontFamily: 'Roboto-Bold' }}>What are you looking for ?</Text>
                    </LinearGradient>

                    <View style={{ flexDirection: 'row',marginTop:40, alignItems: 'center',justifyContent:'center' ,width:'100%',height:40}}>
                        <TouchableOpacity
                        onPress={()=>{NavigationConstants.locationSelected="live";this.props.navigation.navigate('Locality');}}
                            style={{ height: '100%', width: 110, alignItems: 'center', justifyContent: 'center', borderRadius: 20, alignSelf: 'center',borderWidth:1,borderColor:'rgba(237,56,51,1)' }}>
                            <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'rgba(0,0,0,0.5)' }}>A place to Live</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                        onPress={()=>{NavigationConstants.locationSelected="room";this.props.navigation.navigate('Locality');}}
                                style={{ height: '100%', width: 110, alignItems: 'center', justifyContent: 'center',marginLeft:20,borderWidth:1,borderColor:'rgba(237,56,51,1)', borderRadius: 20, alignSelf: 'center' }}>
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'rgba(0,0,0,0.5)' }}>List a room</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
