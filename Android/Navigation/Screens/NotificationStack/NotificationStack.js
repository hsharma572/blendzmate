import React, {Component} from 'react';
import {
  View,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  BackHandler,
  Image,
  FlatList,
  Text 
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import {NavigationConstants} from '../../commonValues';
import { width, height, totalSize } from 'react-native-dimension';
import {Constants} from '../../Component/Contants';

export default class NotificationStack extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      isLoading: true,
      lat: null,
      long: null,
      access_Token: '',
      HtmlView: '',
      user_name: '',
      backButtonEnabled: false,
      resultData:[]
    };
  }



 componentDidMount() {
    
    this.getNotification();
   
  }
  getNotification=()=>{
    console.log('returnData2',NavigationConstants.accessToken,url.server_key);
    fetch(`${url.url}/v1/user/fetch_notifications`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${NavigationConstants.accessToken}&server_key=${url.server_key}`
  })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log('returnData2', responseJson.data);
          this.setState({ resultData: responseJson.data,isLoading: false })
      })
      .catch((error) => {
        console.log('returnData2', error);
      })

  }


  renderLoading = () => (
    <ActivityIndicator animating size="small" color="blue" style={{flex: 1}} />
  );

  render() {
    return this.state.isLoading ? (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="small" color="blue" animating />
      </View>
    ) : (
      <View style={{flex: 1}}>
         <FlatList
        data={this.state.resultData}
        renderItem={({ item }) => (
         <View style={{flexDirection:'row',backgroundColor:'white',borderTopWidth:0.6,borderTopColor:'#cfcfcf',borderRadius:2,}}>
            <View style={{width:width(16),justifyContent:'center',alignItems:'center',paddingVertical:height(0.5)}}>
              <Image source={{ uri: item.avatar }} style={{width:40,height:40,borderRadius:30}} />
            </View>
            <View style={{paddingVertical:height(1.5),paddingHorizontal:width(2)}}>
                <Text style={{fontSize:totalSize(1.6),color:item.seen?'#b3b3b3':'black'}}>
                  {item.username} {item.text}
                </Text>
            </View>
         </View>
        )}
        keyExtractor={item => item.id}
        extraData={this.state.resultData}
      />
      </View>
    );
  }
}

