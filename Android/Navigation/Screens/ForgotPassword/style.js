import Color from '../../../assets/Colors/Colors';
import { width, height, totalSize } from 'react-native-dimension';
const Styles = {
    LoginLabel:{
        //marginTop:20,
        fontFamily: 'Roboto-Bold',
        fontSize: 20,
        color:'rgba(237,56,51,1)',
        marginLeft:5
    },
    InputView:{
        width:width(76),
        height:45,
        backgroundColor:'white',
        marginTop:20,
        borderRadius: 20,
        flexDirection: 'row',
        elevation:2,
        paddingHorizontal: width(6),
        alignItems: 'center',
        shadowOffset: { width: 3, height: 3 },
        shadowColor: '#8a795d',
        shadowOpacity: 0.2,
    },
    LoginContainer:{
        height:width(82),
        width:width(88),
        backgroundColor:'white',
        alignSelf: 'center',
        padding: 20,
        elevation:5,
        borderRadius:8
    },
    LoginButton:{
        height:50,
        width:50,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius: 40,
        position:'absolute',
        right:25,
        bottom:25,
        backgroundColor:'rgba(69,183,221,1)'
    },
    error: {
        fontSize: 11,
        color: 'red',
        marginTop:3,
        marginLeft:10
    }
}

export default Styles;