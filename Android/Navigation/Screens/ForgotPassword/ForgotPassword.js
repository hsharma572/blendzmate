import React, { Component } from 'react';
import { View, Text, ImageBackground, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import Econ1 from 'react-native-vector-icons/FontAwesome5';
import url from '../../Component/url';
import Modal from 'react-native-modal';
import { width,height,totalSize } from 'react-native-dimension';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      badEmail: false,
      isLoading: false,
      SuccesMessage: '',
      isModalVisible: false,
      isErrorModalOpen: false
    };
  }

  validate(email) {
    let valid = true;
    if (reg.test(email) === false) {
      valid = false;
      this.setState({ badEmail: true })
    } else {
      this.setState({ badEmail: false })
    }
    return valid;
  }

  ResetPassword() {
    const { email } = this.state;
    if (this.validate(email)) {
      this.setState({ isLoading: true })
      fetch(`${url.url}/v1/auth/forget`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        //timeout: 2000,
        body: `email=${email}&server_key=${url.server_key}`
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('forgot password response:', responseJson);
          if (responseJson.code === '200') {
            this.setState({ SuccesMessage: responseJson.message, isModalVisible: true, isLoading: false }, () => {
              console.log('success message', this.state.SuccessMessage);

            })
          }
          else {
            this.setState({ ErrorMessage: responseJson.errors.error_text, isModalVisible: true, isLoading: false }, () => {
              console.log('error msg:', this.state.ErrorMessage);
            })
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false, isErrorModalOpen: true })
        })
    }
  }


  render() {
    const { LoginLabel, InputView, LoginContainer, LoginButton, error } = Styles;
    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}
          colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']} style={{ flex: 1, justifyContent: 'center' }}>

          <View style={LoginContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              <ImageBackground
                source={{ uri: url.url2+'/media/img/logo.png' }}
                style={{ height: 50, width: 70, alignSelf: 'center' }}
              />
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack()}
                style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Econ1 name="arrow-left" size={12} color="rgba(237,56,51,1)" />
                <Text style={[LoginLabel, { fontSize: 13 }]}>Sign In</Text>
              </TouchableOpacity>
            </View>

            <Text style={[LoginLabel, { marginTop: 20 }]}>Forgot Password</Text>
            <View style={[InputView, { marginTop: 40 }]}>
              <Econ name="envelope" size={12} color="rgba(237,56,51,1)" />
              <TextInput
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
                style={{ marginLeft: 15, height: 40, width: 200, fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                placeholder="email"
              />
            </View>
            {
              this.state.badEmail ? <Text style={error}>Enter Correct Email</Text> : null
            }

            <TouchableOpacity onPress={() => this.ResetPassword()}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}
                colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                style={{ height: 40, width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 30, borderRadius: 20, flexDirection: 'row' }}>
                {
                  this.state.isLoading
                    ?
                    <ActivityIndicator size="small" color="white" />
                    :
                    <Text style={{ fontSize: 12, color: 'white', fontFamily: 'OpenSans-Regular', marginLeft: 10 }}>
                      Reset Password
                    </Text>
                }
              </LinearGradient>
            </TouchableOpacity>

            <Modal isVisible={this.state.isModalVisible}>
              <View
                style={{ height: width(45), width: width(45), alignSelf: 'center', backgroundColor: 'white',  alignItems: 'center',borderRadius:5 }} >
                <View style={{marginTop:height(2)}}>
                <Econ style={{ alignSelf: 'center' }} name={this.state.SuccesMessage ? "check-circle" : "times-circle"} size={totalSize(3)} color={this.state.SuccesMessage ? 'green' : 'red'} />
                </View>
        
                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: totalSize(2), color: 'black' }}>{this.state.SuccesMessage ? 'Success...' : 'Oooops!'}</Text>
                <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                <Text style={{ fontSize: totalSize(1.5), fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center',paddingHorizontal:8 }}>{this.state.SuccesMessage ? this.state.SuccesMessage : this.state.ErrorMessage}</Text>
                <TouchableOpacity
                  onPress={() => this.setState({ email: '', isModalVisible: !this.state.isModalVisible })}
                  style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                  activeOpacity={1}>
                  <Text style={{ fontSize: totalSize(1.5), color: 'white' }}>Close</Text>
                </TouchableOpacity>
              </View>
            </Modal>

            <Modal isVisible={this.state.isErrorModalOpen}>
              <View
                style={{  height: width(45), width: width(45), alignSelf: 'center', backgroundColor: 'white',  alignItems: 'center',borderRadius:5 }} >

                {/* <Econ style={{ alignSelf: 'center' }} name="times-circle" size={34} color={this.state.SuccesMessage?'green':'red'} /> */}
                <View style={{marginTop:height(2)}}>
                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: totalSize(2), color: 'black' }}>Oooops!</Text>
                </View>
                <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                <Text style={{ fontSize: totalSize(1.5), fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center',paddingHorizontal:8 }}>Something Went Wrong..!</Text>
                <TouchableOpacity
                  onPress={() => { this.setState({ isErrorModalOpen: !this.state.isErrorModalOpen }) }}
                  style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                  activeOpacity={1}>
                  <Text style={{ fontSize: totalSize(1.5), color: 'white' }}>Close</Text>
                </TouchableOpacity>
              </View>
            </Modal>

          </View>

        </LinearGradient>
      </View>
    );
  }
}
