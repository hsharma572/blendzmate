import React, { Component } from 'react';
import { View, Text, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';
import Styles from './style';
import url from '../../Component/url';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import Colors from '../../../assets/Colors/Colors';
import NotificationList from '../../Component/NotificationList';
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      access_Token: '',
      isLoading: true,
      NotificationsList: '',
      isModalVisible: false
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
      if (error === null) {
        this.setState({ access_Token: result.replace(/"/g, '').trim() }, () => this.get_Notifications())
      }
      else {
        alert('Something Went wrong...!!')
      }
    })
  }

  get_Notifications() {
    this.setState({ isLoading: true })
    const { access_Token } = this.state
    fetch(`${url.url}/v1/user/fetch_notifications`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user notificatins response:', responseJson);
        if (responseJson.code === '200') {
          this.setState({ NotificationsList: responseJson.data, isLoading: false })
        }
        else {
          this.setState({ isLoading: false, isModalVisible: true })
        }

      })
      .catch((error) => {
        this.setState({ isLoading: false, isModalVisible: true })
        //alert('suggestion catch alert', error)
      })
  }

  render() {
    return (
      this.state.isLoading
        ?
        <ActivityIndicator size='small' color={Colors.ButtonColor} style={{ flex: 1 }} />
        :
        <View style={{ flex: 1, backgroundColor: Colors.PrimaryBackGroundColor }}>

          <View style={{
            height: 45,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: "center", paddingHorizontal: 15, backgroundColor: Colors.PrimaryBackGroundColor
          }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-left' size={15} color='black' />
            </TouchableOpacity>
            <Text style={{ fontSize: 15, color: 'black', fontWeight: 'bold' }}>Notifications</Text>
            <View></View>
          </View>

          {this.state.NotificationsList.length === 0
            ?
            <View style={{ flex: 1, backgroundColor: Colors.PrimaryBackGroundColor, justifyContent:'center', alignItems:'center' }}>
              <Text style={{ fontSize: 13, color: 'black' }}>No data found</Text>
            </View>
            :
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.NotificationsList}
              renderItem={(data) => {
                return <NotificationList data={data} />
              }}
            />
          }

          <Modal isVisible={this.state.isModalVisible}>
            <View
              style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >
              <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
              <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
              <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
              <TouchableOpacity
                onPress={() => { this.setState({ isModalVisible: !this.state.isModalVisible }) }}
                style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                activeOpacity={1}>
                <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
              </TouchableOpacity>
            </View>
          </Modal>
        </View>
    );
  }
}
