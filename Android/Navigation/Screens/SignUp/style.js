import Color from '../../../assets/Colors/Colors';
import { width, height, totalSize } from 'react-native-dimension';
const Styles = {
    LoginLabel:{
        //marginTop:20,
        fontFamily: 'Roboto-Bold',
        fontSize: 14,
        marginLeft: 7,
        color:'rgba(237,56,51,1)'
    },
    InputView:{
        width:width(76),
        height:45,
        backgroundColor:'white',
        marginTop:20,
        borderRadius: 20,
        flexDirection: 'row',
        elevation:2,
        paddingHorizontal: width(6),
        alignItems: 'center',
        shadowOffset: { width: 3, height: 3 },
        shadowColor: '#8a795d',
        shadowOpacity: 0.2,
    },
    LoginContainer:{
       
        width:width(88),
        backgroundColor:'white',
        alignSelf: 'center',
        padding: 20,
        elevation:5,
        borderRadius:8
    },
    LoginButton:{
        height:50,
        width:50,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius: 40,
        position:'absolute',
        right:25,
        bottom:25,
        backgroundColor:'rgba(69,183,221,1)'
    },
    TxtInput:{
        height:'100%',width:'22%',fontSize:30,backgroundColor:'white', borderBottomWidth:2,borderBottomColor:'rgba(237,56,51,0.80)',textAlign:'center'
    },
    error: {
        fontSize: 11,
        color: 'red',
        marginTop:3,
        marginLeft:10
    }
}

export default Styles;