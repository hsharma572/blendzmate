import React, { Component } from 'react';
import { View, Text, ImageBackground, TextInput, TouchableOpacity, Linking, ActivityIndicator } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import Econ1 from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import OtpInputs from 'react-native-otp-inputs';
import url from '../../Component/url';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import { width, height, totalSize } from 'react-native-dimension';
import {NavigationConstants} from '../../commonValues'
const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
import CheckBox from 'react-native-check-box'

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AgreeTerms: false,
            secureTextEntry: true,
            MaleSelected: true,
            FemaleSelected: false,
            ModalOpen: false,
            username: '',
            email: '',
            password: '',
            conf_password: '',
            gender: 'Male',
            badUsername: false,
            badEmail: false,
            badPassword: false,
            matchPassword: false,
            readTerms: false,
            isLoading: false,
            ServerKey: '1539874186',
            SuccesMessage: '',
            ErrorMessage: '',
            isModalVisible: false,
            isErrorModalOpen: false,
            otp: '',
            hash:'',
            time:'',
            TypedOtp: '',
            Access_Token: '',
            isVerifyLoading: false
        };
    }

    SignUp() {
        const { username, email, password, conf_password, gender, AgreeTerms } = this.state;
        if (this.validate(username, email, password, conf_password, AgreeTerms)) {
            this.setState({ isLoading: true })
            fetch(`${url.url}/v1/auth/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                },
                //timeout: 2000,
                body: `username=${username}&email=${email}&password=${password}&conf_password=${conf_password}&server_key=${url.server_key}&device_id=${NavigationConstants.onesingalUserid}`
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sign up response:', responseJson);
                    this.setState({ isLoading: false })
                    //have to remove after somethime
                    // this.setState({ ModalOpen: true })
                    if (responseJson.code === '200') {
                        AsyncStorage.multiSet(
                            [
                                [Constants.USER_LOGIN_STATUS, "true"],
                                [Constants.USER_ID, responseJson.data.user_id.toString()],
                                [Constants.USER_TOKEN, JSON.stringify(responseJson.data.access_token)],
                            ], (err, res) => {
                                if (err === null) {
                                    console.log(responseJson,'SignUp');
                                    this.setState(
                                        {
                                            SuccesMessage: responseJson.data.message,
                                            otp: responseJson.data.otp,
                                            Access_Token: responseJson.data.access_token,
                                            hash: responseJson.data.hash,
                                            time: responseJson.data.time,
                                        }, () => {
                                            if (this.state.otp) {
                                                this.setState({ ModalOpen: true })
                                            }
                                        })
                                }
                                else {
                                    this.setState({ isModalVisible: false, isErrorModalOpen: true })
                                }
                            })

                    }
                    else {

                        this.setState({ SuccesMessage: '', ErrorMessage: responseJson.errors.error_text, isModalVisible: true }, () => {
                            console.log('error msg:', this.state.ErrorMessage, );
                        })
                    }
                })
                .catch((error) => {
                   console.log(error);
                    this.setState({ isLoading: false, isErrorModalOpen: true })
                })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };


    validate(username, email, password, conf_password, terms) {
        let valid = true;
        console.log(username.length);
        console.log(password, conf_password)
        if (username.length < 3 || username.length > 17) {
            valid = false;
            this.setState({ badUsername: true })
        } else {
            this.setState({ badUsername: false })
        }

        if (reg.test(email) === false) {
            valid = false;
            this.setState({ badEmail: true })
        } else {
            this.setState({ badEmail: false })
        }

        if (password.length <= 8) {
            valid = false;
            this.setState({ badPassword: true })
        } else {
            this.setState({ badPassword: false })
        }

        if (!(password === conf_password)) {
            valid = false;
            this.setState({ matchPassword: true })
        } else {
            this.setState({ matchPassword: false })
        }

        if (!terms) {
            valid = false;
            this.setState({ readTerms: true })
        } else {
            this.setState({ readTerms: false })
        }

        return valid;
    }

    verify() {
        this.setState({ isVerifyLoading: true })
        fetch(url.url2+'/aj/startup/verify-mobile', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: `server_key=${url.server_key}&access_token=${this.state.Access_Token}&time=${this.state.time}&hashed=${this.state.hash}&otp=${this.state.TypedOtp}`
        })
            .then(res => res.json())
            .then(data => {
                console.log(data,`server_key=${url.server_key}&access_token=${this.state.Access_Token}&time=${this.state.time}&hashed=${this.state.hash}&otp=${this.state.otp}`)
                
                if (data.status === 200 &&data.message !="OTP incorrect" ) {
                    this.setState(
                        {
                            isVerifyLoading: false,
                            ModalOpen: false
                        },
                        () => this.props.navigation.navigate('AddPhoto'))
                }
                else {
                 
                        this.setState({ SuccesMessage: '', ErrorMessage: data.message, isModalVisible: true,isVerifyLoading: false, }, () => {
                            console.log('error msg:', this.state.ErrorMessage, );
                        })
                }
            })
            .catch(e => {
                this.setState(
                    {
                        isVerifyLoading: false,
                        isErrorModalOpen: true
                    })
            })
    }

    render() {
        const { LoginLabel, InputView, LoginContainer, error } = Styles;
        return (
            <View style={{ flex: 1 }}>
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 1 }}
                    colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']} style={{ flex: 1, justifyContent: 'center' }}>

                    <View style={LoginContainer}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <ImageBackground
                                source={{ uri: url.url2+'/media/img/logo.png' }}
                                style={{ height: height(6), width: 70, alignSelf: 'center' }}
                            />
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                                style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Econ1 name="arrow-left" size={12} color="rgba(237,56,51,1)" />
                                <Text style={LoginLabel}>Sign In</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={[InputView, { marginTop: 20 }]}>
                            <TextInput
                                onChangeText={(text) => this.setState({ username: text })}
                                style={{ height: height(6), width: width(78), fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                placeholder="Username"
                            />
                        </View>
                        {
                            this.state.badUsername ? <Text style={error}>Enter Correct Username</Text> : null
                        }

                        <View style={[InputView, { marginTop: 20 }]}>
                            <TextInput
                                onChangeText={(text) => this.setState({ email: text })}
                                style={{ height: height(6), width: width(78), fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                placeholder="E-mail address"
                            />
                        </View>
                        {
                            this.state.badEmail ? <Text style={error}>Enter Correct Email</Text> : null
                        }

                        <View style={[InputView, { marginTop: 20 }]}>
                            <TextInput
                                onChangeText={(text) => this.setState({ password: text })}
                                secureTextEntry={this.state.secureTextEntry}
                                style={{ height: height(6), width: width(78), fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                placeholder="password" 
                            />
                        </View>
                        {
                            this.state.badPassword ? <Text style={error}>Password must be 8 or more character long</Text> : null
                        }

                        <View style={[InputView, { marginTop: 20 }]}>
                            <TextInput
                                onChangeText={(text) => this.setState({ conf_password: text })}
                                secureTextEntry={true}
                                style={{ height: height(6), width: width(62), fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                placeholder="Confirm Password"
                            />
                            <TouchableOpacity onPress={() => this.setState({ secureTextEntry: !this.state.secureTextEntry })}>
                                <Econ1 name={this.state.secureTextEntry ? 'eye-slash' : "eye"} size={14} color={this.state.secureTextEntry ? "rgba(0,0,0,0.4)" : "rgba(237,56,51,1)"} />
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.matchPassword ? <Text style={error}>Password & Confirm Password Not Matched</Text> : null
                        }

                        <TouchableOpacity style={{ height: height(5), width: '100%', backgroundColor: 'white', alignItems: 'center', marginTop: 20, borderRadius: 20, borderWidth: 1, borderColor: 'rgba(0,0,0,0.4)', flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ MaleSelected: !this.state.MaleSelected, FemaleSelected: !this.state.FemaleSelected, gender: 'Male' }, () => {
                                        console.log('data:', this.state)
                                    })
                                }}
                                style={{
                                    height: '100%',
                                    width: '50%',
                                    backgroundColor: this.state.MaleSelected ? 'rgba(237,56,51,0.80)' : 'white',
                                    justifyContent: 'center', alignItems: 'center',
                                    borderTopLeftRadius: 20, borderBottomLeftRadius: 20
                                }}>
                                <Text style={{ fontSize: 12, fontFamily: 'OpenSans-Regular', color: this.state.MaleSelected ? 'white' : 'rgba(0,0,0,0.4)' }}>Male</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ FemaleSelected: !this.state.FemaleSelected, MaleSelected: !this.state.MaleSelected, gender: 'Female' }, () => {
                                        console.log('state data:', this.state)
                                    })
                                }}
                                style={{
                                    height: '100%',
                                    width: '50%',
                                    backgroundColor: this.state.FemaleSelected ? 'rgba(69,183,221,0.80)' : 'white',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderTopRightRadius: 20, borderBottomRightRadius: 20
                                }}>
                                <Text style={{ fontSize: 12, fontFamily: 'OpenSans-Regular', color: this.state.FemaleSelected ? 'white' : 'rgba(0,0,0,0.4)' }}>Female</Text>
                            </TouchableOpacity>
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignItems: 'center',marginTop:height(1),paddingLeft:5 }}>
                            <CheckBox
                                style={{  }}
                                isChecked={this.state.AgreeTerms}
                                onClick={() => this.setState({ AgreeTerms: !this.state.AgreeTerms })}
                            />
                            <View style={{ marginLeft: 5,justifyContent:'center' }}>
                             <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', }}>I agree to the</Text>
                            </View>
                            
                            <TouchableOpacity onPress={()=>{
                                let url="https://blendzmate.com/terms-of-use";
                                Linking.canOpenURL(url).then(supported => {
                                if (supported) {
                                    Linking.openURL(url);
                                } else {
                                    console.log("Don't know how to open URI: " + this.props.url);
                                }
                                });
                            }} style={{ marginLeft: 5,justifyContent:'center' }}><Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'rgba(69,183,221,1)' }}>Terms of use</Text></TouchableOpacity>

                            <Text> &</Text>

                            <TouchableOpacity onPress={()=>{
                                let url="https://blendzmate.com/privacy-and-policy";
                                Linking.canOpenURL(url).then(supported => {
                                if (supported) {
                                    Linking.openURL(url);
                                } else {
                                    console.log("Don't know how to open URI: " + this.props.url);
                                }
                                });
                            }} style={{ marginLeft: 5,justifyContent:'center' }}><Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'rgba(69,183,221,1)' }}>Privacy Policy</Text></TouchableOpacity>
                        </View>
                        {
                            this.state.readTerms ? <Text style={error}>please select Terms of use</Text> : null
                        }

                        <TouchableOpacity
                            style={{
                                height: height(5),
                                width: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                              marginTop:height(3),
                               
                                alignSelf: 'center',
                                borderRadius: 20, flexDirection: 'row'
                            }}
                            // this.SignUp(this.state.username, this.state.email, this.state.password, this.state.conf_password, this.state.AgreeTerms)
                            onPress={() => this.SignUp()}>

                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                                style={{ height: '100%', width: '100%', borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                                {
                                    this.state.isLoading
                                        ?
                                        <ActivityIndicator size="small" color="white" />
                                        :
                                        <Text style={{ fontSize: 12, color: 'white', fontFamily: 'OpenSans-Regular', marginLeft: 10 }}>
                                            Sign up
                                        </Text>
                                }
                            </LinearGradient>
                        </TouchableOpacity>

                        <Modal isVisible={this.state.isModalVisible}>
                            <View
                                style={{  height: width(50), width: width(50), alignSelf: 'center', backgroundColor: 'white', alignItems: 'center',borderRadius:5 }} >
                                 <View style={{marginTop:height(2)}}>
                                 <Econ style={{ alignSelf: 'center' }} name={this.state.SuccesMessage ? "check-circle" : "times-circle"} size={totalSize(3)} color={this.state.SuccesMessage ? 'green' : 'red'} />
                                 </View>
                                
                                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: totalSize(2), color: 'black' }}>{this.state.SuccesMessage ? 'Success...' : 'Oooops!'}</Text>
                                <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                                <Text style={{ fontSize: totalSize(1.5), fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center',paddingHorizontal:8 }}>{this.state.SuccesMessage ? this.state.SuccesMessage : this.state.ErrorMessage}</Text>
                                <TouchableOpacity
                                    onPress={this.toggleModal}
                                    style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                                    activeOpacity={1}>
                                    <Text style={{ fontSize: totalSize(1.5), color: 'white' }}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>

                        <Modal isVisible={this.state.isErrorModalOpen}>
                            <View
                                style={{  height: width(50), width: width(50), alignSelf: 'center', backgroundColor: 'white',  alignItems: 'center',borderRadius:5 }} >

                                {/* <Econ style={{ alignSelf: 'center' }} name="times-circle" size={34} color={this.state.SuccesMessage?'green':'red'} /> */}
                                <View style={{marginTop:height(2)}}>
                                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: totalSize(2), color: 'black' }}>Oooops!</Text>
                                </View>
                                <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                                <Text style={{ fontSize: totalSize(1.5), fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center',paddingHorizontal:8 }}>Something Went Wrong..!</Text>
                                <TouchableOpacity
                                    onPress={() => { this.setState({ isErrorModalOpen: !this.state.isErrorModalOpen }) }}
                                    style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                                    activeOpacity={1}>
                                    <Text style={{ fontSize: totalSize(1.5), color: 'white' }}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>
                    </View>

                    <Modal isVisible={this.state.ModalOpen} animationIn='slideInUp' animationInTiming={1000} animationOut='slideOutDown' animationOutTiming={1000} >
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ height: 310, width: 300, backgroundColor: 'white', borderRadius: 8 }}>
                                <ImageBackground
                                    source={require('../../../assets/Images/modal.png')} style={{ height: 110, width: '100%', marginTop: 20 }}>
                                </ImageBackground>

                                <Text style={{ marginTop: 20, textAlign: 'center', fontSize: 9, fontFamily: 'OpenSans-Bold', color: 'rgba(237,56,51,0.80)' }}>
                                    Please enter the OTP you have received on your email
                                </Text>

                                <View style={{ height: 60, width: '90%', alignSelf: 'center', marginTop: 15 }}>
                                    <OtpInputs
                                        handleChange={code => this.setState({ TypedOtp: code }, () => console.log('otp:',code, this.state.TypedOtp, this.state.otp))}
                                        defaultValue={''}
                                        numberOfInputs={4}
                                        focusedBorderColor="rgba(237,56,51,1)"
                                        unfocusedBorderColor="rgba(69,183,221,1)"
                                        inputStyles={{ color: 'rgba(237,56,51,1)' }}
                                    />
                                </View>

                                <TouchableOpacity style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                    <TouchableOpacity onPress={() => {
                                        //have to remove after somethime
                                        //this.props.navigation.navigate('AddPhoto')
                                        if (this.state.otp == (this.state.TypedOtp).replace(/"/g, "")) {
                                           
                                            this.verify()
                                            // this.setState({ModalOpen:false})
                                        }
                                        else {
                                            alert('Your OTP is wrong..!!')
                                        }
                                    }}>
                                        <LinearGradient
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1, y: 1 }}
                                            colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                                            style={{ height: 40, width: 100, borderRadius: 20, justifyContent: 'center', alignSelf: 'center', marginVertical: 15, alignItems: 'center' }}>
                                            {
                                                this.state.isVerifyLoading
                                                    ?
                                                    <ActivityIndicator size='small' color='white' />
                                                    :
                                                    <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular' }}>Submit</Text>
                                            }
                                        </LinearGradient>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.setState({ ModalOpen: false })}>
                                        <LinearGradient
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1, y: 1 }}
                                            colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                                            style={{ height: 40, width: 100, borderRadius: 20, marginLeft: 20, justifyContent: 'center', alignSelf: 'center', marginVertical: 15, alignItems: 'center' }}>
                                            <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular' }}>Cancel</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </Modal>

                </LinearGradient>

            </View>
        );
    }
}
