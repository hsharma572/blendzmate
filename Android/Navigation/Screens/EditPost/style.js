import Colors from '../../../assets/Colors/Colors';

const Styles = {
    instagramView:{
        width:'100%',
        //height:360,
        backgroundColor:Colors.white,
        marginTop:25,
        borderRadius:5,
        elevation:1
    },
    feedHeader:{
        height:60,
        width:'100%',
        backgroundColor:Colors.white,
        //padding: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius:5,
       // paddingHorizontal: 20,
    },
    header:{
        height: 30, 
        width: '95%', 
        alignSelf: 'center', 
        flexDirection: 'row', 
        justifyContent:'space-between',
        alignItems: 'center'
    },
    headerText:{
        fontFamily: 'Roboto-Bold', 
        fontSize: 13, 
        marginLeft: 15
    },
    timeHeader:{
        marginLeft: 15,
        fontFamily: 'Roboto-Bold', 
        fontSize: 9, 
        color: 'grey'
    },
    commentView:{
        height: 60, paddingHorizontal: 15, justifyContent: 'space-between', flexDirection: 'row'
    },
    commentInput:{
        fontFamily: 'OpenSans-Regular', fontSize: 11, marginLeft: 15, height: 35, width: 230
    },
    LikeText:{
        marginTop: 5, fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'grey'
    },
    LikeView:{
        justifyContent: 'center', alignItems: 'flex-start', height: 70
    }
}

export default Styles;