import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, ImageBackground, TextInput, ActivityIndicator } from 'react-native';
import Colors from '../../../assets/Colors/Colors';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Econ1 from 'react-native-vector-icons/FontAwesome';
import Styles from './style';
import CommentSection from '../../Component/commentSection/commentSection';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import url from '../../Component/url';
import Swiper from 'react-native-swiper';

import Toast from 'react-native-simple-toast';

export default class EditPost extends Component {
  constructor(props) {
    super(props);
    //console.log('comment screen data:', this.props.navigation.state.params.data.data);
    this.state = {
      id: this.props.navigation.state.params.data.data,
      CommentText: '',
      access_Token: '',
      data: [],
      isLoading: false,
      DefaultCommentCount: 3,
      UpdateText: '',
      isUpdateTextLoading: false
    };
  }

  componentWillMount() {
    this.setState({ isLoading: true });
    AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
      if (error === null) {
        this.setState({ access_Token: result.replace(/"/g, '').trim() }, () => this.getSinglePost());

      }
      else {
        alert('Something Went wrong...!!');
      }
    });
  }


  async getSinglePost() {
    console.log('functoin log:');
    const { access_Token } = this.state;
    await fetch(`${url.url}/v1/post/fetch_post_by_id`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${this.state.id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('single post response:', responseJson);
        if (responseJson.code === '200') {
          this.setState({ data: responseJson.data, isLoading: false });
        }
        else {
          this.setState({ isLoading: false });
          alert('something went wrong..!!');
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        alert('catch alert');
      });
  }




  EditPost(id) {
    const { access_Token, UpdateText } = this.state
    this.setState({ isUpdateTextLoading: true })
    fetch(`${url.url}/v1/post/edit_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${id}&text=${UpdateText}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('edit post response:', responseJson);
        if (responseJson.code === '200') {
          Toast.show(responseJson.message, Toast.SHORT);
          // this.getSinglePost()
          this.setState({ isUpdateTextLoading: false },()=>{
            this.props.navigation.goBack()
          })
        }
        else {
          Toast.show(responseJson.errors.error_text, Toast.SHORT);
          this.setState({ isUpdateTextLoading: false })
        }
      })
      .catch((error) => {
        // this.setState({ isFollowSpinner: false, isModalVisible: true })
        //alert('catch alert')
        Toast.show('something went wrong..!!', Toast.SHORT);
        this.setState({ isUpdateTextLoading: false })
      })
  }


  render() {
    const { instagramView, feedHeader, header, headerText, timeHeader, commentView, commentInput, LikeText, LikeView } = Styles;
    const { totalComment } = this.props.navigation.state.params.data.data;
    return (
      this.state.isLoading
        ?
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="small" color='blue' />
        </View>
        :
        <ScrollView
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{ padding: 15, backgroundColor: Colors.white, }}
          showsVerticalScrollIndicator={false}>
          <View style={[instagramView, { marginTop: 0 }]}>
            <View style={feedHeader}>
              <View style={header}>
                <View style={{flexDirection:'row',alignItems:'center',marginLeft:7}}>
                <Econ1 name="user" size={20} color="grey" />
                <View>
                <Text style={headerText}>{this.state.data.username}</Text>
                <Text style={timeHeader}>{this.state.data.time_text}</Text>
                </View>
                
                </View>
              </View>
            </View>

            <View style={{ height: 180, backgroundColor: 'grey', width: '100%' }}>
              <Swiper containerStyle={{ height: '100%', backgroundColor: 'grey' }}
                loop={false} activeDotColor='blue' paginationStyle={{ bottom: -25 }}
                activeDotStyle={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'red' }}
                dotStyle={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'rgba(0,0,0,0.4)' }}>
                {
                  this.state.data.media_set.map((d, i) => {
                    return <ImageBackground source={{ uri: d.file.toString() }}
                      style={{ height: '100%', width: '100%' }}>
                    </ImageBackground>
                  })
                }
              </Swiper>
            </View>

            <View style={commentView}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={LikeView}>
                  <TouchableOpacity
                    disabled={true}
                  // disabled={this.state.data.is_liked?true:false}
                  // onPress={() => this.LikePost()}
                  >
                    {this.state.data.is_liked
                      ?
                      <Econ1 name="heart" size={18} color="red" />
                      :
                      <Econ name="heart" size={18} color="grey" />
                    }
                  </TouchableOpacity>
                  <Text style={LikeText}>{this.state.data.likes} Likes</Text>
                </View>
                <View style={[LikeView, { marginLeft: 10 }]}>
                  <TouchableOpacity disabled={true} style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Econ name="comment" size={18} color="grey" />
                  </TouchableOpacity>
                  <Text style={LikeText}>{this.state.data.comments.length} Comments</Text>
                </View>
              </View>
              <TouchableOpacity
                disabled={true}
                // onPress={() => this.MakeFavourite(this.state.data.post_id)}
                // disabled={this.state.data.is_saved ? true :false}
                style={{ height: 70, width: 20, marginTop: 15 }}>
                {
                  this.state.data.is_saved
                    ?
                    <Econ1 name="bookmark" size={18} color="#FF9800" />
                    :
                    <Econ name="bookmark" size={18} color="grey" />
                }
              </TouchableOpacity>
            </View>


            <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.1)' }}></View>

            <TextInput
              defaultValue={this.state.data.description}
              onChangeText={(text) => this.setState({ UpdateText: text })}
              style={{ width: '90%', height: 200, alignSelf: 'center', padding: 20, borderWidth: 1, borderColor: 'rgba(0,0,0,0.1)', borderRadius: 4, marginVertical: 20, textAlignVertical: 'top' }}>

            </TextInput>

            <TouchableOpacity
              onPress={() => this.EditPost(this.state.data.post_id)}
              style={{ height: 40, width: 100, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.ActiveTintColor, borderRadius: 4, alignSelf: 'center', marginBottom: 20 }}>
              {
                this.state.isUpdateTextLoading ?
                  <ActivityIndicator size='small' color='white' />
                  :
                  <Text style={{ fontSize: 11, color: 'white' }}>Update Post</Text>
              }
            </TouchableOpacity>
          </View>
        </ScrollView>);
  }
}
