import React, {Component} from 'react';
import {
  View,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
import Styles from './style';
import Geolocation from 'react-native-geolocation-service';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import {Constants} from '../../Component/Contants';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      isLoading: false,
      lat: null,
      long: null,
      access_Token: '',
      HtmlView: '',
      user_name: '',
      backButtonEnabled: false,webKey:0,
    };
  }

  // hasLocationPermission = async () => {
  //   if (Platform.OS === 'ios' || (Platform.OS === 'android' && Platform.Version < 23)) {
  //     return true;
  //   }

  //   const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

  //   if (hasPermission) return true;

  //   const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

  //   if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

  //   if (status === PermissionsAndroid.RESULTS.DENIED) {
  //     alert('Location permission denied by user.');
  //   } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
  //     alert('Location permission revoked by user.');
  //   }
  //   return false;
  // };

  componentWillUnmount(){
    this.focusListener.remove();
  }

  async componentDidMount() {
    this.setState({isLoading: true});
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
     
       
        this.setState({webKey:this.state.webKey+1});
      
       
     });
    // BackHandler.addEventListener('hardwareBackPress', ()=> {
    //   if (this.state.backButtonEnabled) {
    //     this.webview.goBack();
    //     return true;
    //   }
    // });

    // const hasLocationPermission = await this.hasLocationPermission();
    // if (hasLocationPermission) {
    //   Geolocation.getCurrentPosition(
    //     (position) => {
    //       this.setState({ lat: position.coords.latitude, long: position.coords.longitude })
    //       console.log('lat',position);
    //     },
    //     (error) => {
    //       this.setState({ isLoading: false })
    //       // See error code charts below.
    //       console.log(error.code, error.message);
    //     },
    //     { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    //   );
    // }
    AsyncStorage.getItem('UserName', (err, result) => {
      if (err === null) {
        console.log(result, 'resulltttttt');
        this.setState({user_name: result});
      }
    }).then(() => {
      AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
        if (error === null) {
          this.setState(
            {access_Token: result.replace(/"/g, '').trim(), isLoading: false},
            () => {
              console.log(this.state.access_Token, 'token<=');
              let html = this.createHTML(
                url.url2+'/' +
                  this.state.user_name,
              );
              console.log(html);
              this.setState({
                HtmlView: html,
                isLoading: false,
              });
            },
          );
        } else {
          alert('Something Went wrong...!!');
          this.setState({isLoading: false});
        }
      });
    });
  }

  // getWebView() {
  //   fetch(`https://blendzmate.com/deals`, {
  //     method: 'POST',
  //     headers: {
  //       "Content-Type": "application/x-www-form-urlencoded",
  //     },
  //     body: `access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app&lat=${this.state.lat}&lng=${this.state.long}`
  //   })
  //     .then((response) => response.text())
  //     .then((res) => {
  //       this.setState({ HtmlView: res, isLoading: false })
  //       console.log('deal injected:', res)
  //     })
  //     .catch((err) => console.log(err))
  // }

  // injectjs() {
  //   let jsCode = `const bodyData = access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app&lat=${this.state.lat}&lng=${this.state.long}
  //   fetch('https://blendzmate.com/deals', {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/x-www-form-urlencoded',
  //     },
  //     body: bodyData,
  //   }).then(response => response.text()).then(valueText => {
  //     alert(JSON.stringify(valueText));
  //   });`;
  //   return jsCode;
  // }

  createHTML = link => {
    return `<html><script>function runthis(){const body = 'access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app';
    post('${link}',{access_token: '${this.state.access_Token}',server_key:'${url.server_key}',webview_type:'app'});
}

function post(path, params, method='post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;
  
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];
  
        form.appendChild(hiddenField);
      }
    }
  
    document.body.appendChild(form);
    form.submit();
  }
  setTimeout(function(){ runthis(); }, 400);

</script><body></body></html>`;
  };

  renderLoading = () => (
    <View style={{ flex: 1 }}><ActivityIndicator animating size='large' color='blue'  /></View>
  );

  render() {
    return this.state.isLoading ? (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="small" color="blue" animating />
      </View>
    ) : (
      <View style={{flex: 1}}>
        <WebView
          ref={ref => (this.webview = ref)}
          key={this.state.webKey}
          onNavigationStateChange={state => {
            console.log(state, 'pppppp');
            this.setState({
              backButtonEnabled: state.canGoBack,
            });
            if (state.url.includes("/listing")) {
              alert('Can only be viewed/edited through website');
              this.webview.stopLoading();
              return false;
            }
            if (
              state.url === url.url2+'/signout'||state.url === url.url2+'/welcome' &&
              state.loading === true
            ) {
              AsyncStorage.removeItem(Constants.USER_TOKEN).then(() => {
                console.log('log out:');
                //this.props.navigation.navigate('loginStack')
                //PubSub.publish('_goToCommentScreen', { data: this.props.data.item.post_id })
                PubSub.publish('_goToLoginScreen');
              });
            }
            if(state.url.includes("/post/")){
                let post_id=res = state.url.split("/post/");
                
                PubSub.publish('_goToCommentScreen', { data: post_id[1] });
                this.webview.stopLoading();
              return false;
                
            }
            
            if(state.url.includes('blendsmate')||state.url.includes('blendzmate')){
               console.log(html,'fromprofile');
              let html=this.createHTML(state.url);
              console.log(html,'fromprofile');
               this.setState({
              HtmlView: html,
            });
            }
            
            
           
          }}
          renderLoading={this.renderLoading}
          javaScriptEnabled={true}
          startInLoadingState={true}
          //evaluateJavaScript={this.injectjs()}
          //injectJavaScript={this.Fetch}
          source={{html: this.state.HtmlView}}
        />
      </View>
    );
  }
}

// import React, { Component } from 'react';
// import { View, Text, TouchableOpacity, FlatList } from 'react-native';
// import Styles from './style';
// import LinearGradient from 'react-native-linear-gradient';
// import Econ from 'react-native-vector-icons/FontAwesome';
// import { ScrollView } from 'react-native-gesture-handler';
// import Follow from '../../Component/FollowView';
// import PostList from '../../Component/PostList';
// import { createStackNavigator } from 'react-navigation-stack';
// import { createAppContainer } from 'react-navigation';
// import Settings from '../Setting/Settings';
// import General from '../General/General';
// import ProfileSetting from '../ProfileSetting/ProfileSetting';
// import Password from '../Password/Password';
// import Privacy from '../Privacy/Privacy';
// import Notification from '../Notification/Notification';
// import Interests from '../Interests/Interests';
// import DeleteAccount from '../DeleteAccount/DeleteAccount';
// import VerificationProfile from '../VerificationProfile/VerificationProfile';
// import Blocked from '../Blocked/Blocked';
// import Color from '../../../assets/Colors/Colors';
// import ProfileTab from '../../Component/ProfileTab';
// import Followers from '../Followers/Followers';
// import Following from '../Following/Following';
// import Favourites from '../Favourites/Favourites';

// const color = ['#00C9FF', '#92FE9D'];

// class Profile extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       data: [
//         { id: 0, key: 'posts', isSelected: false },
//         { id: 1, key: 'followers', isSelected: false },
//         { id: 2, key: 'following', isSelected: false },
//         { id: 3, key: 'favourites', isSelected: false }
//       ]
//     };
//   }

//   render() {
//     const { ProfileView, ProfileImageView, UserNameText, ButtonView, FollowView } = Styles;
//     return (
//       <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ backgroundColor: Color.PrimaryBackGroundColor }}>
//         <View>
//           <LinearGradient
//             start={{ x: 0, y: 0 }}
//             end={{ x: 1, y: 1 }}
//             colors={color}
//             style={ProfileView}>
//             <View style={ProfileImageView}>
//               <Econ name="user" size={50} color="grey" />
//             </View>

//             <View style={{ flexDirection: 'row' }}>
//               <Text style={UserNameText}>admin</Text>
//             </View>

//             <View style={{ flexDirection: 'row', alignItems: 'center' }}>
//               <TouchableOpacity
//                 onPress={() => this.props.navigation.navigate('Settings')}
//                 style={ButtonView}>
//                 <View style={{ flexDirection: 'row', alignItems: 'center' }}>
//                   <Econ name="edit" size={11} color="white" />
//                   <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Bold', marginLeft: 5 }}>Profile settings</Text>
//                 </View>
//               </TouchableOpacity>

//               <TouchableOpacity style={[ButtonView, { marginLeft: 10 }]}>
//                 <View style={{ flexDirection: 'row', alignItems: 'center' }}>
//                   <Econ name="edit" size={11} color="white" />
//                   <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Bold', marginLeft: 5 }}>Listings</Text>
//                 </View>
//               </TouchableOpacity>
//             </View>
//           </LinearGradient>
//         </View>

//         <View style={FollowView}>
//           <TouchableOpacity style={{ height: 50, width: '25%', backgroundColor: Color.PrimaryBackGroundColor, alignItems: 'center', justifyContent: 'center' }}>
//             <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Bold' }}>0</Text>
//             <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', marginTop: 5 }}>Posts</Text>
//           </TouchableOpacity>
//           <TouchableOpacity
//           onPress={()=>this.props.navigation.navigate('Followers')}
//           style={{ height: 50, width: '25%', backgroundColor: Color.PrimaryBackGroundColor, alignItems: 'center', justifyContent: 'center' }}>
//             <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Bold' }}>0</Text>
//             <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', marginTop: 5 }}>Followers</Text>
//           </TouchableOpacity>
//           <TouchableOpacity
//           onPress={()=>this.props.navigation.navigate('Following')}
//           style={{ height: 50, width: '25%', backgroundColor: Color.PrimaryBackGroundColor, alignItems: 'center', justifyContent: 'center' }}>
//             <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Bold' }}>0</Text>
//             <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', marginTop: 5 }}>Following</Text>
//           </TouchableOpacity>
//           <TouchableOpacity
//            onPress={()=>this.props.navigation.navigate('Favourites')}
//           style={{ height: 50, width: '25%', backgroundColor: Color.PrimaryBackGroundColor, alignItems: 'center', justifyContent: 'center' }}>
//             <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Bold' }}>0</Text>
//             <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', marginTop: 5 }}>Favourites</Text>
//           </TouchableOpacity>
//         </View>

//       </ScrollView>
//     );
//   }
// }

// const ProfileStack = createStackNavigator({
//   Profile: Profile,
//   Followers:Followers,
//   Following:Following,
//   Favourites:Favourites,
//   Settings: Settings,
//   General: General,
//   ProfileSetting: ProfileSetting,
//   Password: Password,
//   Privacy: Privacy,
//   Notification: Notification,
//   Interests: Interests,
//   DeleteAccount: DeleteAccount,
//   VerificationProfile: VerificationProfile,
//   Blocked: Blocked
// }, {
//   headerMode: 'none'
// })

// export default createAppContainer(ProfileStack);
