import Color from '../../../assets/Colors/Colors';

const Styles = {
    ProfileView:{
        height:300,
        width:'100%',
        //backgroundColor:'grey',
        justifyContent:'center',
        alignItems: 'center',
    },
    ProfileImageView:{
        height:100,
        width:100,
        borderRadius: 50,
        backgroundColor:'white',
        elevation:5,
        alignItems: 'center',
        justifyContent:'center'
    },
    UserNameText:{
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        color:'white',
        marginTop: 20,
    },
    ButtonView:{
        paddingHorizontal: 15,
        paddingVertical: 10,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius:20,
        backgroundColor:'rgba(0,0,0,0.2)',
        marginTop:20
    },
    FollowView:{
        height:60,
        width:'88%',
        alignSelf:'center',
        backgroundColor:Color.PrimaryBackGroundColor,
        marginTop:10,
        flexDirection: 'row',
    }
}

export default Styles;
