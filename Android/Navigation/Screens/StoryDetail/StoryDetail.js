import React, { Component } from 'react';
import { View, Text, TextInput, ImageBackground, StyleSheet, Dimensions , TouchableOpacity} from 'react-native';
import Swiper from 'react-native-swiper'
import Colors from '../../../assets/Colors/Colors';
import Econ from 'react-native-vector-icons/FontAwesome5';

const WIDTH = Dimensions.get('window').width;

export default class StoryDetail extends Component {
    constructor(props) {
        super(props);
        console.log('stories data', this.props.navigation.getParam('data'));
        this.state = {
            avatar: this.props.navigation.getParam('data').avatar,
            username: this.props.navigation.getParam('data').username,
        };
    }

    // _onMomentumScrollEnd = (e, state, context) => {
    //     console.log(e)
    //     console.log(state)
    //     console.log(context)
    //     if (e.nativeEvent.position === this.props.navigation.getParam('data').stories.length - 1) {
    //         setTimeout(() => {
    //             this.props.navigation.goBack()
    //         }, 5000)
    //     }
    // }

    render() {
        return (
            <View
                style={{ flex: 1, backgroundColor:'black' }}>
                <Swiper
                   // onMomentumScrollEnd={this._onMomentumScrollEnd}
                    autoplay={true}
                    showsButtons={true}
                    autoplayTimeout={5}
                    showsPagination={false}
                    autoplayDirection={true}
                    loop={false}
                    nextButton={
                        <View
                            style={{
                                height: 30, width: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 40, backgroundColor: 'rgba(255,255,255,0.5)'
                            }}>
                            <Econ name="chevron-right" size={15} color='red' />
                        </View>
                    }
                    prevButton={
                        <View
                            style={{
                                height: 30, width: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 40, backgroundColor: 'rgba(255,255,255,0.5)'
                            }}>
                            <Econ name="chevron-left" size={15} color='red' />
                        </View>
                    }
                    style={styles.wrapper}>
                    {
                        this.props.navigation.getParam('data').stories.map((d, i) => {
                            return <ImageBackground source={{ uri: d.src }}
                                key={i}
                                resizeMode='contain'
                                style={{ height: '100%', width: '100%' }}>
                                <View
                                    style={{
                                        position: 'absolute',
                                        top: 10, left: 10,
                                        width: WIDTH - 30,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center'
                                    }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <ImageBackground
                                            source={{ uri: this.state.avatar }}
                                            imageStyle={{ borderRadius: 20 }}
                                            style={{ height: 40, width: 40, }}>
                                        </ImageBackground>
                                        <View style={{ height: 30, justifyContent: 'space-between' }}>
                                            <Text style={{ fontSize: 14, fontFamily: 'Roboto-Bold', color: 'white', marginLeft: 10 }}>
                                                {this.state.username}
                                            </Text>
                                            <Text style={{ fontSize: 11, fontFamily: 'Roboto-Bold', color: 'white', marginLeft: 10 }}>
                                                {d.time_text}
                                            </Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity 
                                    onPress={()=>this.props.navigation.goBack()}
                                    style={{ height: 25, width: 25, justifyContent: 'center', alignItems: 'center' }}>
                                        <Econ name="times" size={18} color='white' />
                                    </TouchableOpacity>
                                </View>
                            </ImageBackground>
                        })
                    }
                </Swiper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    }
})