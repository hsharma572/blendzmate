import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, FlatList, ImageBackground, StyleSheet, ScrollView, PermissionsAndroid, Platform, ActivityIndicator, Image } from 'react-native';
import Colors from '../../../assets/Colors/Colors';
import RoomMateList from '../../Component/RoomMateList';
import ImagePicker from 'react-native-image-picker';
import PropertyType from '../../Component/PropertyType';
import { Dropdown } from 'react-native-material-dropdown';
import PropertyFeatures from '../../Component/PropertyType';
import RulesList from '../../Component/RulesList';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import Toast from 'react-native-simple-toast';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
import url from '../../Component/url';
import Icon from 'react-native-vector-icons/FontAwesome5'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
const options = {
    title: 'Select Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
let latestlat=0,latestlong=0;
import { width, height, totalSize } from 'react-native-dimension';

export default class ListingDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListingName: '',
            Rent: '',
            RoomMate: [
                { id: 0, roomate: 'Male' },
                { id: 1, roomate: 'Female' },
                { id: 2, roomate: `Doesn\'t Matter` }
            ],
            SelectedRoomate: '',
            SelectedRoomateColor: false,
            SelectedRoomateType: '',//this is for upload,
            isPayRentYes: false,
            PayRent: '', // this is for upload
            isPayRentNo: false,
            //for image upload
            ImageName: '',
            ImageType: '',
            ImageUri: '',
            avatarSource: '',
            PropertyTypeColor: false,
            PropertyType: '', //this is for upload 
            PropertyCondominimum: false,
            PropertyApartment: false,
            PropertyDormitory: false,
            OccupantsData: [
                { value: '1' },
                { value: '2' },
                { value: '3' },
                { value: '4' },
                { value: '5' }
            ],
            Occupants: '1', //this is for upload,
            NoOfRooms: [
                { value: '1 BHK' },
                { value: '2 BHK' },
                { value: '3 BHK' },
                { value: '4 BHK' },
                { value: '5 BHK' },
                { value: '6 BHK' },
                { value: '6+ BHK' }
            ],
            NoOfBaths: [
                { value: '1 Bath' },
                { value: '2 Baths' },
                { value: '3 Baths' },
                { value: '4 Baths' },
                { value: '5 Baths' },
                { value: '6 Baths' },
                { value: '6+ Baths' }
            ],
            NoOfRoom: '',//this is for upload
            NoOfBath: '',//this is for upload
            PropertyFeatures: [
                { id: 0, features: 'Wifi Included', isSelected: false },
                { id: 1, features: 'In-unit Washer', isSelected: false },
                { id: 2, features: 'Air Conditioning', isSelected: false },
                { id: 3, features: 'Elevator', isSelected: false },
                { id: 4, features: 'Furnished', isSelected: false },
                { id: 5, features: 'Gym', isSelected: false },
                { id: 6, features: 'Tv', isSelected: false },
                { id: 7, features: 'Private Bathroom', isSelected: false },
                { id: 8, features: 'Parking', isSelected: false },
                { id: 9, features: 'Meals Provided', isSelected: false },
                { id: 10, features: 'Power Backup', isSelected: false },
                { id: 11, features: '24 Hour Power Supply', isSelected: false },
                { id: 12, features: 'Security Gaurd', isSelected: false },
                { id: 13, features: 'Has Dog', isSelected: false },
                { id: 14, features: 'Has Cat', isSelected: false },
                { id: 15, features: 'Has Other Pet', isSelected: false }
            ],
            Features: [],
            RulesData: [
                { id: 0, rules: 'No smoking', isSelected: false },
                { id: 1, rules: 'No pets', isSelected: false },
                { id: 2, rules: 'No drugs', isSelected: false },
                { id: 3, rules: 'No drinking', isSelected: false },
                { id: 4, rules: 'Dogs Ok', isSelected: false },
                { id: 5, rules: 'Cats Ok', isSelected: false },
                { id: 6, rules: 'Other Pets Ok', isSelected: false },
                { id: 7, rules: 'Couples Ok', isSelected: false }
            ],
            MarkerLatitude: 24.92009056750823,
            MarkerLongitude: 67.1012272143364,
            markers: [
                {
                    coordinates: {
                        latitude: 3.148561,
                        longitude: 101.652778,
                    },
                },
            ],
            isMarkerShow: false,
            Pin: '',//this is for upload
            State: '',//this is for upload
            City: '',//this is for upload
            Locality: '',//this is for upload
            FullAddress: '',//this is for upload,
            Country: '',//this is for upload,
            MapLatitude: '',
            MapLongitude: '',
            loading: true,
            coordinate: '',
            pType: [],//this is for upload
            regulation: [], //this is for upload
            ImageUploading: false,
            ImageList: '',
            ImageId: [],
            isDelete: false,
            isDeleteLoading: false,
            loadingInfoData:false
        };
    }

    componentDidMount() {
        this.getLocation();
    }

    async getLocation() {
        const hasLocationPermission = await this.hasLocationPermission();

        if (!hasLocationPermission) return;

        this.setState({ loading: true }, () => {
            Geolocation.getCurrentPosition(
                (position) => {
                    this.setState({ MapLatitude: position.coords.latitude, MapLongitude: position.coords.longitude, loading: false }, () => {
                        latestlat=position.coords.latitude;latestlong=position.coords.latitude;
                        let NewCordinates = this.state.markers.map((item) => {
                            item.coordinates.latitude = position.coords.latitude,
                                item.coordinates.longitude = position.coords.longitude
                            return item
                        })
                        this.setState({ markers: NewCordinates })
                    });
                    console.log(position);
                },
                (error) => {
                    this.setState({ loading: false });
                    console.log(error);
                },
                {
                     enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
            );
        });
    }

    async hasLocationPermission() {
        if (Platform.OS === 'ios' ||
            (Platform.OS === 'android' && Platform.Version < 23)) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (hasPermission) return true;

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            Toast.show('Location permission denied by user.', Toast.SHORT);
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            Toast.show('Location permission revoked by user.', Toast.SHORT);
        }

        return false;
    }

    UploadImage() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                this.setState({ ImageName: response.fileName, ImageType: response.type, ImageUri: response.uri })
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
            }
        });
    }

    SelectFeatures(item) {
        this.state.pType.push(item.features)
        let NewFeatures = this.state.PropertyFeatures.map((i, index) => {
            if (item.id === i.id) {
                if (i.isSelected) {
                    this.state.Features.push(item.features)
                }
                else {
                    console.log(item.features, 'jjj')
                }
                i.isSelected = !i.isSelected

            }
            return i
        })
        this.setState({ PropertyFeatures: NewFeatures })
    }

    RulesSelect(item) {
        this.state.regulation.push(item.rules)
        let NewFeatures = this.state.RulesData.map((i, index) => {
            if (item.id === i.id) {
                i.isSelected = !i.isSelected
            }
            return i
        })
        this.setState({ RulesData: NewFeatures })
    }

    setMarker(coordinate) {
        let NewCordinates = this.state.markers.map((item) => {
            item.coordinates = coordinate
            return item
        })
        this.setState({ markers: NewCordinates, coordinate: coordinate, MapLatitude: coordinate.latitude,
        MapLongitude: coordinate.longitude, }, () => {
            this.getAddress(this.state.coordinate)
        })
    }

    getAddress(coordinate) {

        // alert(JSON.stringify(coordinate))
        Geocoder.init("AIzaSyB8Wm2IOByeafGEqgMNUcyC_8KmhFdgkjY");
        Geocoder.from(coordinate.longitude, coordinate.latitude)
            .then(json => {
               
                var addressComponent = json.results[0].address_components[0];
                console.log(addressComponent);
            })
            .catch(error => console.warn(error));
    }
    csvToString=(csValues,name)=>{
        let csv=csValues.split(",");
        let str='';
        for(let i=0;i<csv.length;i++){
            str=str+name+"[]="+csv[i]+"&";
        }
        console.log(str);
        return str;
    }

    submit() {
     
        const {
            SelectedRoomateType,
            PayRent, ImageName,
            ImageType, ImageUri, PropertyType,ImageId,
            Occupants, NoOfRoom, NoOfBath, Pin, State, City, Locality, FullAddress, Country, ListingName, Rent, pType, regulation
        } = this.state;
        let access=this.props.navigation.getParam('regiser_token', '');
        let userid=this.props.navigation.getParam('user_id', '');
        let tenantString=this.csvToString(SelectedRoomateType,"tenant");
        let featuresString=this.csvToString(pType.toString(),"features");
        let rulesString=this.csvToString(regulation.toString(),"rules");

        this.setState({ loadingInfoData: true })
    
        console.log(`name_listing=${ListingName}&price_listing=${Rent}&images=${ImageId.toString()}&${tenantString}upfront=${PayRent}&prop=${PropertyType}&occupants=${Occupants}&bedroomCount=${NoOfRoom}&bath=${NoOfBath}&${featuresString}${rulesString}zip=${Pin}&country=${Country}&state=${State}&city=${City}&locality=${Locality}&address=${FullAddress}&access_token=${access}&server_key=${url.server_key}&user_update_id=${userid}&user_id=${userid}&lat=${latestlat}&lng=${latestlong}`)
        fetch(url.url2+'/listing/account/add_listing',{
            method:'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body:`name_listing=${ListingName}&price_listing=${Rent}&images=${ImageId.toString()}&${tenantString}upfront=${PayRent}&prop=${PropertyType}&occupants=${Occupants}&bedroomCount=${NoOfRoom}&bath=${NoOfBath}&${featuresString}${rulesString}zip=${Pin}&country=${Country}&state=${State}&city=${City}&locality=${Locality}&address=${FullAddress}&access_token=${access}&server_key=${url.server_key}&user_update_id=${userid}&user_id=${userid}&lat=${latestlat}&lng=${latestlong}`
        })
        .then(data=>data.json())
        .then(res=>{
            this.setState({ loadingInfoData: false })
            console.log(res,'listing');
            if(res.msg=="Listing added successfully"){
                this.props.navigation.navigate('MainScreen')

               
            }else{
                alert(res.msg);
            }

        })
        .catch(e=>{
            this.setState({ loadingInfoData: false })
            console.log(e)
        })
    }

    Upload() {
        const { ImageName, ImageType, ImageUri } = this.state;
        if(ImageUri==''){
            Toast.show('Please select image', Toast.SHORT);
            return;
        }
        let Data = {
            access_token: this.props.navigation.getParam('user_id', ''),
            server_key: url.server_key,
            image: {
                uri: ImageUri,
                type: ImageType,
                name: ImageName
            }
        }

        data = new FormData()
        for (let key in Data) {
            data.append(key, Data[key])
        }
        this.setState({ ImageUploading: true })
        fetch(url.url2+'/listing/image/add_images', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: data
        })
            .then(data => data.json())
            .then(res => {
                console.log(res.images, 'gggg')
                this.setState({ ImageId: [] })
             
                for (i = 0; i < res.images.length; i++) {
                    this.state.ImageId.push(res.images[i].id)
                    
                }
                console.log(this.state.ImageId, 'image id')
                if (res.error) {
                    this.setState(
                        {
                            avatarSource: '',
                            ImageUploading: false,
                            ImageList: res.images
                        }, () => {
                            console.log(this.state.ImageList, 'kkkkkkk')
                        })
                }
                else {
                    this.setState({ ImageUploading: false })
                    Toast.show(res.msg, Toast.SHORT);
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({ ImageUploading: false })
                Toast.show('something went wrong..!!', Toast.SHORT);

            })
    }

    DeleteImage(item, index) {
        this.state.ImageList.splice(index, 1)
        let indexOfID = this.state.ImageId.indexOf(item.id)
        this.state.ImageId.splice(indexOfID, 1)
        this.setState({ isDelete: true })
    }

    render() {
        return (
            this.state.loading
                ?
                <ActivityIndicator size='small' color='black' style={{ flex: 1 }} />
                :
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ backgroundColor: Colors.PrimaryBackGroundColor, justifyContent: 'center', alignItems: 'center', paddingVertical: 30 }}>
                    <View style={{
                        width: width(80), 
                        paddingVertical: 10,
                        paddingHorizontal: 15,
                        borderRadius: 5,
                        backgroundColor: 'white',
                        elevation: 1
                    }}>
                        <TextInput
                            onChangeText={(text) => this.setState({ ListingName: text })}
                            placeholder='Listing Name. Eg: 1BHK at Marathahalli, Banglore'
                            style={{
                                fontFamily: 'OpenSans-Regular',
                                width: 270,
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TextInput
                            onChangeText={(text) => this.setState({ Rent: text })}
                            placeholder='Rent'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 10,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Upload Property Image
                        </Text>

                        <TouchableOpacity
                            onPress={() => this.UploadImage()}
                            style={{
                                height: 150,
                                width: '100%',
                                marginTop: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: 'rgba(0,0,0,0.1)',
                                borderRadius: 5
                            }}>
                            {
                                this.state.avatarSource
                                    ?
                                    <ImageBackground
                                        imageStyle={{ borderRadius: 5 }}
                                        source={this.state.avatarSource} style={{ height: '100%', width: '100%' }} />
                                    :
                                    <Text style={{ fontSize: 13, color: 'Roboto-Regular', color: 'rgba(0,0,0,0.4)' }}>No Image Available</Text>
                            }
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.Upload()}
                            style={{ height: 35, width: 100, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.ActiveTintColor, alignSelf: 'center', marginTop: 10 }}>
                            {
                                this.state.ImageUploading
                                    ?
                                    <ActivityIndicator size='small' color='white' />
                                    :
                                    <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans-Regular' }}>Upload image</Text>
                            }
                        </TouchableOpacity>

                        <View style={{ height: 150, width: '100%', marginTop: 10, backgroundColor: 'rgba(0,0,0,0.1)' }}>
                            <FlatList
                                showsHorizontalScrollIndicator={false}
                                // numColumns={4}
                                horizontal
                                key={this.state.isDelete}
                                data={this.state.ImageList}
                                renderItem={({ item, index }) => {
                                    console.log(index, 'lolo')
                                    return <ImageBackground
                                        style={{ height: 150, width: 150, margin: 5 }}
                                        source={{ uri: item.file }}>
                                        <TouchableOpacity
                                            onPress={() => this.DeleteImage(item, index)}
                                            style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 10, position: 'absolute', top: 10, right: 10, backgroundColor: 'black',
                                                justifyContent: 'center', alignItems: 'center'
                                            }}>
                                            <Icon name='trash' size={9} color='red' />
                                        </TouchableOpacity>
                                    </ImageBackground>
                                }}
                            />
                        </View>

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 10,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Your roomate should be?
                    </Text>

                        <FlatList
                            contentContainerStyle={{ paddingVertical: 10 }}
                            horizontal
                            data={this.state.RoomMate}
                            renderItem={(data) => {
                                return <RoomMateList
                                    SelectedRoomate={this.state.SelectedRoomate}
                                    SelectedRoomateColor={this.state.SelectedRoomateColor}
                                    Selected={(id, roomate) => this.setState({ SelectedRoomate: id, SelectedRoomateType: roomate, SelectedRoomateColor: true })}
                                    data={data}
                                />
                            }}
                        />

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 5,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Would you like roommates to pay rent upfront?
                    </Text>

                        <View style={{ height: 30, width: 105, marginVertical: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ isPayRentYes: true, isPayRentNo: false, PayRent: 'Yes' })}
                                style={{ height: 25, paddingHorizontal: 15, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: this.state.isPayRentYes ? Colors.ActiveTintColor : 'rgba(0,0,0,0.1)', borderRadius: 15 }}>
                                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 11, color: this.state.isPayRentYes ? Colors.ActiveTintColor : 'rgba(0,0,0,0.5)' }}>Yes</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ isPayRentYes: false, isPayRentNo: true, PayRent: 'No' })}
                                style={{ height: 25, paddingHorizontal: 15, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: this.state.isPayRentNo ? Colors.ActiveTintColor : 'rgba(0,0,0,0.1)', borderRadius: 15 }}>
                                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 11, color: this.state.isPayRentNo ? Colors.ActiveTintColor : 'rgba(0,0,0,0.5)' }}>No</Text>
                            </TouchableOpacity>
                        </View>

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 5,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Property Type
                    </Text>

                        <View style={{ height: 30, width: '100%', marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ PropertyCondominimum: true, PropertyDormitory: false, PropertyApartment: false, PropertyType: 'Condominimum' })}
                                style={{ height: 25, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: this.state.PropertyCondominimum ? Colors.ActiveTintColor : 'rgba(0,0,0,0.1)', borderRadius: 15 }}>
                                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 11, color: this.state.PropertyCondominimum ? Colors.ActiveTintColor : 'rgba(0,0,0,0.5)' }}>Condominium</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ PropertyCondominimum: false, PropertyApartment: true, PropertyDormitory: false, PropertyType: 'Apartment' })}
                                style={{ height: 25, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: this.state.PropertyApartment ? Colors.ActiveTintColor : 'rgba(0,0,0,0.1)', borderRadius: 15 }}>
                                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 11, color: this.state.PropertyApartment ? Colors.ActiveTintColor : 'rgba(0,0,0,0.5)' }}>Apartment</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ PropertyCondominimum: false, PropertyDormitory: true, PropertyApartment: false, PropertyType: 'Dormitory' })}
                                style={{ height: 25, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: this.state.PropertyDormitory ? Colors.ActiveTintColor : 'rgba(0,0,0,0.1)', borderRadius: 15 }}>
                                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 11, color: this.state.PropertyDormitory ? Colors.ActiveTintColor : 'rgba(0,0,0,0.5)' }}>Dormitory</Text>
                            </TouchableOpacity>
                        </View>

                        <Dropdown
                            fontSize={11}
                            baseColor='black'
                            textColor='grey'
                            labelFontSize={13}
                            pickerStyle={{ height: 180 }}
                            label='Total occupants allowed?'
                            data={this.state.OccupantsData}
                            onChangeText={(value) => this.setState({ Occupants: value })}
                        />

                        <Dropdown
                            fontSize={11}
                            baseColor='black'
                            textColor='grey'
                            labelFontSize={13}
                            label='No. Of Rooms?'
                            pickerStyle={{ height: 180 }}
                            data={this.state.NoOfRooms}
                            onChangeText={(value) => this.setState({ NoOfRoom: value })}
                        />

                        <Dropdown
                            fontSize={11}
                            baseColor='black'
                            textColor='grey'
                            labelFontSize={13}
                            label='No. Of Baths?'
                            pickerStyle={{ height: 180 }}
                            data={this.state.NoOfBaths}
                            onChangeText={(value) => this.setState({ NoOfBath: value })}
                        />

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 5,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Property Features
                    </Text>

                        <View style={{ width: 280, alignSelf: 'center', flexDirection: 'row', flexWrap: 'wrap', marginTop: 10, paddingHorizontal: 10 }}>
                            {
                                this.state.PropertyFeatures.map((item, index) => {
                                    return <PropertyFeatures
                                        item={item}
                                        PropertyFeatures={this.state.PropertyFeatures}
                                        PropertyTypeColor={(item) => this.SelectFeatures(item)}

                                    />
                                })
                            }
                        </View>

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 5,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Rules
                    </Text>

                        <View style={{ width: 280, alignSelf: 'center', flexDirection: 'row', flexWrap: 'wrap', marginTop: 10, paddingHorizontal: 10 }}>
                            {
                                this.state.RulesData.map((item, index) => {
                                    return <RulesList
                                        item={item}
                                        PropertyFeatures={this.state.PropertyFeatures}
                                        RulesColor={(item) => this.RulesSelect(item)}

                                    />
                                })
                            }
                        </View>

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 5,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Map Location
                    </Text>
                        <Text
                            style={{
                                fontSize: 10,
                                marginTop: 5,
                                color: 'rgba(0,0,0,0.4)',
                                fontFamily: 'Roboto-Regular'
                            }}>
                            Note :- Just enter your locality in search bar on map and we will find your address
                    </Text>

                        <View style={{ height: 255, width: '100%', backgroundColor: 'white', borderRadius: 5, justifyContent: 'space-between' }}>

                            {/* <GooglePlacesAutocomplete
                                placeholder="Enter your address"
                                minLength={2} // minimum length of text to search
                                autoFocus={false}
                                returnKeyType={'search'} // Can be left out for default return key
                                listViewDisplayed={true} // true/false/undefined
                                fetchDetails={true}
                                // renderDescription={row => console.log(row)} 
                                onPress={(data, details = null) => {
                                    // alert('hello')
                                    console.log(details)
                                    this.setState({
                                        MarkerLatitude: details.geometry.location.lat,
                                        MarkerLongitude: details.geometry.location.lng,
                                    });
                                    // props.notifyChange(details.geometry.location);
                                }}
                                filterReverseGeocodingByTypes={[
                                    'locality',
                                    'administrative_area_level_3',
                                ]}
                                GooglePlacesSearchQuery={{
                                    rankby: 'distance',
                                    // types: 'cities',
                                }}
                                // GooglePlacesDetailsQuery={{
                                //     // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                                //     fields: 'geometry',
                                //   }}
                                query={{
                                    key: 'AIzaSyB8Wm2IOByeafGEqgMNUcyC_8KmhFdgkjY',
                                    language: 'en',
                                }}
                                styles={{
                                    textInputContainer: {
                                        backgroundColor: 'white',
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                        width: 278,
                                        marginLeft: 0,
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        zIndex: 111111111111111,
                                        // height:100
                                    },
                                    textInput: {
                                        borderWidth: 1,
                                        borderRadius: 0,
                                        borderColor: 'rgba(0,0,0,0.1)',
                                        height: 40,
                                        color: 'black',
                                        fontSize: 14,
                                        marginLeft: 0,

                                    },
                                    predefinedPlacesDescription: {
                                        color: 'black',
                                    },
                                    listView: {
                                        height: 400,
                                        // zIndex: 111111111111111111,
                                    },
                                }}
                                nearbyPlacesAPI="GooglePlacesSearch"
                                debounce={300}
                            /> */}
                            <View style={StyleSheet.absoluteFillObject}>
  <FontAwesome name="map-marker" 
    style={{    
      zIndex: 3,
      position: 'absolute',
      marginTop: -37,
      marginLeft: -11,
      left: '50%',
      top: '50%'}} 
    size={40}
    color="#f00" />
                            <MapView
                                onPress={(e) => {
                                    console.log(e.nativeEvent.coordinate,'lat');
                                    this.setMarker(e.nativeEvent.coordinate)
                                }}
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={{ height: 255, width: '100%', marginTop: 10 }}
                                onRegionChange={(region)=>{
                                    latestlat=region.latitude;
                                    latestlong=region.longitude;
                                }}
                                region={{
                                    latitude: this.state.MapLatitude,
                                    longitude: this.state.MapLongitude,
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                }}>
                          
                            </MapView>
                            </View>
                        </View>

                        <Text
                            style={{
                                fontSize: 12,
                                marginTop: 15,
                                color: '#333',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Address
                    </Text>

                        <Text
                            style={{
                                fontSize: 10,
                                marginTop: 5,
                                color: 'rgba(0,0,0,0.4)',
                                fontFamily: 'Roboto-Regular'
                            }}>
                            Note :- For your ease. Please search your address in map. We will try to auto-fill the address
                    </Text>

                        <TextInput
                            value={this.state.Pin}
                            onChangeText={(text) => this.setState({ Pin: text })}
                            placeholder='Pin/Zip Code'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TextInput
                            value={this.state.Country}
                            onChangeText={(text) => this.setState({ Country: text })}
                            placeholder='Country'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TextInput
                            value={this.state.State}
                            onChangeText={(text) => this.setState({ State: text })}
                            placeholder='State'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TextInput
                            value={this.state.City}
                            onChangeText={(text) => this.setState({ City: text })}
                            placeholder='City'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TextInput
                            value={this.state.Locality}
                            onChangeText={(text) => this.setState({ Locality: text })}
                            placeholder='Locality'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TextInput
                            value={this.state.FullAddress}
                            onChangeText={(text) => this.setState({ FullAddress: text })}
                            placeholder='Full Address'
                            style={{
                                width: 270,
                                fontFamily: 'OpenSans-Regular',
                                alignSelf: 'center',
                                height: 40,
                                fontSize: 11,
                                marginTop: 5,
                                borderBottomWidth: 1,
                                borderColor: 'rgba(0,0,0,0.1)'
                            }}
                        />

                        <TouchableOpacity
                            onPress={() => this.submit()}
                            style={{ height: 35, width: 80, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.ActiveTintColor, alignSelf: 'center', marginTop: 10 }}>
                            
                            

                            {
                                        this.state.loadingInfoData
                                            ?
                                            <ActivityIndicator size='small' color='white' />
                                            :
                                            <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans-Regular' }}>Submit</Text>
                                    }
                        </TouchableOpacity>
                    </View>
                </ScrollView>
        );
    }
}
