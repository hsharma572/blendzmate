import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  ActivityIndicator,
  Platform,
} from 'react-native';
import Colors from '../../../assets/Colors/Colors';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Econ1 from 'react-native-vector-icons/FontAwesome';
import Styles from './style';
import CommentSection from '../../Component/commentSection/commentSection';
import AsyncStorage from '@react-native-community/async-storage';
import {Constants} from '../../Component/Contants';
import url from '../../Component/url';
import Swiper from 'react-native-swiper';
import Entypo from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PubSub from 'pubsub-js';
import {width, height, totalSize} from 'react-native-dimension';
import Video from 'react-native-video';
import {NavigationConstants} from '../../commonValues'
import VideoPlayer from '../../VideoPlayer';
import Toast from 'react-native-simple-toast';
export default class CommentScreen extends Component {
  constructor(props) {
    super(props);
    //console.log('comment screen data:', this.props.navigation.state.params.data.data);
    this.state = {
      id: this.props.navigation.state.params.data.data,
      CommentText: '',
      access_Token: '',
      data: [],
      comments: [],
      isLoading: false,
      DefaultCommentCount: 3,
      showOptions: false,
      lastcomment: 0,
      endOfComments: false,
      isliked: false,
      openModalForReport: false,
      blockLoading: false,
      ReportLoading: false,
      isFollowSpinner: false,
      modalImage: '',
      openModalForImage: false,
    };
  }

  componentWillMount() {
    this.setState({isLoading: true});
    AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
      if (error === null) {
        this.setState({access_Token: result.replace(/"/g, '').trim()}, () =>
          this.getSinglePost(),
        );
      } else {
        alert('Something Went wrong...!!');
      }
    });
  }

  DeleteComment(id) {
    const {access_Token} = this.state;
    console.log(
      `access_token=${access_Token}&server_key=${
        url.server_key
      }&comment_id=${id}`,
      'llllllllll',
    );
    fetch(`${url.url}/v1/post/delete_comment`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&comment_id=${id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('stories response:', responseJson);

        if (responseJson.code === '200') {
          //alert('Deleted...');
          this.getSinglePost();
        } else {
          //this.setState({ isFollowSpinner: false })
          Toast.show('something went wrong..!!', Toast.SHORT);
          // alert('something went wrong..!!');
        }
      })
      .catch(error => {
        //this.setState({ isFollowSpinner: false })
        // alert('catch alert');
        Toast.show('something went wrong..!!', Toast.SHORT);
      });
  }

  async getSinglePost() {
    console.log('functoin log:');
    const {access_Token} = this.state;
    console.log(
      `access_token=${access_Token}&server_key=${url.server_key}&post_id=${
        this.state.id
      }`,
      'getSinglePost',
    );
    await fetch(`${url.url}/v1/post/fetch_post_by_id`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&post_id=${this.state.id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('single post response:', responseJson);
        if (responseJson.code === '200') {
          let len = Object.keys(responseJson.data.comments).length;
          let comm = [];
          let lstcmt = 0;
          for (let i = 0; i < len; i++) {
            comm[i] =
              responseJson.data.comments[
                Object.keys(responseJson.data.comments)[i]
              ];

            lstcmt = comm[i].id;
          }
          console.log('single post response:', comm);
          this.setState({
            data: responseJson.data,
            comments: comm,
            isliked: responseJson.data.is_liked,
            isLoading: false,
            lastcomment: lstcmt,
          });
        } else {
          this.setState({isLoading: false});
          alert('something went wrong..!!');
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        console.log(error, 'getSinglePost');
      });
  }

  PostComment() {
    const {access_Token, CommentText, data} = this.state;
    fetch(`${url.url}/v1/post/add_comment`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&text=${CommentText}&post_id=${data.post_id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('post like response:', responseJson);
        if (responseJson.code === '200') {
          //alert('you Commented...!')
          this.setState({
            CommentText: '',
          });
          this.getSinglePost();
        } else {
          // alert('something went wrong..!!')
          Toast.show('something went wrong..!!', Toast.SHORT);
        }
      })
      .catch(error => {
        // alert('catch alert')
        Toast.show('something went wrong..!!', Toast.SHORT);
      });
  }

  FetchComment() {
    const {access_Token, CommentText, data, lastcomment} = this.state;
    console.log(
      `access_token=${access_Token}&server_key=${url.server_key}&post_id=${
        data.post_id
      }&offset=${lastcomment}`,
    );
    fetch(`${url.url}/v1/post/fetch_comments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&post_id=${data.post_id}&offset=${lastcomment}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson, 'vbn');
        let len = Object.keys(responseJson.data).length;
        if (len == 0) {
          this.setState({endOfComments: true});
        } else {
          let comm = [];
          let lstcmt = 0;
          for (let i = 0; i < len; i++) {
            comm[i] = responseJson.data[Object.keys(responseJson.data)[i]];

            lstcmt = comm[i].id;
          }
          let commtens = [...this.state.comments, ...comm];
          console.log('single post response:', comm);
          this.setState({
            comments: commtens,
            isLoading: false,
            lastcomment: lstcmt,
          });
        }
      })
      .catch(error => {
        // alert('catch alert')
        Toast.show('something went wrong..!!', Toast.SHORT);
      });
  }

  BlockUser() {
    const {access_Token, data} = this.state;
    this.setState({blockLoading: true});
    fetch(`${url.url}/v1/user/block`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&users_id=${data.user_id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          Toast.show(responseJson.data.message, Toast.SHORT);
          this.setState({openModalForReport: false, blockLoading: false});
          this.props.navigation.goBack();
        } else {
          this.setState({blockLoading: false});
          Toast.show('something went wrong..!!', Toast.SHORT);
          //alert('something went wrong..!!')
        }
      })
      .catch(error => {
        this.setState({blockLoading: false});
        //alert('post catch alert')
        //console.log('post error', error);
      });
  }

  RepostPost() {
    const {access_Token, data} = this.state;
    this.setState({ReportLoading: true});
    fetch(`${url.url}/v1/post/report_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&post_id=${data.post_id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          console.log('home post:', responseJson.data);
          Toast.show(responseJson.message, Toast.SHORT);
          this.setState({openModalForReport: false, ReportLoading: false});
          this.props.navigation.goBack();
        } else {
          Toast.show('something went wrong..!!', Toast.SHORT);
          this.setState({ReportLoading: false});
          //alert('something went wrong..!!')
        }
      })
      .catch(error => {
        this.setState({ReportLoading: false});
        //alert('post catch alert')
        //console.log('post error', error);
      });
  }

  LikePost() {
    const {access_Token, data} = this.state;
    this.setState({isliked: !this.state.isliked});
    fetch(`${url.url}/v1/post/like_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&post_id=${data.post_id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('post like response:', responseJson);
        if (responseJson.code === '200') {
          this.getSinglePost();
        } else {
          this.setState({isliked: !this.state.isliked});
          // alert('something went wrong..!!')
          Toast.show('something went wrong..!!', Toast.SHORT);
        }
      })
      .catch(error => {
        this.setState({isliked: !this.state.isliked});
        // alert('catch alert')
        Toast.show('something went wrong..!!', Toast.SHORT);
      });
  }
  DeletePost(id) {
    // alert(id)
    const {access_Token} = this.state;
    console.log(
      `access_token=${access_Token}&server_key=${url.server_key}&post_id=${id}`,
    );
    fetch(`${url.url}/v1/post/delete_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&post_id=${id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Delete post response:', responseJson);

        if (responseJson.code === '200') {
          this.props.navigation.goBack();
          Toast.show(responseJson.message, Toast.SHORT);
        } else {
          Toast.show(responseJson.errors.error_text, Toast.SHORT);
        }
      })
      .catch(error => {
        Toast.show('something went wrong..!!', Toast.SHORT);
      });
  }

  MakeFavourite(id) {
    const {access_Token} = this.state;
    fetch(`${url.url}/v1/post/add_to_favorite`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&post_id=${id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('favourite post response:', responseJson);
        if (responseJson.code === '200') {
          Toast.show(responseJson.message, Toast.SHORT);
          this.getSinglePost();
        } else {
          Toast.show(responseJson.message, Toast.SHORT);
        }
      })
      .catch(error => {
        // this.setState({ isFollowSpinner: false, isModalVisible: true })
        //alert('catch alert')
        Toast.show('something went wrong..!!', Toast.SHORT);
      });
  }

  FollowUser(id, type) {
    const {access_Token} = this.state;
    this.setState({isFollowSpinner: true});
    console.log(
      `access_token=${access_Token}&server_key=${
        url.server_key
      }&follow_id=${id}`,
      'FollowUser',
    );
    fetch(`${url.url}/v1/user/follow`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${
        url.server_key
      }&follow_id=${id}`,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('FollowUser', responseJson);
        if (responseJson.code === '200') {
          this.setState(
            {
              isFollowSpinner: false,
              openModalForReport: false,
              UserSuggestion: '',
            },
            () => {
              if (type == 'suggest') {
                setTimeout(() => {
                  this.User_Suggestion();
                }, 1000);
              } else {
                Toast.show('Unfollowed successfully', Toast.SHORT);
                this.props.navigation.goBack();
              }
            },
          );
          //alert('you follow...!')
        } else {
          this.setState({isFollowSpinner: false});
          // alert('something went wrong..!!')
        }
      })
      .catch(error => {
        this.setState({isFollowSpinner: false});
        console.log(error, 'FollowUser');
      });
  }

  render() {
    const {
      instagramView,
      feedHeader,
      header,
      headerText,
      timeHeader,
      commentView,
      commentInput,
      LikeText,
      LikeView,
    } = Styles;
    const {totalComment} = this.props.navigation.state.params.data.data;
    return this.state.isLoading ? (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="small" color="blue" />
      </View>
    ) : (
      <View style={{flex: 1}}>
        <View
          style={{
            height: height(7),
            backgroundColor: 'white',
            elevation: 1,
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
            style={{paddingHorizontal: width(4)}}>
            <Text style={{fontSize: totalSize(2), color: 'black'}}>
              <AntDesign name="arrowleft" size={totalSize(2.2)} color="black" />{' '}
              <Text style={{paddingHorizontal: width(4)}}>
                {' '}
                {' Blendzmate'}
              </Text>
            </Text>
          </TouchableOpacity>
        </View>
        <ScrollView
          contentContainerStyle={{padding: 5, backgroundColor: Colors.white}}
          showsVerticalScrollIndicator={false}>
          <View style={[instagramView, {marginTop: 0}]}>
            <View style={feedHeader}>
              <View style={header}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: 7,
                  }}>
                  {this.state.data.user_data &&
                  this.state.data.user_data.avatar ? (
                    <Image
                      style={{height: 27, width: 27, borderRadius: 20}}
                      source={{
                        uri: this.state.data.user_data.avatar,
                      }}
                    />
                  ) : (
                    <Econ1 name="user" size={25} color="grey" />
                  )}
                  <View>
                    <Text style={headerText}>{this.state.data.username}</Text>
                    <Text style={timeHeader}>{this.state.data.time_text}</Text>
                  </View>
                </View>

                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 20,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        showOptions: !this.state.showOptions,
                      });
                    }}>
                    <Entypo
                      name="dots-three-horizontal"
                      size={16}
                      color="grey"
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            {this.state.showOptions ? (
              <View
                style={{
                  width: 120,
                  borderRadius: 5,
                  elevation: 10,
                  zIndex: 99999,
                  paddingHorizontal: width(4),
                  paddingVertical: height(0.5),
                  position: 'absolute',
                  backgroundColor: 'white',
                  right: 0,
                  top: 40,
                }}>
                {NavigationConstants.user_id==this.state.data.user_id?<TouchableOpacity
                  onPress={() => this.DeletePost(this.state.data.post_id)}>
                  <Text style={{fontSize: 11, fontFamily: 'OpenSans-Bold',marginVertical:height(1)}}>
                    Delete post
                  </Text>
                </TouchableOpacity>:null}
               
                {NavigationConstants.user_id==this.state.data.user_id?<TouchableOpacity
                  onPress={() => {
                    PubSub.publish('GoToEdit', {data: this.state.data.post_id});
                  }}>
                  <Text
                    style={{
                      fontSize: 11,
                      fontFamily: 'OpenSans-Bold',
                      marginTop: 10,
                      marginVertical:height(1)
                    }}>
                    Edit post
                  </Text>
                </TouchableOpacity>:null}
           
                {NavigationConstants.user_id!=this.state.data.user_id?<TouchableOpacity
                  onPress={() => {
                    this.setState({openModalForReport: true});
                  }}>
                  <Text
                    style={{
                      fontSize: 11,
                      fontFamily: 'OpenSans-Bold',
                      marginTop: 10,
                      marginVertical:height(1)
                    }}>
                    Report
                  </Text>
                </TouchableOpacity>:null}
           
              </View>
            ) : null}

            <View
              style={{
                height: height(30),
                backgroundColor: 'grey',
                width: '100%',
              }}>
              <Swiper
                containerStyle={{height: '100%', backgroundColor: 'grey'}}
                loop={false}
                activeDotColor="blue"
                paginationStyle={{bottom: -25}}
                activeDotStyle={{
                  height: 6,
                  width: 6,
                  borderRadius: 3,
                  backgroundColor: 'red',
                }}
                dotStyle={{
                  height: 6,
                  width: 6,
                  borderRadius: 3,
                  backgroundColor: 'rgba(0,0,0,0.4)',
                }}>
                {this.state.data.media_set.map((d, i) => {
                  let str = d.file.toString();
                  let extension = str.substring(
                    str.lastIndexOf('.') + 1,
                    str.length,
                  );
                  //console.log('ext:', extension)
                  if (
                    extension === 'jpg' ||
                    extension === 'gif' ||
                    extension === 'png'
                  ) {
                    return (
                      <TouchableOpacity
                        onLongPress={() => {
                          this.setState({
                            modalImage: d.file.toString(),
                            openModalForImage: true,
                          });
                        }}>
                        <Image
                          source={{uri: d.file.toString()}}
                          style={{height: '100%', width: '100%'}}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    if (Platform.OS === 'ios') {
                      return (
                        <Video
                          source={{
                            uri: d.file.toString(),
                          }}
                          style={{width: width(80), height: height(30)}}
                          controls={true}
                          resizeMode={'cover'}
                        />
                      );
                    } else {
                      return (
                        <TouchableOpacity
                          onPress={() => {
                            PubSub.publish('_goToVideo', {
                              data: d.file.toString(),
                            });
                          }}
                          style={{
                            flex: 1,
                            backgroundColor: 'black',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <AntDesign
                            name="caretright"
                            size={totalSize(3)}
                            color="white"
                          />
                        </TouchableOpacity>
                      );
                    }
                  }
                })}
              </Swiper>
            </View>

            <View style={commentView}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={[LikeView, {flexDirection: 'row'}]}>
                  <Text style={LikeText}>{this.state.data.likes}</Text>
                  <TouchableOpacity
                    // disabled={this.state.data.is_liked?true:false}
                    onPress={() => this.LikePost()}>
                    {this.state.isliked ? (
                      <Econ1 name="heart" size={18} color="red" />
                    ) : (
                      <Econ name="heart" size={18} color="grey" />
                    )}
                  </TouchableOpacity>
                </View>
                <View
                  style={[LikeView, {marginLeft: 10, flexDirection: 'row'}]}>
                  <Text style={LikeText}>{this.state.data.votes}</Text>
                  <TouchableOpacity
                    disabled={false}
                    style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Econ name="comment" size={18} color="grey" />
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                onPress={() => this.MakeFavourite(this.state.data.post_id)}
                // disabled={this.state.data.is_saved ? true :false}
                style={{height: 70, width: 20, marginTop: 15}}>
                {this.state.data.is_saved ? (
                  <Econ1 name="bookmark" size={18} color="#FF9800" />
                ) : (
                  <Econ name="bookmark" size={18} color="grey" />
                )}
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '100%',
                height: 1,
                backgroundColor: 'rgba(0,0,0,0.1)',
              }}
            />

            <View style={[feedHeader, {height: 60}]}>
              <View style={header}>
                <Econ1 name="user" size={24} color="grey" />
                <TextInput
                  defaultValue={this.state.CommentText}
                  onChangeText={text => {
                    this.setState({CommentText: text});
                  }}
                  placeholder="Write a comment"
                  style={commentInput}
                />

                <TouchableOpacity
                  onPress={() => this.PostComment()}
                  style={{marginLeft: 5}}>
                  <Econ name="paper-plane" size={18} color="grey" />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                width: '100%',
                height: 1,
                backgroundColor: 'rgba(0,0,0,0.1)',
              }}
            />
            {this.state.data.comments.length === 0 ? (
              <Text
                style={{
                  textAlign: 'center',
                  marginVertical: 10,
                  fontSize: 11,
                  fontFamily: 'Roboto-Bold',
                }}>
                No Comment Available
              </Text>
            ) : (
              <View>
                <FlatList
                  contentContainerStyle={{paddingBottom: 10}}
                  ListFooterComponent={() => {
                    if (this.state.endOfComments) {
                      return null;
                    } else {
                      return (
                        <TouchableOpacity
                          onPress={() => {
                            this.FetchComment();
                          }}>
                          <Text
                            style={{
                              textAlign: 'center',
                              marginVertical: 10,
                              fontSize: 11,
                              fontFamily: 'Roboto-Bold',
                            }}>
                            see more comments
                          </Text>
                        </TouchableOpacity>
                      );
                    }
                  }}
                  data={this.state.comments}
                  renderItem={({item}) => {
                    // console.log()
                    if (item) {
                      return (
                        <CommentSection
                          data={item}
                          access_Token={this.state.access_Token}
                          isDeletHide={false}
                          Delete={id => this.DeleteComment(id)}
                        />
                      );
                    } else return null;
                  }}
                  keyExtractor={item =>
                    item ? item.id : Math.floor(Math.random() * 100000)
                  }
                  extraData={this.state.comments}
                />
              </View>
            )}
          </View>
        </ScrollView>

        <Modal
          isVisible={this.state.openModalForReport}
          onBackdropPress={() => this.setState({openModalForReport: false})}>
          <View
            style={{
              backgroundColor: 'white',
              width: width(80),
              height: height(30),
              alignItems: 'center',
              alignSelf: 'center',
              borderRadius: 10,
            }}>
            <View
              style={{
                padding: width(4),
                alignItems: 'center',
                height: height(16),
                justifyContent: 'center',
              }}>
              {this.state.data.user_data.avatar ? (
                <Image
                  style={{height: width(14), width: width(14)}}
                  source={{
                    uri: this.state.data.user_data.avatar,
                  }}
                />
              ) : (
                <Econ1 name="user" size={width(14)} color="grey" />
              )}
              <View style={{marginVertical: 5}}>
                <Text
                  style={{
                    fontFamily: 'Roboto-Bold',
                    fontSize: 13,
                  }}>
                  {this.state.data.username}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: height(7),
                borderTopWidth: 1,
                borderTopColor: '#f2f2f2',
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (!this.state.blockLoading) {
                    this.BlockUser();
                  }
                }}
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRightWidth: 1,
                  borderRightColor: '#f2f2f2',
                }}>
                {this.state.blockLoading ? (
                  <ActivityIndicator size="small" color="black" />
                ) : (
                  <Text style={{color: '#fa5050'}}>Block</Text>
                )}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  if (!this.state.isFollowSpinner) {
                    this.FollowUser(this.state.data.user_id, 'unfollow');
                  }
                }}
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {this.state.isFollowSpinner ? (
                  <ActivityIndicator size="small" color="black" />
                ) : (
                  <Text>Unfollow</Text>
                )}
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: height(7),
                borderTopWidth: 1,
                borderTopColor: '#f2f2f2',
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (!this.state.ReportLoading) {
                    this.RepostPost();
                  }
                }}
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {this.state.ReportLoading ? (
                  <ActivityIndicator size="small" color="black" />
                ) : (
                  <Text>Report</Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          isVisible={this.state.openModalForImage}
          onBackdropPress={() => this.setState({openModalForImage: false})}>
          <TouchableOpacity
            onPress={() => this.setState({openModalForImage: false})}
            style={{
              width: width(80),
              height: height(60),
              alignSelf: 'center',
              borderRadius: 10,
            }}>
            <Image
              style={{width: width(80), height: '100%'}}
              source={{
                uri: this.state.modalImage,
              }}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}
