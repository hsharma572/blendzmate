import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, BackHandler, Alert, ActivityIndicator } from 'react-native';
import Styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import Econ, { FA5Style } from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from 'react-native-image-picker';
import url from '../../Component/url';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import { Constants } from '../../Component/Contants';
const options = {
    title: 'Select Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

export default class AddPhoto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatarSource: '',
            ImageUri: '',
            ImageType: '',
            ImageName: '',
            isLoading: false,
            RegisterToken: '',
            isErrorModalOpen: false,
            isSkipLoading: false
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        //RegisterToken
        AsyncStorage.getItem(Constants.USER_TOKEN, (err, res) => {
            if (err === null) {
                this.setState({ RegisterToken: res.replace(/['"]+/g, '') })
            }
            else {
                alert('Something went wrong..!')
            }
        })

    }

    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    }

    UploadPhoto() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                // console.log(response);
                this.setState({ ImageUri: response.uri, ImageType: response.type, ImageName: response.fileName })
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
            }
        });
    }

    Save_Photo() {
        this.setState({ isLoading: true })
        const { ImageName, ImageType, ImageUri, RegisterToken } = this.state;
        data = new FormData()

        Data = {
            access_token: RegisterToken,
            server_key: url.server_key,
            photo: {
                uri: ImageUri,
                type: ImageType,
                name: ImageName
            }
        }

        for (let key in Data) {
            console.log(key)
            data.append(key, Data[key])
        }

        // data.append('photo', {
        //     uri: ImageUri,
        //     type: ImageType,
        //     name: ImageName
        // })

        console.log('data:', data);
        // debugger
        fetch(`${url.url2}/aj/startup/startup_image`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: data
            })
            .then(res => res.json())
            .then(data => {
                console.log(data,'image upload');
                
                if (data.status === 200) {
                    this.setState(
                        {
                            isLoading: false
                        }, () => {
                            this.props.navigation.navigate('LookingFor')
                        })
                }
                else {
                    this.setState({ isLoading: false, isErrorModalOpen: true })
                }
                //this.props.navigation.navigate('LookingFor')
            })
            .catch(e => {
                console.log(e,'photo')
                this.setState({ isLoading: false, isErrorModalOpen: true })
            })
    }

    skip() {
        // this.props.navigation.navigate('LookingFor')
        this.setState({ isSkipLoading: true })
        const { regiser_token } = this.state;
        fetch(`${url.url2}/aj/startup/skip`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: `server_key=${url.server_key}&access_token=${regiser_token}`
        })
            .then((res) => res.json())
            .then(data => {
                console.log(data)
                
                if (data.status === 200) {
                    this.setState({ isSkipLoading: false }, () => {
                        this.props.navigation.navigate('LookingFor')
                    })
                }
                else {
                    this.setState({ isSkipLoading: false, isErrorModalOpen: true })
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({ isSkipLoading: false, isErrorModalOpen: true })
            })
    }

    render() {
        const { MainContainer } = Styles;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={MainContainer}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 1 }}
                        colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.70)']}
                        style={{ height: 70, width: '100%', padding: 20, flexDirection: 'row', justifyContent: 'space-between', borderTopLeftRadius: 8, borderTopRightRadius: 8 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Econ name="camera" size={20} color="white" />
                            <Text style={{ marginLeft: 10, fontSize: 18, color: 'white', letterSpacing: 1, fontFamily: 'Roboto-Bold' }}>Add a photo</Text>
                        </View>

                        <TouchableOpacity

                        //this.skip()
                            onPress={() =>  this.props.navigation.navigate('LookingFor')}
                            activeOpacity={1}
                            style={{ height: 30, width: 60, backgroundColor: 'white', borderRadius: 15, justifyContent: 'center', alignItems: 'center' }}>
                            {
                                this.state.isSkipLoading
                                    ?
                                    <ActivityIndicator size='small' color='black' />
                                    :
                                    <Text style={{ fontFamily: 'OpenSans-Regular', fontSize: 11, color: 'rgba(0,0,0,0.5)' }}>Skip</Text>
                            }
                        </TouchableOpacity>

                    </LinearGradient>

                    <TouchableOpacity
                        onPress={() => this.UploadPhoto()}
                        style={{
                            height: 120,
                            width: 120,
                            borderRadius: 60,
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignSelf: 'center',
                            marginTop: 80,
                            borderWidth: 1,
                            borderStyle: 'dashed',
                            borderColor: 'rgba(0,0,0,0.6)'
                        }}>
                        {
                            this.state.avatarSource
                                ?
                                <ImageBackground
                                    imageStyle={{ borderRadius: 60 }}
                                    source={this.state.avatarSource}
                                    style={{ height: '100%', width: '100%' }}>
                                </ImageBackground>
                                :
                                <ImageBackground source={require('../../../assets/Images/camera.png')} style={{ height: 50, width: 50 }}>
                                </ImageBackground>
                        }
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.Save_Photo()}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 1 }}
                            colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                            style={{ height: 40, width: '50%', alignItems: 'center', justifyContent: 'center', marginTop: 60, borderRadius: 20, alignSelf: 'center' }}>
                            {
                                this.state.isLoading
                                    ?
                                    <ActivityIndicator size='small' color='white' />
                                    :
                                    <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>Save & Continue</Text>
                            }
                        </LinearGradient>
                    </TouchableOpacity>
                </View>

                <Modal isVisible={this.state.isErrorModalOpen}>
                    <View
                        style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >

                        {/* <Econ style={{ alignSelf: 'center' }} name="times-circle" size={34} color={this.state.SuccesMessage?'green':'red'} /> */}
                        <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
                        <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                        <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
                        <TouchableOpacity
                            onPress={() => { this.setState({ isErrorModalOpen: !this.state.isErrorModalOpen }) }}
                            style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                            activeOpacity={1}>
                            <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        );
    }
}
