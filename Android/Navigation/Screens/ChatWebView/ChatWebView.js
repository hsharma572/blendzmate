import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import { Constants } from '../../Component/Contants';
import { NavigationActions } from 'react-navigation';

export default class ChatWebView extends Component {
    constructor(props) {
        super(props);
        ////https://blendzmate.com/messages
        console.log('tokennnnn:', this.props.navigation)
        this.state = {
            Base_UrlName:'Messages',
            New_url:'',
            visible: true,
            username: this.props.navigation.getParam('user'),
            access_Token: this.props.navigation.getParam('token'),
            isLoading: false,
            HtmlView:this.createHTML(url.url2+'/messages/'+this.props.navigation.getParam('user')),
        };
    }


    createHTML = link => {
        return `<html><script>function runthis(){const body = 'access_token=${this.props.navigation.getParam('token')}&server_key=${url.server_key}&webview_type=app';
            
        post('${link}',{access_token: '${this.props.navigation.getParam('token')}',server_key:'${url.server_key}',webview_type:'app'});
    }
    
    function post(path, params, method='post') {

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        const form = document.createElement('form');
        form.method = method;
        form.action = path;
      
        for (const key in params) {
          if (params.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = params[key];
      
            form.appendChild(hiddenField);
          }
        }
      
        document.body.appendChild(form);
        form.submit();
      }
      setTimeout(function(){ runthis(); }, 400);
    
    </script><body></body></html>`;
      };



    renderLoading = () => <View style={{ flex: 1 }}><ActivityIndicator animating size='large' color='blue'  /></View>
    render() {
        return (
            this.state.isLoading
                ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='small' color='blue' animating />
                </View>
                :
                <View style={{ flex: 1 }}>
                    <WebView
                        ref={webview => { this.webview = webview; }}
                        javaScriptEnabled={true}
                        renderLoading={this.renderLoading}
                        startInLoadingState={true}
                        source={{ html: this.state.HtmlView }}
                        onNavigationStateChange={(state)=>{
                            console.log(state,'messages')
                            if(state.url === url.url2+'/messages' && state.loading === true){
                                // debugger
                                const navigateAction = NavigationActions.navigate({
                                    routeName: 'Messages',
                                    params: {},
                                    action: NavigationActions.navigate({ routeName: 'Messages' }),
                                  });
                                  this.props.navigation.dispatch(navigateAction);
                            }

                            if(state.url.includes('blendsmate')||state.url.includes('blendzmate')){
              
              let html=this.createHTML(state.url);
               this.setState({
              HtmlView: html,
            });
            }
                            // if(state.title === 'Messages' && state.loading === false){
                            //     this.setState({Base_UrlName:state.url},()=>console.log(this.state.Base_UrlName))
                            // }
                            // if(this.state.Base_url)
                        }}
                    />
                </View>
        );
    }
}
