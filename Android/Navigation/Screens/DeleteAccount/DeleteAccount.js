import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';

export default class DeleteAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { Label, TxtInpt } = Styles;
        return (
            <ScrollView 
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20 }}>
                <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}>Delete account? </Text>
                <Text style={{ fontSize: 9, fontFamily: 'OpenSans-Regular', color: "#333" }}>Are you sure you want to delete your account? All content will be permanently removed! </Text>
                <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                    <Text style={Label}>Password</Text>
                    <TextInput underlineColorAndroid="transparent" style={TxtInpt} />
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5,borderWidth:1,borderColor:'#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Continue</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
