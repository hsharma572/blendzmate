const Styles = {
    InputView: {
        height: 35, width: '100%', borderRadius: 20, borderWidth: 1, flexDirection: 'row', alignItems: 'center',borderColor: 'rgba(0,0,0,0.3)',
    },
    InputText: {
        paddingVertical:2,
        height: '100%',
        paddingHorizontal: 15,
        fontSize: 11,
        fontFamily: 'OpenSans-Regular',
        width: '85%',
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20
    },
    SearchButtonView: {
        height: '100%',
        width: 50, borderBottomRightRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopRightRadius: 20,
    }
}

export default Styles;