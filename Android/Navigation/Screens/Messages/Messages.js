import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity, TextInput } from 'react-native';
import Styles from './style';
import { Constants } from '../../Component/Contants';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import ChatUserList from '../../Component/ChatUserList/ChatUserList';
import Modal from "react-native-modal";
import ChatWebView from '../ChatWebView/ChatWebView';
import { createStackNavigator } from 'react-navigation-stack';
import { withNavigation } from 'react-navigation';
import Econ from 'react-native-vector-icons/FontAwesome5';

class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      access_Token: '',
      ChatData: '',
      isLoading: false,
      isModalVisible: false
    };
  }


  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      console.log('focused');
      this.get_data();
    });
    AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
      if (error === null) {
        this.setState({ access_Token: result.replace(/"/g, '').trim() }, () => this.get_data())
      }
      else {
        this.setState({ isModalVisible: true })
      }
    })
  }


  componentWillUnmount() {
    this.focusListener.remove();
  }


  get_data() {
    this.setState({ isLoading: true })
    const { access_Token } = this.state
    fetch(`${url.url}/v1/messages/get_chats`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('user post response:', responseJson);
        if (responseJson.code === '200') {
          console.log('chat data:', responseJson.data);
          this.setState({ ChatData: responseJson.data, isLoading: false })
        }
        else {
          this.setState({ isLoading: false, isModalVisible: true })
          //alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false, isModalVisible: true })
        //alert('post catch alert')
        //console.log('chat error', error);
      })
  }

  render() {
    const { InputView, InputText, SearchButtonView } = Styles;
    return (
      <View style={{ flex: 1, padding: 15 }}>
        {
          this.state.isLoading
            ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size='small' color='blue' />
            </View>
            :
            this.state.ChatData.length === 0
              ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 13, fontFamily: 'Roboto-Regular' }}>No Data available</Text>
              </View>
              :
              <View style={{ flex: 1 }}>
                <View style={InputView}>
                  <TextInput style={InputText} placeholder="Search..." placeholderTextColor="rgba(0,0,0,0.3)" />
                  <TouchableOpacity style={SearchButtonView}>
                    <Econ name='search' size={15} color="rgba(0,0,0,0.3)"/>
                  </TouchableOpacity>
                </View>

                <FlatList
                  style={{ marginTop: 10 }}
                  
                  data={this.state.ChatData}
                  renderItem={(data) => { 
                    return <ChatUserList
                      data={data}
                      OpenWebView={(username) => this.props.navigation.navigate('ChatWebView', { user: username, token:this.state.access_Token })}
                       />
                  }}
                />
              </View>
        }
        <Modal isVisible={this.state.isModalVisible}>
          <View
            style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >
            <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
            <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
            <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
            <TouchableOpacity
              onPress={() => { this.setState({ isModalVisible: !this.state.isModalVisible }) }}
              style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
              activeOpacity={1}>
              <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const MessageStack = createStackNavigator({
  Messages: Messages,
  ChatWebView: ChatWebView
}, {
  headerMode: 'none'
})

export default withNavigation(MessageStack)