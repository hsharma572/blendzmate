const Styles = {
    ProfileView:{
        height:200,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center'
    },
    Heading:{
        fontFamily: 'OpenSans-Bold',
        fontSize: 20,
        color:'white'
    },
    TxtInput:{
        height:40,
        width:250,
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        marginTop: 20,
        color:'white',
        fontFamily:'OpenSans-Regular',
        fontSize:13
    },
    ListView:{
        marginTop:15,
        paddingHorizontal: 15,
    }
}

export default Styles;