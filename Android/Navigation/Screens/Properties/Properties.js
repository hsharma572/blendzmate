// import React, { Component } from 'react';
// import { View, Text, ScrollView, TextInput, FlatList } from 'react-native';
// import Styles from './style';
// import LinearGradient from 'react-native-linear-gradient';
// import PropertiesList from '../../Component/PropertiesList';
// import { createStackNavigator } from 'react-navigation-stack';
// import PropertiesDetails from '../PropertiesDetails/PropertiesDetails';
// import { createAppContainer } from 'react-navigation';

// const color = ['#6BC6E4', '#7196B1', '#E63E3A'];

// class Properties extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

//   render() {
//     const { ProfileView, Heading, TxtInput, ListView } = Styles;
//     return (
//       <ScrollView showsVerticalScrollIndicator={false}>
//         <LinearGradient
//           start={{ x: 0, y: 0 }}
//           end={{ x: 1, y: 1 }}
//           colors={color}
//           style={ProfileView}>
//           <Text style={Heading}>Properties</Text>
//           <Text style={[Heading, { fontFamily: 'OpenSans-Regular', fontSize: 13, marginTop: 5 }]}>Explore properties around you</Text>
//           <TextInput style={TxtInput} placeholder="Search Locality" placeholderTextColor="white" />
//         </LinearGradient>
//         <View style={ListView}>
//           <FlatList
//             data={[{ key: 'a' }, { key: 'b' }, { key: 'c' }]}
//             renderItem={(data) => {
//               return <PropertiesList data={data} key={data.key} navigation={this.props.navigation} />
//             }}
//           />
//         </View>
//       </ScrollView>
//     );
//   }
// }

// const PropertiesStack = createStackNavigator({
//   Properties:Properties,
//   Details:PropertiesDetails
// },{
//   headerMode:'none'
// })

// export default createAppContainer(PropertiesStack);

import React, { Component } from 'react';
import { View, PermissionsAndroid, Platform, ActivityIndicator } from 'react-native';
import Styles from './style';
import Geolocation from 'react-native-geolocation-service';
import { WebView } from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import { Constants } from '../../Component/Contants';
import {NavigationConstants} from '../../commonValues'


export default class Properties extends Component {
  constructor(props) {
    super(props);
    // console.log(props.screenProps)
    // console.log(props)
    this.state = {
      isLoading: true,
      lat: NavigationConstants.lat,
      long: NavigationConstants.long,
      access_Token: NavigationConstants.accessToken,
      HtmlView: '',
      webKey:0
    };
  }



  componentWillUnmount(){
    this.focusListener.remove();
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
    
       
        this.setState({webKey:this.state.webKey+1});
    
       
     });
     console.log(NavigationConstants,'NVS');
     if(NavigationConstants.locationLoaded){
      this.setState({
        lat: NavigationConstants.lat,
        long: NavigationConstants.long,
        HtmlView: this.createHTML(url.url2 + '/properties'),
          isLoading: false,
      });
    }
  

  }


  createHTML = link => {
    return `<html><script>function runthis(){const body = 'access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app';
    post('${link}',{access_token: '${this.state.access_Token}',server_key:'${url.server_key}',webview_type:'app',lat:'${this.state.lat}',lng:'${this.state.long}'});
}

function post(path, params, method='post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;
  
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];
  
        form.appendChild(hiddenField);
      }
    }
  
    document.body.appendChild(form);
    form.submit();
  }
  setTimeout(function(){ runthis(); }, 400);

</script><body></body></html>`;
  };
  renderLoading = () => <View style={{ flex: 1 }}><ActivityIndicator animating size='large' color='blue'  /></View>

  render() {

    return (
      this.state.isLoading
        ?
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size='small' color='blue' animating />
        </View>
        :
        <View style={{ flex: 1 }}>
          <WebView
          key={this.state.webKey}
            javaScriptEnabled={true}
            startInLoadingState={true}
            renderLoading={this.renderLoading}
            //evaluateJavaScript={this.injectjs()}
            //injectJavaScript={this.Fetch}
            source={{ html: this.state.HtmlView }}
            onNavigationStateChange={(state)=>{
                     
                    
                     if(state.url.includes('blendsmate')||state.url.includes('blendzmate')){
       
       let html=this.createHTML(state.url);
        this.setState({
       HtmlView: html,
     });
     }
                    
                     
                 }}
          />
        </View>
    );
  }
}
