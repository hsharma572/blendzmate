import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import FollowersList from '../../Component/FollowersList';
import url from '../../Component/url';
import { Constants } from '../../Component/Contants';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
export default class AllSuggestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            UserSuggestion: [],
            isModalVisible: false,
            isSpinner: '',
            id: ''
        };
    }

    componentDidMount() {
        // this.setState({ isLoading: true })
        AsyncStorage.getItem(Constants.USER_TOKEN, (error, result) => {
            if (error === null) {
                this.setState({ access_Token: result.replace(/"/g, '').trim() }, () => this.getUserSuggestion(true))
            }
            else {
                alert('Something Went wrong...!!')
            }
        })
    }

    getUserSuggestion(loading) {
        this.setState({isLoading:loading})
        const { access_Token } = this.state
        fetch(`${url.url}/v1/user/fetch_suggestions`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            //timeout: 2000,
            body: `access_token=${access_Token}&server_key=${url.server_key}`
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('user suggestions response:', responseJson);
                if (responseJson.code === '200') {
                    console.log('user suggestion', responseJson.data);
                    this.setState({ UserSuggestion: responseJson.data, isLoading: false }, () => {
                        console.log('state user suggestion:', this.state.UserSuggestion);
                    })
                }
                else {
                    this.setState({ isLoading: false, isModalVisible: true })
                    //alert('something went wrong..!!')
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false, isModalVisible: true })
                //alert('suggestion catch alert')
            })
    }

    FollowUser(id) {
        const { access_Token } = this.state
        this.setState({ isFollowSpinner: true, isSpinner: true, id: id })
        fetch(`${url.url}/v1/user/follow`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            //timeout: 2000,
            body: `access_token=${access_Token}&server_key=${url.server_key}&user_id=${id}`
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('post follow response:', responseJson);
                if (responseJson.code === '200') {
                    this.setState({ isFollowSpinner: false, isSpinner: false }, () => {
                        // setTimeout(() => {
                            this.getUserSuggestion(false);
                        // }, 1000);
                    })
                    //alert('you follow...!')
                }
                else {
                    this.setState({ isFollowSpinner: false, isModalVisible: true })
                    // alert('something went wrong..!!')
                }
            })
            .catch((error) => {
                this.setState({ isFollowSpinner: false, isModalVisible: true })
                //alert('catch alert')
            })
    }

    render() {
        return (
            this.state.isLoading
                ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="small" color='blue' />
                </View>
                :
                <View style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 20 }}>
                    <Text style={{ fontSize: 14, fontFamily: 'Roboto-Bold' }}>Suggestions For You</Text>
                    {
                        this.state.UserSuggestion.length === 0
                            ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' ,}}>
                                <Text style={{ fontSize: 14, fontFamily: 'Roboto-Bold', color:'rgba(0,0,0,0.4)' }}>No Data Available</Text>
                            </View>
                            :
                            <View style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 20 }}>
                                <FlatList
                                    style={{ marginTop: 15 }}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.UserSuggestion}
                                    renderItem={(data) => {
                                        return <FollowersList
                                            data={data}
                                            id={this.state.id}
                                            isSpinner={this.state.isSpinner}
                                            Follow={(id) => this.FollowUser(id)} isFollower={true} />
                                    }}
                                    keyExtractor={(data) => data.user_id.toString()}
                                />

                                <Modal isVisible={this.state.isModalVisible}>
                                    <View
                                        style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >
                                        <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
                                        <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                                        <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
                                        <TouchableOpacity
                                            onPress={() => { this.setState({ isModalVisible: !this.state.isModalVisible }) }}
                                            style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                                            activeOpacity={1}>
                                            <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Modal>
                            </View>
                    }
                </View>
        );
    }
}
