import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import RadioForm from 'react-native-simple-radio-button';

var Gender_item = [
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' }
];

export default class General extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Gender: '',
            GenderView: false
        };
    }

    render() {
        const { Label, TxtInpt } = Styles;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20, flex: 1 }}>
                <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}> General </Text>
                <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                    <Text style={Label}>Username</Text>
                    <TextInput underlineColorAndroid="transparent" style={TxtInpt} />

                    <Text style={[Label, { marginTop: 15 }]}>E-mail</Text>
                    <TextInput underlineColorAndroid="transparent" style={TxtInpt} />

                    <Text style={[Label, { marginTop: 15 }]}>Gender</Text>
                    
                    <TouchableOpacity onPress={()=>this.setState({GenderView:true})}>
                    <TextInput
                        editable={false}
                        value={this.state.Gender}
                        placeholder="Male"
                        underlineColorAndroid="transparent" style={TxtInpt}
                    />
                    </TouchableOpacity>
                    {
                        this.state.GenderView
                            ?
                            <View style={{ marginTop: 10, width: '70%', backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={Gender_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={5}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ Gender: value }, () => { setTimeout(() => { this.setState({ GenderView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Country</Text>
                    <TextInput
                        placeholder="Select Country"
                        underlineColorAndroid="transparent" style={TxtInpt} />
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, borderWidth: 1, borderColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
