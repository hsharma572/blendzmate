import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import RadioForm from 'react-native-simple-radio-button';

var Smoke_item = [
    { label: 'Yes, I do.', value: 'Yes, I do.' },
    { label: 'No, I don\'t.', value: 'No, I don\'t.' },
];

var Introvert_item = [
    { label: 'Introvert', value: 'Introvert' },
    { label: 'Extrovert', value: 'Extrovert' },
];

var Pets_item = [
    { label: 'Yes, I do have.', value: 'Yes, I do have.' },
    { label: 'No, I don\'t have.', value: 'No, I don\'t have.' },
];

var Employed_item = [
    { label: 'Yes, I am.', value: 'Yes, I am.' },
    { label: 'No, I am not.', value: 'No, I am not.' },
];

var Organized_item = [
    { label: 'I am organized', value: 'I am organized' },
    { label: 'I am somewhat organized', value: 'I am somewhat organized' },
    { label: 'I am not organized', value: 'I am not organized' },
];

var Married_item = [
    { label: 'Yes, I am', value: 'Yes, I am' },
    { label: 'No, I am not', value: 'No, I am not' },
    //{ label: 'I am not organized', value: 'I am not organized' },
];

var Children_item = [
    { label: 'Yes, I have', value: 'Yes, I have' },
    { label: 'No, I don\'t have', value: 'No, I don\'t have' },
];

var Eating_item = [
    { label: 'I prefer eating out', value: 'I prefer eating out' },
    { label: 'I prefer eating at home', value: 'I prefer eating at home' },
];

var LouadPerson_item = [
    { label: 'I am loud person', value: 'I am loud person' },
    { label: 'I am moderate', value: 'I am moderate' },
    { label: 'I am quite person', value: 'I am quite person' }
];

var Handicap_item = [
    { label: 'Yes, I am physically challeged', value: 'Yes, I am physically challeged' },
    { label: 'Yes, I have hearing problem', value: 'Yes, I have hearing problem' },
    { label: 'Yes, I am Visually impaired', value: 'Yes, I am Visually impaired' },
    { label: 'No, Not required', value: 'No, Not required' }
];

export default class Interests extends Component {
    constructor(props) {
        super(props);
        this.state = {
            smoke: '',
            introvert: '',
            pets: '',
            employed: '',
            organized: '',
            engaged: '',
            children: '',
            eatingHome: '',
            quitePerson: '',
            handicap: '',
            SmokeView: false,
            IntrovertView: false,
            PetsView: false,
            EmployedView:false,
            OrganizedView:false,
            MarriedView:false,
            ChildrenView:false,
            EatingView:false,
            LaudPersonView:false,
            HandicapView:false,
            avatarSource:''
        };
    }

   

    render() {
        const { Label, TxtInpt } = Styles;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20 }}>
                <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}> Interests </Text>
                <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                    <Text style={Label}>Do you smoke?</Text>
                    <TouchableOpacity onPress={() => this.setState({ SmokeView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.smoke}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.SmokeView
                            ?
                            <View style={{ marginTop: 10, width: 200, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={Smoke_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ smoke: value }, () => { setTimeout(() => { this.setState({ SmokeView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Are you an introvert or an extrovert?</Text>
                    <TouchableOpacity onPress={() => this.setState({ IntrovertView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.introvert}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.IntrovertView
                            ?
                            <View style={{ marginTop: 10, width: 190, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={Introvert_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ introvert: value }, () => { setTimeout(() => { this.setState({ IntrovertView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Do you have pets?</Text>
                    <TouchableOpacity onPress={() => this.setState({ PetsView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.pets}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.PetsView
                            ?
                            <View style={{ marginTop: 10, width: 250, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={Pets_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ pets: value }, () => { setTimeout(() => { this.setState({ PetsView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Are you employed?</Text>
                    <TouchableOpacity onPress={() => this.setState({ EmployedView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.employed}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.EmployedView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                    radio_props={Employed_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ employed: value }, () => { setTimeout(() => { this.setState({ EmployedView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Would you consider yourself a organized, somewhat organized, or not organized at all?</Text>
                    <TouchableOpacity onPress={() => this.setState({ OrganizedView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.organized}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.OrganizedView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between' }}
                                    radio_props={Organized_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    //formHorizontal={true}
                                    onPress={(value) => { this.setState({ organized: value }, () => { setTimeout(() => { this.setState({ OrganizedView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Are you married or engaged?</Text>
                    <TouchableOpacity onPress={() => this.setState({ MarriedView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.engaged}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.MarriedView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between',alignItems:'center' }}
                                    radio_props={Married_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ engaged: value }, () => { setTimeout(() => { this.setState({ MarriedView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Do you have children?</Text>
                    <TouchableOpacity onPress={() => this.setState({ ChildrenView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.children}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.ChildrenView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between',alignItems:'center' }}
                                    radio_props={Children_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ children: value }, () => { setTimeout(() => { this.setState({ ChildrenView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }


                    <Text style={[Label, { marginTop: 15 }]}>Do you prefer eating out or eating at home?</Text>
                    <TouchableOpacity onPress={() => this.setState({ EatingView: true })}>
                    <TextInput
                        editable={false}
                        value={this.state.eatingHome}
                        underlineColorAndroid="transparent"
                        style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.EatingView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between' }}
                                    radio_props={Eating_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    //formHorizontal={true}
                                    onPress={(value) => { this.setState({ eatingHome: value }, () => { setTimeout(() => { this.setState({ EatingView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }


                    <Text style={[Label, { marginTop: 15 }]}>Would you consider yourself a loud person or a quiet person?</Text>
                    <TouchableOpacity onPress={() => this.setState({ LaudPersonView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.quitePerson}
                            underlineColorAndroid="transparent"
                            style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.LaudPersonView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between' }}
                                    radio_props={LouadPerson_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    //formHorizontal={true}
                                    onPress={(value) => { this.setState({ quitePerson: value }, () => { setTimeout(() => { this.setState({ LaudPersonView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }

                    <Text style={[Label, { marginTop: 15 }]}>Do require any special accommodations(handicap, hearing impaired, visually impaired)?</Text>
                    <TouchableOpacity onPress={() => this.setState({ HandicapView: true })}>
                        <TextInput
                            editable={false}
                            value={this.state.handicap}
                            underlineColorAndroid="transparent" style={TxtInpt} />
                    </TouchableOpacity>
                    {
                        this.state.HandicapView
                            ?
                            <View style={{ marginTop: 10, width: 230, backgroundColor: '#f1f1f1', padding: 10, justifyContent: 'center' }}>
                                <RadioForm
                                    style={{ justifyContent: 'space-between' }}
                                    radio_props={LouadPerson_item}
                                    initial={-1}
                                    selectedLabelColor={'#333'}
                                    buttonSize={7}
                                    labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                    labelColor={'#333'}
                                    buttonColor={'#333'}
                                    selectedButtonColor={'#333'}
                                    onPress={(value) => { this.setState({ handicap: value }, () => { setTimeout(() => { this.setState({ HandicapView: false }) }, 100) }) }}
                                />
                            </View>
                            :
                            null
                    }
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, borderWidth: 1, borderColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
