import React, { Component } from 'react';
import { View, PermissionsAndroid, Platform, ActivityIndicator } from 'react-native';
import Styles from './style';
import Geolocation from 'react-native-geolocation-service';
import { WebView } from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import { Constants } from '../../Component/Contants';
import { NavigationConstants } from '../../commonValues';

export default class Explore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: NavigationConstants.lat,
      long: NavigationConstants.long,
      access_Token:NavigationConstants.accessToken ,
      isLoading: true,
      HtmlView: '',webKey:0,
    };
  }

  

  componentWillUnmount(){
    this.focusListener.remove();
  }

  async componentDidMount() {

    this.focusListener = this.props.navigation.addListener('didFocus', () => {
     if(this.webview){
       console.log('reloadeed explore');
      this.setState({webKey:this.state.webKey+1});
     }
      
    });


    if(NavigationConstants.locationLoaded){
      this.setState({
        lat: NavigationConstants.lat,
        long: NavigationConstants.long,
        HtmlView: this.createHTML(url.url2 + '/explore'),
          isLoading: false,
      });
    }

  
  }
  createHTML = link => {
    return  `<html><script>function runthis(){const body = 'access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app';
    post('${link}',{access_token: '${this.state.access_Token}',server_key:'${url.server_key}',webview_type:'app',lat:'${this.state.lat}',lng:'${this.state.long}'});
}

function post(path, params, method='post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;
  
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];
  
        form.appendChild(hiddenField);
      }
    }
  
    document.body.appendChild(form);
    form.submit();
  }
  setTimeout(function(){ runthis(); }, 400);

</script><body></body></html>`;
  };



  renderLoading = () => <View style={{ flex: 1 }}><ActivityIndicator animating size='large' color='blue'  /></View>

  render() {
    return (
      this.state.isLoading
        ?
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
          <ActivityIndicator size='small' color='blue' animating />
        </View>
        :
        <View style={{ flex: 1 }}>
          <WebView
            //onMessage={(event) => console.log('mmmmm:', event.nativeEvent.data)}
            ref={webview => { this.webview = webview; }}
            javaScriptEnabled={true}
            renderLoading={this.renderLoading}
            startInLoadingState={true}
            onLoadEnd={syntheticEvent => {
    // update component to be aware of loading status
    const { nativeEvent } = syntheticEvent;
    // this.isLoading = nativeEvent.loading;
    console.log(nativeEvent,'explore');

  }}
  key={this.state.webKey}
            source={{ html: this.state.HtmlView }}
            onNavigationStateChange={(state)=>{
                     
                    console.log(state,'explore');
                     if(state.url.includes('blendsmate')||state.url.includes('blendzmate')){
       
       let html=this.createHTML(state.url);
        this.setState({
       HtmlView: html,
     });
     }
     if(state.url.includes("/post/")){
                let post_id=res = state.url.split("/post/");
                
                PubSub.publish('_goToCommentScreen', { data: post_id[1] });
                this.webview.stopLoading();
              return false;
                
            }
                    
                     
                 }}
          />
        </View>
    );
  }
}

  // getWebView() {
  //   fetch(`https://blendzmate.com/explore`, {
  //     method: 'POST',
  //     headers: {
  //       "Content-Type": "application/x-www-form-urlencoded",
  //     },
  //     body: `access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app&lat=${this.state.lat}&lng=${this.state.long}`
  //   })
  //     .then((response) => response.text())
  //     .then((res) => {
  //       this.setState({ HtmlView: res, isLoading: false })
  //       console.log('explore injected:', res)
  //     })
  //     .catch((err) => console.log(err))
  // }