import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity, RefreshControl } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import FollowersList from '../../Component/FollowersList';
import url from '../../Component/url';
import Econ from 'react-native-vector-icons/FontAwesome5';

export default class Followers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      access_Token: '',
      user_id: '',
      FollowerData: '',
      isLoading: false
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    AsyncStorage.multiGet([Constants.USER_TOKEN, Constants.USER_ID], (error, result) => {
      if (error === null) {
        console.log('result', result[0][1]);
        this.setState({ access_Token: result[0][1].replace(/"/g, '').trim(), user_id: result[1][1] }, () => this.FetchFollowers());
      }
      else {
        alert('Something Went wrong...!!')
      }
    })
  }

  FetchFollowers() {
    const { access_Token, user_id } = this.state
    fetch(`${url.url}/v1/user/followers`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&user_id=${user_id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Follower user response:', responseJson);
        if (responseJson.code === '200') {
          this.setState({ FollowerData: responseJson.data, isLoading: false })
        }
        else {
          this.setState({ isLoading: false })
          alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false })
        alert('catch alert')
      })
  }

  render() {
    return (
      <View style={{ flex: 1, paddingVertical: 30, paddingHorizontal: 20 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}>Followers</Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Econ name='arrow-left' size={13} color='black' />
            <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Bold', color: "#333", marginLeft: 5 }}>back</Text>
          </TouchableOpacity>
        </View>
        {
          this.state.isLoading
            ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size='small' color='blue' />
            </View>
            :
            this.state.FollowerData.length === 0
              ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Bold', color: "rgba(0,0,0,0.4)" }}>
                  No Data Available
                </Text>
              </View>
              :
              <FlatList
                style={{ marginTop: 15 }}
                showsVerticalScrollIndicator={false}
                data={this.state.FollowerData}
                renderItem={(data) => {
                  return <FollowersList data={data} key={data.item.user_id} isFollower={true} />
                }}
                keyExtractor={(data) => data.user_id.toString()}
              />
        }
      </View>
    );
  }
}
