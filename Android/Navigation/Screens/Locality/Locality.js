import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, ImageBackground, CheckBox, ScrollView, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Econ1 from 'react-native-vector-icons/FontAwesome5';
import RadioForm from 'react-native-simple-radio-button';
import DateTimePicker from "react-native-modal-datetime-picker";
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import Modal from 'react-native-modal';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { NavigationConstants } from '../../commonValues';
import { height } from 'react-native-dimension';
import Toast from 'react-native-simple-toast';

var Gender_item = [
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' },
    { label: 'Other', value: 'other' }
];

var Smoke_item = [
    { label: 'Yes, I do.', value: 'smoke' },
    { label: 'No, I don\'t.', value: 'nosmoke' },
];

var Introvert_item = [
    { label: 'Introvert', value: 'introvert' },
    { label: 'Extrovert', value: 'extrovert' },
];

var Pets_item = [
    { label: 'Yes, I do have.', value: 'pet' },
    { label: 'No, I don\'t have.', value: 'nopet' },
];

var Employed_item = [
    { label: 'Yes, I am.', value: 'employed' },
    { label: 'No, I am not.', value: 'unemployed' },
];

var Organized_item = [
    { label: 'I am organized', value: 'organized' },
    { label: 'I am somewhat organized', value: 'somewhatorganized' },
    { label: 'I am not organized', value: 'notorganized' },
];

var Married_item = [
    { label: 'Yes, I am', value: 'married' },
    { label: 'No, I am not', value: 'notmarried' },
    //{ label: 'I am not organized', value: 'I am not organized' },
];

var Children_item = [
    { label: 'Yes, I have', value: 'child' },
    { label: 'No, I don\'t have', value: 'nochild' },
];

var Eating_item = [
    { label: 'I prefer eating out', value: 'eatout' },
    { label: 'I prefer eating at home', value: 'eathome' },
];

var LouadPerson_item = [
    { label: 'I am loud person', value: 'loud' },
    { label: 'I am moderate', value: 'moderate' },
    { label: 'I am quiet person', value: 'quite' }
];

var Handicap_item = [
    { label: 'Yes, I am physically challeged', value: 'physical' },
    { label: 'Yes, I have hearing problem', value: 'hearing' },
    { label: 'Yes, I am Visually impaired', value: 'visually' },
    { label: 'No, Not required', value: 'notchallenged' }
];




const color = ['rgba(237,56,51,1)', 'rgba(69,183,221,0.70)'];

export default class Locality extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AboutView: false,
            CheckBoxTrue: false,
            InterestView: false,
            NotificationView: false,
            LocalityView: true,
            GenderView: false,
            Gender: '',
            SmokeView: false,
            Smoke: '',
            Introvert: '',
            IntrovertView: false,
            PetsView: false,
            Pets: '',
            EmployedView: true,
            Employed: '',
            Organized: '',
            OrganizedView: false,
            Married: '',
            MarriedView: false,
            Children: '',
            ChildrenView: false,
            EatingView: false,
            Eating: '',
            LaudPersonView: false,
            LaudPerson: '',
            Handicap: '',
            HandicapView: false,
            isDateTimePickerVisible: false,
            date: 'dd',
            Month: 'mm',
            Year: 'yyyy',
            FirstName: '',
            LastName: '',
            MobileNo: '',
            WhatWork: '',
            WhereWork: '',
            Bio: '',
            Country: '',
            birthDate: '',
            regiser_token: '',
            isLoading: false,
            isInterestLoading: false,
            isErrorModalOpen: false,
            user_id: '',
            isSkipLoading: false,
            latitude: 80.0000,
            longitude: 60.0000,
            location: 'rajkot',
            genIndex:-1,
            smokeIndex:-1,
            introIndex:-1,
            petIndex:-1,
            employedIndex:-1,
            orgIndex:-1,
            martialIndex:-1,
            childIndex:-1,
            eatIndex:-1,
            loudIndex:-1,
            handIndex:-1
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('USER_TOKEN', (err, res) => {
            if (err === null) {
                console.log(res,res.replace(/['"]+/g, ''));
                this.setState({ regiser_token: res.replace(/['"]+/g, '') })
            }
        })
        AsyncStorage.getItem('USER_ID', (err, res) => {
            if (err === null) {
                this.setState({ user_id: res.replace(/['"]+/g, '') })
            }
        })
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        console.log("A date has been picked: ", date.getDate(), date.getMonth() + 1, date.getFullYear());
        this.setState({ date: date.getDate(), Month: date.getMonth() + 1, Year: date.getFullYear(), birthDate: date.getDate() + "/" + date.getDate() + "/" + date.getFullYear() })
        this.hideDateTimePicker();
    };

    Start_UpInfo() {
        var val = this.state.MobileNo;
        //check if mobile number is ten digits
        if (/^\d{10}$/.test(val)) {   
        } else {
            Toast.show('Invalid number; must be ten digits', Toast.SHORT);
            return 
        }
        this.setState({ isLoading: true })
        const { FirstName, LastName, Gender, birthDate, MobileNo, Country, WhatWork, WhereWork, Bio, regiser_token, user_id } = this.state;
        
        fetch(url.url2+'/aj/startup/startup_info', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: `fname=${FirstName}&lname=${LastName}&gender=${Gender}&dob=${birthDate}&phone=${MobileNo}&country=${Country}&work=${WhatWork}&workplace=${WhereWork}&bio=${Bio}&server_key=${url.server_key}&access_token=${regiser_token}&user_update_id=${user_id}&user_id=${user_id}`
        })
            .then((res) => res.json())
            .then(data => {
                console.log(data,`fname=${FirstName}&lname=${LastName}&gender=${Gender}&dob=${birthDate}&phone=${MobileNo}&country=${Country}&work=${WhatWork}&workplace=${WhereWork}&bio=${Bio}&server_key=${url.server_key}&access_token=${regiser_token}&user_update_id=${user_id}&user_id=${user_id}`)
               
                if (data.status === 200) {
                    this.setState({ isLoading: false, AboutView: false })

                    if(NavigationConstants.locationSelected=="room"){
                        this.props.navigation.navigate('ListingDetails',{
                            regiser_token:this.state.regiser_token, 
                            user_id:this.state.user_id
                        })
                    }else{
                        this.setState({ InterestView: true,  })
                    }
                }
                else {
                    this.setState({ isLoading: false, isErrorModalOpen: true })
                }

                //this.setState({ InterestView: true, AboutView: false })
            })
            .catch(e => {
                console.log(e)
                this.setState({ isLoading: false, isErrorModalOpen: true })
                this.setState({ InterestView: true, AboutView: false })
            })
    }

    Intresets() {
        this.setState({ isInterestLoading: true })

        const { Smoke, Introvert, Pets, Employed, Organized, Married, Children, Eating, LaudPerson, Handicap, regiser_token, user_id } = this.state;
        console.log(`hobby=${Smoke},${Introvert},${Pets},${Employed},${Organized},${Married},${Children},${Eating},${LaudPerson},${Handicap}&server_key=${url.server_key}&access_token=${regiser_token}&user_update_id=${user_id}&user_id=${user_id}`,'interest')
       
        fetch(url.url2+'/aj/settings/interests', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: `hobby=${Smoke},${Introvert},${Pets},${Employed},${Organized},${Married},${Children},${Eating},${LaudPerson},${Handicap}` + `&server_key=${url.server_key}&access_token=${regiser_token}&user_update_id=${user_id}&user_id=${user_id}`

        })
            .then((res) => res.json())
            .then(data => {
                console.log(data,'interest')
            
                if (data.status === 200) {
                    this.setState({ isInterestLoading: false, NotificationView: false, InterestView: false })
                    if(NavigationConstants.locationSelected=="room"){
                        this.props.navigation.navigate('ListingDetails',{
                            regiser_token:this.state.regiser_token, 
                            user_id:this.state.user_id
                        })
                    }else{
                        this.props.navigation.navigate('MainScreen')
                    }
                }
                else {
                    this.setState({ isInterestLoading: false, isErrorModalOpen: true })
                }

                //this.setState({ NotificationView: true, InterestView: false })
            })
            .catch(e => {
                console.log(e)
                this.setState({ isInterestLoading: false, isErrorModalOpen: true })
            })
    }

    skip() {
        // this.props.navigation.navigate('LookingFor')
        this.setState({ isSkipLoading: true })
        const { regiser_token } = this.state;
        fetch(url.url2+'/aj/startup/skip', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: `server_key=${url.server_key}&access_token=${regiser_token}`
        })
            .then((res) => res.json())
            .then(data => {
                console.log(data)
    
                if (data.status === 200) {
                    this.setState({ isSkipLoading: false, InterestView: true, AboutView: false })
                }
                else {
                    this.setState({ isSkipLoading: false, isErrorModalOpen: true })
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({ isSkipLoading: false, isErrorModalOpen: true })
            })
    }

    render() {
        const { MainContainer, FormLabel, inputStyle, SaveButton, SkipButton, TitleView, TitleTextView, FromView, SkipButtonText } = Styles;
        return (
            <ScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center', padding: 30 }} showsVerticalScrollIndicator={false}>
                {
                    this.state.LocalityView
                        ?
                        <View style={MainContainer}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={color}
                                style={TitleView}>
                                <Text style={[TitleTextView, { marginLeft: 0 }]}>In which Locality ?</Text>
                            </LinearGradient>

                            <Text style={FormLabel}>
                                Enter the street/ area name
                            </Text>

                            {/* <TextInput
                                placeholder="Enter the street/ area name"
                                style={inputStyle}
                            /> */}

                            <GooglePlacesAutocomplete
                                placeholder="Enter your address"
                                minLength={2} // minimum length of text to search
                                autoFocus={true}
                                returnKeyType={'search'} // Can be left out for default return key
                                listViewDisplayed={true} // true/false/undefined
                                fetchDetails={true}
                                // renderDescription={row => console.log(row)} 
                                onPress={(data, details = null) => {
                                    // alert('hello')
                                    console.log(details)
                                    this.setState({
                                        latitude: details.geometry.location.lat,
                                        longitude: details.geometry.location.lng,
                                    });
                                    // props.notifyChange(details.geometry.location);
                                }}
                                filterReverseGeocodingByTypes={[
                                    'locality',
                                    'administrative_area_level_3',
                                    ]}
                                GooglePlacesSearchQuery={{
                                    rankby: 'distance',
                                    // types: 'cities',
                                }}
                                // GooglePlacesDetailsQuery={{
                                //     // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                                //     fields: 'geometry',
                                //   }}
                                query={{
                                    key: 'AIzaSyB8Wm2IOByeafGEqgMNUcyC_8KmhFdgkjY',
                                    language: 'en',
                                }}
                                styles={{
                                    textInputContainer: {
                                        backgroundColor: 'white',
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                        // height:300,
                                        // marginVertical:20
                                    },
                                    textInput: {
                                        // zIndex: 1111111111111,
                                        marginLeft: 5,
                                        marginRight: 0,
                                        height: 38,
                                        color: 'black',
                                        fontSize: 14
                                    },
                                    predefinedPlacesDescription: {
                                        color: 'black',
                                        // zIndex: 111111111,
                                    },
                                    listView: {
                                        // marginTop: 50, // This right here - remove the margin top and click on the first result, that will work.
                                        // elevation: 1,
                                        // height:300,
                                        // backgroundColor: 'white',
                                        // position: 'absolute', // and the absolute position.
                                        zIndex: 111111111111111111,
                                    },
                                }}
                                nearbyPlacesAPI="GooglePlacesSearch"
                                debounce={300}
                            />
                            {/* this.props.navigation.navigate('MainScreen', { data: { long: this.state.longitude, lat: this.state.latitude } }) */}
                            <TouchableOpacity
                                onPress={() => { console.log(this.props); 
                                   
                                    // this.props.navigation.navigate('MainScreen', { data: { let: this.state.latitude, lng: this.state.longitude } }) 
                                    this.setState({ isLoading: false, LocalityView: false, AboutView: true })
                                    }}
                                style={{ height: 40, width: 110, alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: 20, marginBottom: 20, borderWidth: 1, borderColor: 'rgba(237,56,51,1)', borderRadius: 20, alignSelf: 'center' }}>
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'rgba(0,0,0,0.5)' }}>Go ahead !</Text>
                            </TouchableOpacity>
                        </View>

                        :
                        null
                }
                {
                    this.state.AboutView
                        ?
                        <View style={FromView}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={color}
                                style={[TitleView, { flexDirection: 'row' }]}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Econ1 name="user" size={20} color="white" />
                                    <Text style={TitleTextView}>Tell us about you</Text>
                                </View>

                                <TouchableOpacity
                                    onPress={() => this.skip()}
                                    //activeOpacity={1}
                                    style={SkipButton}>
                                    {
                                        this.state.isSkipLoading
                                            ?
                                            <ActivityIndicator size='small' color='black' />
                                            :
                                            <Text style={SkipButtonText}>Skip</Text>
                                    }
                                </TouchableOpacity>

                            </LinearGradient>

                            <Text style={FormLabel}>
                                First name
                            </Text>

                            <TextInput
                                onChangeText={(text) => this.setState({ FirstName: text })}
                                placeholder="First name"
                                style={inputStyle}
                            />

                            <Text style={FormLabel}>
                                Last name
                            </Text>

                            <TextInput
                                onChangeText={(text) => this.setState({ LastName: text })}
                                placeholder="Last name"
                                style={inputStyle}
                            />

                            <Text style={FormLabel}>
                                Gender
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ GenderView: true })}>
                                <TextInput
                                    defaultValue={this.state.genIndex==-1?'Gender':Gender_item[this.state.genIndex].label}
                                    editable={false}
                                    placeholder="Gender"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.GenderView
                                    ?
                                    <View style={{ marginTop: 10, width: '90%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Gender_item}
                                            initial={this.state.genIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Gender_item.findIndex(x => x.value ===value);
                                                this.setState({ Gender: value,genIndex:index }, () => { setTimeout(() => { this.setState({ GenderView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={[FormLabel, { marginTop: height(5) }]}>
                                Date of Birth
                            </Text>

                            <TouchableOpacity onPress={this.showDateTimePicker}>
                                <TextInput
                                    editable={false}
                                    value={`${this.state.Month}/${this.state.date}/${this.state.Year}`}
                                    placeholder="mm/dd/yyyy"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                            />

                            <Text style={FormLabel}>
                                Phone
                            </Text>

                            <TextInput
                                onChangeText={(text) => this.setState({ MobileNo: text })}
                                keyboardType="numeric"
                                placeholder="Phone"
                                style={inputStyle}
                            />

                            <Text style={FormLabel}>
                                Country
                            </Text>

                            <TextInput
                                onChangeText={(text) => this.setState({ Country: text })}
                                placeholder="Country"
                                style={inputStyle}
                            />

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={FormLabel}>
                                    Do you work ?
                                </Text>
                                <CheckBox style={{ marginTop: 20 }} value={this.state.CheckBoxTrue} onValueChange={value => this.setState({ CheckBoxTrue: !this.state.CheckBoxTrue })} />
                            </View>

                            <TextInput
                                onChangeText={(text) => this.setState({ WhatWork: text })}
                                placeholder="What do you do ?"
                                style={inputStyle}
                                editable={this.state.CheckBoxTrue}
                            />

                            <TextInput
                                onChangeText={(text) => this.setState({ WhereWork: text })}
                                placeholder="Where do you do ?"
                                style={[inputStyle, { marginTop: 10 }]}
                                editable={this.state.CheckBoxTrue}
                            />

                            <Text style={FormLabel}>
                                Tell us about yourself
                            </Text>

                            <TextInput
                                onChangeText={(text) => this.setState({ Bio: text })}
                                style={[inputStyle, { height: 60 }]}
                            />


                            <TouchableOpacity onPress={() => this.Start_UpInfo()}>

                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 1 }}
                                    colors={color}
                                    style={SaveButton}>
                                    {
                                        this.state.isLoading
                                            ?
                                            <ActivityIndicator size='small' color='white' />
                                            :
                                            <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>Save & Continue</Text>
                                    }
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                }

                {
                    this.state.InterestView
                        ?
                        <View style={FromView}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={color}
                                style={[TitleView, { flexDirection: 'row' }]}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={TitleTextView}>Your Interests</Text>
                                </View>

                                <TouchableOpacity
                                    // this.setState({ InterestView: false, NotificationView: true })
                                    onPress={() => {
                                        if(NavigationConstants.locationSelected=="room"){
                                        this.props.navigation.navigate('ListingDetails',{
                                            regiser_token:this.state.regiser_token, 
                                            user_id:this.state.user_id
                                        }) 
                                    }else{
                                        this.props.navigation.navigate('Login')
                                    }
                                        
                                    }}
                                    //activeOpacity={1}
                                    style={SkipButton}>
                                    <Text style={SkipButtonText}>Skip</Text>
                                </TouchableOpacity>

                            </LinearGradient>

                            <Text style={FormLabel}>
                                Do you smoke ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ SmokeView: true })}>
                                <TextInput
                                    defaultValue={this.state.smokeIndex==-1?'Choose an option':Smoke_item[this.state.smokeIndex].label}
                                    editable={false}
                                    //placeholder="First name"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>

                            {
                                this.state.SmokeView
                                    ?
                                    <View style={{ marginTop: 10, width: '70%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Smoke_item}
                                            initial={this.state.smokeIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Smoke_item.findIndex(x => x.value ===value);
                                                this.setState({ Smoke: value,smokeIndex:index }, () => { setTimeout(() => { this.setState({ SmokeView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Are you an introvert or an extrovert ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ IntrovertView: true })}>
                                <TextInput
                                    editable={false}
                                    defaultValue={this.state.introIndex==-1?'Choose an option':Introvert_item[this.state.introIndex].label}
                                    //placeholder="Last name"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.IntrovertView
                                    ?
                                    <View style={{ marginTop: 10, width: '70%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Introvert_item}
                                            initial={this.state.introIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Introvert_item.findIndex(x => x.value ===value);
                                                this.setState({ Introvert: value,introIndex:index }, () => { setTimeout(() => { this.setState({ IntrovertView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Do you have pets?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ PetsView: true })}>
                                <TextInput
                                    value={this.state.petIndex==-1?'Choose an option':Pets_item[this.state.petIndex].label}
                                    editable={false}
                                    //placeholder=""
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.PetsView
                                    ?
                                    <View style={{ marginTop: 10, width: '90%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Pets_item}
                                            initial={this.state.petIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Pets_item.findIndex(x => x.value ===value);
                                                this.setState({ Pets: value,petIndex:index }, () => { setTimeout(() => { this.setState({ PetsView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Are you employed ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ EmployedView: true })}>

                                <TextInput
                                    editable={false}
                                    value={this.state.employedIndex==-1?'Choose an option':Employed_item[this.state.employedIndex].label}
                                    //placeholder="dd/mm/yyyy"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>

                            {
                                this.state.EmployedView
                                    ?
                                    <View style={{ marginTop: 10, width: '75%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Employed_item}
                                            initial={this.state.employedIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Employed_item.findIndex(x => x.value ===value);
                                                this.setState({ Employed: value,employedIndex:index }, () => { setTimeout(() => { this.setState({ EmployedView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Would you consider yourself organized, somewhat organized, or not organized at all?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ OrganizedView: true })}>
                                <TextInput
                                    editable={false}
                                    value={this.state.orgIndex==-1?'Choose an option':Organized_item[this.state.orgIndex].label}
                                    //keyboardType="numeric"
                                    //placeholder="Phone"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.OrganizedView
                                    ?
                                    <View style={{ marginTop: 10, width: '75%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            //style={{ alignItems: 'center' }}
                                            radio_props={Organized_item}
                                            initial={this.state.orgIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            // formHorizontal={true}
                                            onPress={(value) => {
                                                let index= Organized_item.findIndex(x => x.value ===value);
                                                 this.setState({ Organized: value,orgIndex:index }, () => { setTimeout(() => { this.setState({ OrganizedView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Are you married or engaged ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ MarriedView: true })}>
                                <TextInput
                                    editable={false}
                                    value={this.state.martialIndex==-1?'Choose an option':Married_item[this.state.martialIndex].label}
                                    //placeholder="Country"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.MarriedView
                                    ?
                                    <View style={{ marginTop: 10, width: '75%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Married_item}
                                            initial={this.state.martialIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                
                                                let index= Married_item.findIndex(x => x.value ===value);
                                                
                                                this.setState({ Married: value,martialIndex:index }, () => { setTimeout(() => { this.setState({ MarriedView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Do you have children ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ ChildrenView: true })}>
                                <TextInput
                                    editable={false}
                                    value={this.state.childIndex==-1?'Choose an option':Children_item[this.state.childIndex].label}
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.ChildrenView
                                    ?
                                    <View style={{ marginTop: 10, width: '80%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Children_item}
                                            initial={this.state.childIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Children_item.findIndex(x => x.value ===value);
                                                this.setState({ Children: value,childIndex:index }, () => { setTimeout(() => { this.setState({ ChildrenView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Do you prefer eating out or eating at home ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ EatingView: true })}>
                                <TextInput
                                    value={this.state.eatIndex==-1?'Choose an option':Eating_item[this.state.eatIndex].label}
                                    editable={false}
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.EatingView
                                    ?
                                    <View style={{ marginTop: 10, width: '80%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            //style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Eating_item}
                                            initial={this.state.eatIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            //formHorizontal={true}
                                            onPress={(value) => { 
                                                let index= Eating_item.findIndex(x => x.value ===value);
                                                
                                                this.setState({ Eating: value,eatIndex:index }, () => { setTimeout(() => { this.setState({ EatingView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }


                            <Text style={FormLabel}>
                                Would you consider yourself a laud person or a quiet person ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ LaudPersonView: true })}>
                                <TextInput
                                    editable={false}
                                    value={this.state.loudIndex==-1?'Choose an option':LouadPerson_item[this.state.loudIndex].label}
                                    //placeholder="dd/mm/yyyy"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.LaudPersonView
                                    ?
                                    <View style={{ marginTop: 10, width: '80%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            //style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={LouadPerson_item}
                                            initial={this.state.loudIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            //formHorizontal={true}
                                            onPress={(value) => {
                                                let index= LouadPerson_item.findIndex(x => x.value ===value);
                                                 this.setState({ LaudPerson: value,loudIndex:index }, () => { setTimeout(() => { this.setState({ LaudPersonView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <Text style={FormLabel}>
                                Do require any special accommodations(handicap, hearing impaired, visually impaired) ?
                            </Text>

                            <TouchableOpacity onPress={() => this.setState({ HandicapView: true })}>
                                <TextInput
                                    editable={false}
                                    value={this.state.handIndex==-1?'Choose an option':Handicap_item[this.state.handIndex].label}
                                    //placeholder="dd/mm/yyyy"
                                    style={inputStyle}
                                />
                            </TouchableOpacity>
                            {
                                this.state.HandicapView
                                    ?
                                    <View style={{ marginTop: 10, width: '80%', backgroundColor: 'white', padding: 10, justifyContent: 'center' }}>
                                        <RadioForm
                                            //style={{ justifyContent: 'space-between', alignItems: 'center' }}
                                            radio_props={Handicap_item}
                                            initial={this.state.handIndex}
                                            selectedLabelColor={'rgba(69,183,221,1)'}
                                            buttonSize={7}
                                            labelStyle={{ fontFamily: 'OpenSans-Regular', fontSize: 11 }}
                                            labelColor={'rgba(237,56,51,1)'}
                                            buttonColor={'rgba(237,56,51,1)'}
                                            selectedButtonColor={'rgba(69,183,221,1)'}
                                            //formHorizontal={true}
                                            onPress={(value) => {
                                                let index= Handicap_item.findIndex(x => x.value ===value);
                                                
                                                 this.setState({ Handicap: value,handIndex:index }, () => { setTimeout(() => { this.setState({ HandicapView: false }) }, 100) }) }}
                                        />
                                    </View>
                                    :
                                    null
                            }

                            <TouchableOpacity onPress={() => this.Intresets()}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 1 }}
                                    colors={color}
                                    style={SaveButton}>
                                    {
                                        this.state.isInterestLoading
                                            ?
                                            <ActivityIndicator size='small' color='white' />
                                            :
                                            <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>Save & Continue</Text>
                                    }
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                }

                {
                    this.state.NotificationView
                        ?
                        <View style={FromView}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={color}
                                style={[TitleView, { flexDirection: 'row' }]}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={TitleTextView}>Notifications</Text>
                                </View>

                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('MainScreen')}
                                    activeOpacity={1}
                                    style={SkipButton}>
                                    <Text style={SkipButtonText}>Skip</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                            <Text style={{ textAlign: 'center', marginVertical: 30, fontSize: 13, fontFamily: 'OpenSans-Regular', color: 'rgba(0,0,0,0.5)' }}>
                                Would you like us to send you notifications related to your account ?
                            </Text>

                            <View style={{ height: 40, alignItems: 'center', flexDirection: 'row', width: '100%', alignSelf: 'center', justifyContent: 'center', marginBottom: 30 }}>
                                <TouchableOpacity onPress={() => {
                                    if(NavigationConstants.locationSelected=="room"){
                                        this.props.navigation.navigate('ListingDetails',{
                                            regiser_token:this.state.regiser_token, 
                                            user_id:this.state.user_id
                                        })
                                    }else{
                                        this.props.navigation.navigate('MainScreen')
                                    }
                                }}>
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 1 }} 
                                        colors={color}
                                        style={{ height: 40, width: 100, alignItems: 'center', justifyContent: 'center', marginTop: 30, marginBottom: 20, borderRadius: 20, alignSelf: 'center' }}>
                                        <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>Yes</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => { if(NavigationConstants.locationSelected=="room"){
                                        this.props.navigation.navigate('ListingDetails',{
                                            regiser_token:this.state.regiser_token, 
                                            user_id:this.state.user_id
                                        }) 
                                    }else{
                                        this.props.navigation.navigate('MainScreen')
                                    }}}>
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 1 }}
                                        colors={['rgba(237,56,51,1)', 'rgba(69,183,221,0.80)']}
                                        style={{ height: 40, width: 100, marginLeft: 15, alignItems: 'center', justifyContent: 'center', marginTop: 30, marginBottom: 20, borderRadius: 20, alignSelf: 'center' }}>
                                        <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'white' }}>Maybe Later</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                        </View>
                        :
                        null
                }


                <Modal isVisible={this.state.isErrorModalOpen}>
                    <View
                        style={{ height: 150, width: 150, alignSelf: 'center', backgroundColor: 'white', padding: 20, alignItems: 'center' }} >
                        <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 13, color: 'black' }}>Oooops!</Text>
                        <View style={{ width: 20, backgroundColor: 'grey', height: 1, marginTop: 3 }}></View>
                        <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'rgba(0,0,0,0.3)', marginTop: 10, textAlign: 'center' }}>Something Went Wrong..!</Text>
                        <TouchableOpacity
                            onPress={() => { this.setState({ isErrorModalOpen: !this.state.isErrorModalOpen }) }}
                            style={{ height: 30, width: 80, backgroundColor: 'red', marginTop: 15, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}
                            activeOpacity={1}>
                            <Text style={{ fontSize: 10, color: 'white' }}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </ScrollView>
        );
    }
}
