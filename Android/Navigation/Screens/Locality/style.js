const Styles = {
    MainContainer: {
        // height: 230,
        width: '100%',
        borderRadius: 8,
        elevation: 2,
        backgroundColor:'white'
    },
    FormLabel:{
        marginLeft: 15, fontFamily: 'Roboto-Bold', color: 'rgba(237,56,51,1)', marginTop: 20, fontSize: 13
    },
    inputStyle:{
        height: 40, width: 250, borderBottomWidth: 1, borderBottomColor: 'rgba(237,56,51,1)', marginLeft: 15, fontSize: 11, fontFamily: 'OpenSans-Regular', color: 'rgba(0,0,0,0.5)'
    },
    SaveButton:{
        height: 40, width: '50%', alignItems: 'center', justifyContent: 'center', marginTop: 30, marginBottom: 20, borderRadius: 20, alignSelf: 'center'
    },
    SkipButton:{
        height: 30, width: 60, backgroundColor: 'white', borderRadius: 15, justifyContent: 'center', alignItems: 'center'
    },
    TitleView:{
        height: 70, width: '100%', padding: 20, alignItems: 'center', justifyContent: 'space-between', borderTopLeftRadius: 8, borderTopRightRadius: 8
    },
    TitleTextView:{
        marginLeft: 10, fontSize: 18, color: 'white', letterSpacing: 1, fontFamily: 'Roboto-Bold'
    },
    FromView:{
        width: '100%', marginTop: 20, elevation: 2, backgroundColor: 'white', borderRadius: 8
    },
    SkipButtonText:{
        fontFamily: 'OpenSans-Regular', fontSize: 11, color: 'rgba(0,0,0,0.5)'
    }
    
}

export default Styles;