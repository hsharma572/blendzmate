const Styles = {
    ProfileView:{
        height:200,
        width:'100%',
        paddingVertical: 20,
        justifyContent:'center',
        alignItems: 'center',
    },
    UserNameText:{
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        color:'white',
        marginTop: 20,
    },
}

export default Styles;