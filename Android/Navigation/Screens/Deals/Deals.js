import React, {Component} from 'react';
import {
  View,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
} from 'react-native';
import Styles from './style';
import Geolocation from 'react-native-geolocation-service';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import url from '../../Component/url';
import { NavigationConstants } from '../../commonValues';

export default class Deals extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      isLoading: true,
      lat: NavigationConstants.lat,
      long: NavigationConstants.long,
      access_Token:NavigationConstants.accessToken ,
      HtmlView: '',
      webKey: 0,
    };
  }

  hasLocationPermission = async () => {
    if (
      Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)
    ) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      alert('Location permission denied by user.');
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      alert('Location permission revoked by user.');
    }
    return false;
  };
  componentWillUnmount() {
    this.focusListener.remove();
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      this.setState({webKey: this.state.webKey + 1});
    });
    console.log(NavigationConstants,'NVS');
    if(NavigationConstants.locationLoaded){
      this.setState({
        lat: NavigationConstants.lat,
        long: NavigationConstants.long,
        HtmlView: this.createHTML(url.url2 + '/deals'),
          isLoading: false,
      });
    }




  }

  // getWebView() {
  //   fetch(`https://blendzmate.com/deals`, {
  //     method: 'POST',
  //     headers: {
  //       "Content-Type": "application/x-www-form-urlencoded",
  //     },
  //     body: `access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app&lat=${this.state.lat}&lng=${this.state.long}`
  //   })
  //     .then((response) => response.text())
  //     .then((res) => {
  //       this.setState({ HtmlView: res, isLoading: false })
  //       console.log('deal injected:', res)
  //     })
  //     .catch((err) => console.log(err))
  // }

  // injectjs() {
  //   let jsCode = `const bodyData = access_token=${this.state.access_Token}&server_key=${url.server_key}&webview_type=app&lat=${this.state.lat}&lng=${this.state.long}
  //   fetch('https://blendzmate.com/deals', {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/x-www-form-urlencoded',
  //     },
  //     body: bodyData,
  //   }).then(response => response.text()).then(valueText => {
  //     alert(JSON.stringify(valueText));
  //   });`;
  //   return jsCode;
  // }

  createHTML = link => {
    console.log(this.state.lat,this.state.long,'<====');
    return `<html><script>function runthis(){const body = 'access_token=${
      this.state.access_Token
    }&server_key=${url.server_key}&webview_type=app';
    post('${link}',{access_token: '${this.state.access_Token}',server_key:'${
      url.server_key
    }',webview_type:'app',lat:'${this.state.lat}',lng:'${this.state.long}'});
}

function post(path, params, method='post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;
  
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];
  
        form.appendChild(hiddenField);
      }
    }
  
    document.body.appendChild(form);
    form.submit();
  }
  setTimeout(function(){ runthis(); }, 400);

</script><body></body></html>`;
  };

  renderLoading = () => (
    <View style={{flex: 1}}>
      <ActivityIndicator animating size="large" color="blue" />
    </View>
  );

  render() {
    return this.state.isLoading ? (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="small" color="blue" animating />
      </View>
    ) : (
      <View style={{flex: 1}}>
        <WebView
          key={this.state.webKey}
          renderLoading={this.renderLoading}
          javaScriptEnabled={true}
          startInLoadingState={true}
          //evaluateJavaScript={this.injectjs()}
          //injectJavaScript={this.Fetch}
          source={{html: this.state.HtmlView}}
          onNavigationStateChange={state => {
            if (
              state.url.includes('blendsmate') ||
              state.url.includes('blendzmate')
            ) {
              let html = this.createHTML(state.url);
              this.setState({
                HtmlView: html,
              });
            }
          }}
        />
      </View>
    );
  }
}
