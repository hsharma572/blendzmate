const Styles = {
    Label:{
        fontSize: 11,
        color:'#333',
        fontFamily: 'OpenSans-Regular',
    },
    TxtInpt:{
        height:35,
        width:'100%',
        fontSize:11,
        fontFamily:'OpenSans-Regular',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
    },
    UploadView:{
        height:120,width:'90%',justifyContent:'center',alignItems:'center',alignSelf:'center',
        borderRadius: 3,borderWidth: 1,borderStyle:'dashed',borderColor: 'rgba(0,0,0,0.2)',marginTop: 20,
    }
}

export default Styles;