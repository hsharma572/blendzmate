import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, Image } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Avatar',
    //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};


export default class VerifationProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: '',
            PhotoID: ''
        };
    }

    UploadPhoto() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    photo: source,
                });
            }
        });
    }

    UploadId() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    PhotoID: source,
                });
            }
        });
    }


    render() {
        const { Label, TxtInpt, UploadView } = Styles;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20 }}>
                <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}> Verification of the profile! </Text>
                <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                    <Text style={Label}>name</Text>
                    <TextInput underlineColorAndroid="transparent" style={TxtInpt} />

                    <Text style={[Label, { marginTop: 15 }]}>Message</Text>
                    <TextInput underlineColorAndroid="transparent" style={[TxtInpt, { height: 60 }]} />

                    <Text style={[Label, { marginTop: 15 }]}>Upload documents</Text>
                    <Text style={[Label, { marginTop: 15, fontSize: 9 }]}>Please upload a photo with your passport / ID & your distinct photo</Text>

                    <TouchableOpacity
                        onPress={() => this.UploadPhoto()}
                        style={UploadView}>
                        {
                            this.state.photo
                                ?
                                <Image source={this.state.photo} style={{ height: '100%', width: '100%' }} />
                                :
                                <View style={{ alignSelf: "center", justifyContent: 'center', alignItems: 'center' }}>
                                    <Econ name="image" color="grey" size={20} />
                                    <Text style={{ fontSize: 9, fontFamily: 'OpenSans-Regular', color: 'grey', marginTop: 5 }}>Your Photo</Text>
                                </View>
                        }
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.UploadId()}
                        style={UploadView}>
                        {
                            this.state.PhotoID
                                ?
                                <Image source={this.state.PhotoID} style={{ height: '100%', width: '100%' }} />
                                :
                                <View style={{ alignSelf: "center", justifyContent: 'center', alignItems: 'center' }}>
                                    <Econ name="image" color="grey" size={20} />
                                    <Text style={{ fontSize: 9, fontFamily: 'OpenSans-Regular', color: 'grey', marginTop: 5 }}>Passport / id card</Text>
                                </View>
                        }
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, borderWidth: 1, borderColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Send</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
