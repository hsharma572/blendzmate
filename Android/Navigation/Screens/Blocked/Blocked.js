import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, FlatList, ActivityIndicator } from 'react-native';
import Styles from './style';
import Econ from 'react-native-vector-icons/FontAwesome';
import BlockedList from '../../Component/BlockedList';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '../../Component/Contants';
import url from '../../Component/url';

export default class Blocked extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            access_Token: '',
            user_id: '',
            BlockUserData: ''
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true })
        AsyncStorage.multiGet([Constants.USER_TOKEN, Constants.USER_ID], (error, result) => {
            if (error === null) {
                console.log('result', result[0][1]);
                this.setState({ access_Token: result[0][1].replace(/"/g, '').trim(), user_id: result[1][1] }, () => this.get_Blocked_Users());
            }
            else {
                alert('Something Went wrong...!!')
            }
        })
    }

    get_Blocked_Users() {
        const { access_Token, user_id } = this.state
        fetch(`${url.url}/v1/user/fetch_blocked_users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            //timeout: 2000,
            body: `access_token=${access_Token}&server_key=${url.server_key}&user_id=${user_id}`
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('block user response:', responseJson);
                if (responseJson.code === '200') {
                    this.setState({ BlockUserData: responseJson.data, isLoading: false })
                }
                else {
                    this.setState({ isLoading: false })
                    alert('something went wrong..!!')
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false })
                alert('catch alert')
            })
    }

    render() {
        const { Label, TxtInpt } = Styles;
        return (
            this.state.isLoading
                ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='small' color='blue' />
                </View>
                :
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ backgroundColor: '#fafafa', paddingHorizontal: 30, paddingVertical: 20 }}>
                    <Text style={{ fontSize: 18, fontFamily: 'OpenSans-Bold', color: "#333" }}>Blocked users </Text>
                    <Text style={{ fontSize: 9, fontFamily: 'OpenSans-Regular', color: "#333" }}>Manage users that you have blocked</Text>
                    <View style={{ height: 1, width: '95%', alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.1)', marginTop: 15 }}></View>

                    <View style={{ borderRadius: 5, backgroundColor: '#f1f1f1', width: '100%', padding: 15, marginTop: 15 }}>
                        {
                            this.state.BlockUserData.length === 0
                                ?
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', textAlign: 'center', color: 'rgba(0,0,0,0.4)' }}>No blocked users found</Text>
                                :
                                <FlatList
                                    ItemSeparatorComponent={() => {
                                        return <View style={{ height: 1, width: '100%', backgroundColor: 'rgba(0,0,0,0.2)', marginVertical: 7 }}></View>
                                    }}
                                    data={this.state.BlockUserData}
                                    renderItem={(data) => {
                                        return <BlockedList data={data} key={data} />
                                    }}
                                />
                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '80%', alignSelf: 'center' }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, borderWidth: 1, borderColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 11, color: '#333', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Back</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ height: 35, marginTop: 30, width: '48%', borderRadius: 5, backgroundColor: '#333', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 11, color: 'white', fontFamily: 'OpenSans-Regular', marginBottom: 3 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
        );
    }
}
