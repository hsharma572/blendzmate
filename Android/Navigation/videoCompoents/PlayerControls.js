import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Feather from 'react-native-vector-icons/Feather'

const iconSize=20;
export default function PlayerControls(props){
    const { playing,
        showPreviousAndNext,
        showSkip,
        previousDisabled,
        nextDisabled,
        onPlay,
        onPause,
        skipForwards,
        skipBackwards,
        onNext,
        onPrevious}=props;
    return(<View style={styles.wrapper}>
        {showPreviousAndNext && (
          <TouchableOpacity
            style={[styles.touchable, previousDisabled && styles.touchableDisabled]}
            onPress={onPrevious}
            disabled={previousDisabled}>
            <Feather name="skip-back" size={iconSize} color="white" />
          </TouchableOpacity>
        )}
    
        {showSkip && (
          <TouchableOpacity style={styles.touchable} onPress={skipBackwards}>
           
            <MaterialIcons name="replay-10" size={iconSize} color="white" />
          </TouchableOpacity>
        )}
    
        <TouchableOpacity
          style={styles.touchable}
          onPress={playing ? onPause : onPlay}>
          {!playing ? <MaterialIcons name="play-arrow" size={iconSize} color="white" /> : <MaterialIcons name="pause" size={iconSize} color="white" />}
        </TouchableOpacity>
    
        {showSkip && (
          <TouchableOpacity style={styles.touchable} onPress={skipForwards}>
    
            <MaterialIcons name="forward-10" size={iconSize} color="white" />
          </TouchableOpacity>
        )}
    
        {showPreviousAndNext && (
          <TouchableOpacity
            style={[styles.touchable, nextDisabled && styles.touchableDisabled]}
            onPress={onNext}
            disabled={nextDisabled}>
            <Feather name="skip-forward" size={iconSize} color="white" />
          </TouchableOpacity>
        )}
      </View>);
}

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flex: 3,
  },
  touchable: {
    padding: 5,
  },
  touchableDisabled: {
    opacity: 0.3,
  },
});