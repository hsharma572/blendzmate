import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Colors from '../../assets/Colors/Colors';

export default class PropertyFeatures extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        //alert('hello')
        return (
          
            <TouchableOpacity
                onPress={() => this.props.PropertyTypeColor(this.props.item)}
                style={{
                    height: 25,
                    marginBottom:5,
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                    borderWidth: 1,
                    borderRadius: 15,
                    marginRight: 10,
                    borderColor:this.props.item.isSelected?Colors.ActiveTintColor:'rgba(0,0,0,0.1)'
                }}>
                <Text
                    style={{
                        fontSize: 11,
                        color: this.props.item.isSelected?Colors.ActiveTintColor:'rgba(0,0,0,0.5)',
                        fontFamily: 'Roboto-Bold'
                    }}>
                    {this.props.item.features}
                </Text>
            </TouchableOpacity>
          
        );
    }
}
