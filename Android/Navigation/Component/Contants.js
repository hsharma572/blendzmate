export const Constants = {
    USER_LOGIN_STATUS: "LOGIN_STATUS",
    USER_ID: "USER_ID",
    USER_TOKEN: "USER_TOKEN",
    USER_NAME:"USER_NAME"
};