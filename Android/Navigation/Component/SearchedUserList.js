import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native';


export default class SearchedUserList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation(this.props.data.item.username)}
                style={{
                    height: 80,
                    backgroundColor: 'white',
                    padding: 10, borderRadius: 5,
                    width: '100%', marginBottom: 10,
                    flexDirection: 'row', alignItems: 'center'
                }}>
                <ImageBackground
                    source={{ uri: this.props.data.item.avatar }}
                    style={{ height: 50, width: 50 }}
                    imageStyle={{ borderRadius: 25 }} />
                <View style={{ height: 50, width: '100%', marginLeft: 10, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 14, fontFamily: 'OpenSans-SemiBold', color: 'black' }}>{this.props.data.item.username}</Text>
                    <Text style={{
                        fontSize: 11,
                        marginTop: 5,
                        fontFamily: 'OpenSans-Regular',
                        color: 'black'
                    }}>{this.props.data.item.name}</Text>
                </View>

            </TouchableOpacity>
        );
    }
}
