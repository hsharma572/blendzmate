import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, FlatList, ImageBackground, TextInput, ActivityIndicator } from 'react-native';
import Styles from './style';
import Colors from '../../../assets/Colors/Colors';
import Econ from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from 'react-native-linear-gradient';
import url from '../../Component/url'

export default class SuggestionView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFollowSpinner: false 
        };
    }

    
  FollowUser(id,type,access_Token) {
    this.setState({ isFollowSpinner: true })
    console.log(`access_token=${access_Token}&server_key=${url.server_key}&follow_id=${id}`,'FollowUser');
    fetch(`${url.url}/v1/user/follow`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      //timeout: 2000,
      body: `access_token=${access_Token}&server_key=${url.server_key}&follow_id=${id}`
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('FollowUser', responseJson);
        if (responseJson.code === '200') {
          this.setState({ isFollowSpinner: false,openModalForReport:false,  }, () => {
            this.props.reloadData(type);
           
          })
          //alert('you follow...!')
        }
        else {
          this.setState({ isFollowSpinner: false, isModalVisible: true })
          // alert('something went wrong..!!')
        }
      })
      .catch((error) => {
        this.setState({ isFollowSpinner: false, isModalVisible: true })
        console.log(error,'FollowUser');
      })
  }

    render() {

        const { cardView, linearGradient } = Styles;

        return (
            <View style={cardView}>
                <Image source={{ uri: this.props.data.item.avatar }} style={{ height: 60, width: 60 }} />
                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, marginTop: 5 }}>{this.props.data.item.username}</Text>
                <Text style={{ fontFamily: 'OpenSans-Regular', fontSize: 11, color: Colors.InactiveTintColor }}>{this.props.data.item.name}</Text>
                <TouchableOpacity onPress={() => this.FollowUser(this.props.data.item.user_id,'suggest',this.props.access_Token)}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#3b5998', '#192f6a']} style={linearGradient}>
                        {
                            this.state.isFollowSpinner
                                ?
                                <ActivityIndicator size="small" color='white' />
                                :
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', color: Colors.white, marginBottom: 3 }}>
                                    Follow
                                </Text>
                        }
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}
