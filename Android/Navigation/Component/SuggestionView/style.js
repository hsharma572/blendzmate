import Colors from '../../../assets/Colors/Colors';

const Styles = {
    cardView:{
        height:185,
        width:150,
        marginVertical: 15,
        backgroundColor:'white',
        borderRadius:10,
        elevation:1,
        alignItems: 'center',
        justifyContent:'center',
        marginRight: 15,
    },
    linearGradient:{
        height:30,
        width:70,
        borderRadius:15,
        justifyContent:'center',
        alignItems: 'center',
        marginTop:10,
        alignSelf: 'center',
    },
}

export default Styles;