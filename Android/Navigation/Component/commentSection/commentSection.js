import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Econ1 from 'react-native-vector-icons/FontAwesome';
import { width, height, totalSize } from 'react-native-dimension';
import url from '../../Component/url';
import Entypo from 'react-native-vector-icons/Entypo';
import Toast from 'react-native-simple-toast';
export default class CommentSection extends Component {
    constructor(props) {
        super(props);
        // console.log('comment props:', this.props.data.item);
        this.state = {
            liked: this.props.data.is_liked?true:false,loading:false,
        };
    }

    Liked() {
        this.setState({ liked: true,loading:true })
        console.log(`access_token=${this.props.access_Token}&server_key=${url.server_key}&comment_id=${this.props.data.id}`,'like_dislike');
        fetch(`${url.url2}/aj/comments/like_dislike`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            //timeout: 2000,
            body: `access_token=${this.props.access_Token}&server_key=${url.server_key}&comment_id=${this.props.data.id}`
          })
            .then((response) => response.json())
            .then((responseJson) => {
              console.log('like_dislike', responseJson);
              if (responseJson.status == '200') {
                  if(responseJson.likes){
                    this.setState({ liked: true,loading:false })
                  }else{
                    this.setState({ liked: false,loading:false })
                  }
                
              }
              else {
                this.setState({ loading: false })
                // alert('something went wrong..!!')
                Toast.show('something went wrong..!!', Toast.SHORT);
      
              }
            })
            .catch((error) => {
              // alert('catch alert')
              console.log('like_dislike',error);
              this.setState({ loading: false })
              Toast.show('something went wrong..!!', Toast.SHORT);

      
            })
    }

    render() {
        return (
            <View>
                <View style={{ backgroundColor: 'white', width: '100%', paddingLeft: 25, paddingTop: 15, justifyContent: 'space-between', flexDirection: 'row', paddingRight: 20, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', width: 260, backgroundColor: 'white' }}>
                        <Econ1 name="user" size={20} color="grey" style={{ marginTop: 5 }} />
                        <View style={{ flexDirection: 'column', backgroundColor: 'white', maxWidth: 260 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View
                                    style={{ backgroundColor: 'rgba(0,0,0,0.2)', borderRadius: 20, marginLeft: 20, alignItems: 'center', flexDirection: 'row', paddingLeft: 15, paddingRight: 20, paddingVertical: 5 }}>
                                    <View style={{ flexDirection: 'row', paddingRight: 35, maxWidth: 190 }}>
                                        <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 11 }}>{this.props.data.username}</Text>
                                        <Text style={{ marginLeft: 10, fontFamily: 'OpenSans-Regular', fontSize: 11, marginBottom: 3 }}>{this.props.data.text}</Text>
                                    </View>
                                </View>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => {
                                        if(!this.state.loading){
                                            this.Liked()
                                        }
                                        }}
                                    style={{ height: height(3), width: height(3), backgroundColor: 'white', elevation: 1, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', borderRadius: 15, position: 'absolute', right: 0, bottom: -10 }}>

                                    {
                                        this.state.liked
                                            ?
                                            <Econ1 name="heart" size={totalSize(1.3)} color='red' />
                                            :
                                            <Econ name="heart" size={totalSize(1.3)} color="grey" />
                                    }
                                </TouchableOpacity>
                            </View>

                            <View style={{ backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', marginLeft: 20, marginTop: 12 }}>
                                <Text>{this.props.data.likes}</Text>
                                <Econ1 name="heart" size={13} color="grey" style={{ marginLeft: 3, marginRight: 5 }} />
                                <Econ1 name="circle" size={3} color="grey" style={{ marginLeft: 3 }} />
                                <Text style={{ marginLeft: 10 }}>{this.props.data.replies}</Text>
                                <Econ1 name="comment" size={13} color="grey" style={{ marginLeft: 3 }} />
                                <Econ1 name="circle" size={3} color="grey" style={{ marginLeft: 3 }} />
                                <Text style={{ marginLeft: 10 ,fontSize:9}}>{this.props.data.time_text}</Text>
                            </View>
                        </View>

                    </View>
                    {!this.props.isDeletHide?(<TouchableOpacity onPress={()=>this.props.Delete(this.props.data.id)}>
                    <Econ name="times" size={15} color="grey" />
                    </TouchableOpacity>):null}
                    
                </View>

            </View>
        );
    }
}
