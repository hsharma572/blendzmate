import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

export default class ImageGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{height:55,width:'20%',flexDirection:'row',paddingVertical:'1%',paddingHorizontal:'0.5%'}}>
        <Image source={{uri:this.props.data.path}} style={{height:'100%',width:'100%'}}/>
      </View>
    );
  }
}
