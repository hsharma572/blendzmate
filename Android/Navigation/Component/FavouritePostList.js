import React, { Component } from 'react';
import { View, Text, ImageBackground } from 'react-native';

export default class FavouritePostList extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.data.item.media_set.map((d,i)=>d));
    this.state = {
    };
  }

  render() {
    return (
      <View style={{height:150,width:150,backgroundColor:'white',marginRight:20,marginBottom:20}}>
        <ImageBackground source={{uri:this.props.data.item.media_set.map((d,i)=>d.file)[0].toString()}} style={{height:'100%',width:'100%'}}/>
      </View>
    );
  }
}
