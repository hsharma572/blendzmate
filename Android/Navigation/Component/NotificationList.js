import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import TimeAgo from 'react-native-timeago';

export default class NotificationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ padding: 10, flexDirection: 'row', alignItems: 'center', borderBottomWidth:1, borderColor:'rgba(0,0,0,0.1)' , backgroundColor:'white'}}>
        <Image source={{ uri: this.props.data.item.avatar }} style={{ height: 40, width: 40, borderRadius: 20 }} />
        <View style={{ marginLeft: 10, height: 35, justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', width:270,  }}>
            <Text style={{ fontWeight: 'bold', fontFamily: 'OpenSans-Regular' }}>{this.props.data.item.username} </Text>
            <Text ellipsizeMode='tail' numberOfLines={1} style={{ fontFamily: 'OpenSans-Regular' }}>{this.props.data.item.text}</Text>
          </View>
          <Text style={{ fontSize: 11, color:'rgba(0,0,0,0.4)' }}>{this.props.data.item.user_data.time_text}</Text>
        </View>
      </View>
    );
  }
}
