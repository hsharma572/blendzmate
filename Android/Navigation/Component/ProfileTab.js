import React from 'react';
import {Text} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import Posts from '../Screens/Posts/Posts';
import Followers from '../Screens/Followers/Followers';
import Following from '../Screens/Following/Following';
import Favourites from '../Screens/Favourites/Favourites';
import Colors from '../../assets/Colors/Colors';

const ProfileTab = createMaterialTopTabNavigator({
    Posts: {
        screen: Posts,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Text style={{textAlign:'center'}}>0</Text>
            ),
        }),
    },
    Followers: {
        screen: Followers,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Text style={{textAlign:'center'}}>0</Text>
            ),
        }),
    },
    Following: {
        screen: Following,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Text style={{textAlign:'center'}}>0</Text>
            ),
        }),
    },
    Favourites: {
        screen: Favourites,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Text style={{textAlign:'center'}}>0</Text>
            ),
        }),
    },
},
    {
        swipeEnabled: true,
        animationEnabled: true,
        tabBarOptions: {
            showIcon:true,
            showLabel:true,
            upperCaseLabel: false,
            activeTintColor: 'blue',
            inactiveTintColor: 'grey',
            tabStyle: {
                width: 75,
                borderBottomWidth: 0,
            },
            style: {
                backgroundColor: Colors.PrimaryBackGroundColor,
                width: '100%',
                height: 70,
                elevation: 0,
                alignSelf: 'center',
                paddingHorizontal: 10,

            },
            labelStyle: {
                fontSize: 9,
                fontFamily: 'Roboto-Bold',
                //textAlign: 'center',
            },
            indicatorStyle: {
                backgroundColor: 'transparent',
                //height: 2,
                alignSelf: 'center',
                marginHorizontal: 20,
                //width: 45,
            },
            'scrollEnabled': true,
            'lazy': false,
        }
    }
)

export default createAppContainer(ProfileTab);