import React from 'react';
import {Image,Text} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Deals from '../../Navigation/Screens/Deals/Deals';
import Explore from '../../Navigation/Screens/Explore/Explore';
import Home from '../../Navigation/Screens/Home/Home';
import MessageStack from '../../Navigation/Screens/Messages/Messages';
import ProfileStack from '../../Navigation/Screens/Profile/Profile';
import NotificationStack from '../../Navigation/Screens/NotificationStack/NotificationStack';
import PropertiesStack from '../../Navigation/Screens/Properties/Properties';
import Colors from '../../assets/Colors/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
import Econ from 'react-native-vector-icons/FontAwesome5';
import IconBadge from 'react-native-icon-badge';
import { width, height, totalSize } from 'react-native-dimension';

console.log('tabbbbbbbbbb')
// global.initialRouteName = "Properties"
const TopTabBar = createMaterialTopTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({screenProps}) => ({
            tabBarIcon: ({ tintColor }) => {
               
                return(
                    <Icon name="md-home" size={16} color={tintColor}/>
              
            )}
        }),
    },
    Explore: {
        screen: Explore,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Econ name="compass" size={14} color={tintColor}/>
            ),
        }),
    }, 
    Deals: {
        screen: Deals,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Image source={require('../../assets/Images/store.png')} style={{height:14,width:14}}/>
            ),
        }),
    },
    Properties: {
        screen: PropertiesStack,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Image source={require('../../assets/Images/properties.png')} style={{height:14,width:14}}/>
            ),
        }),
    },
    Messages: {
        screen: MessageStack,
        navigationOptions: ({screenProps}) => ({
            tabBarIcon: ({ tintColor }) => (
               
                
                <IconBadge
                MainElement={
                    <Econ name="comment-alt" size={12} color={tintColor}/>
                }
                BadgeElement={
                  <Text style={{color:'#FFFFFF',fontSize:8}}>{screenProps.new_messages}</Text>
                }
                IconBadgeStyle={
                  {width:7,
                  height:12,
                  top:-7,
                  borderRadius:10,
                  backgroundColor: '#f72d2d'}
                }
            
                Hidden={screenProps.new_messages==0}
                />
            ),
        }),
    },
    Profile: {
        screen: ProfileStack,
        navigationOptions: () => ({
            tabBarIcon: ({ tintColor }) => (
                <Econ name="user-alt" size={12} color={tintColor}/>
            ),
        }),
    },
    // Notification: {
    //     screen: NotificationStack,
    //     navigationOptions: ({screenProps}) => ({
    //         tabBarIcon: ({ tintColor }) => (
                
    //                     <IconBadge
    // MainElement={
    //     <Icon name="ios-notifications" size={16} color={tintColor}/>
    // }
    // BadgeElement={
    //   <Text style={{color:'#FFFFFF',fontSize:8}}>{screenProps.notif}</Text>
    // }
    // IconBadgeStyle={
    //   {width:7,
    //   height:12,
    //   top:-5,
    //   borderRadius:10,
    //   backgroundColor: '#f72d2d'}
    // }

    // Hidden={screenProps.notif==0}
    // />
    //         ),
    //     }),
    // },
},
    {   
        // initialRouteName:global.initialRouteName,
        swipeEnabled: false,
        animationEnabled: true,
        navigationOptions:({navigation})=>{console.log(navigation,'intb');
        },
        tabBarOptions: {
            showIcon:true,
            showLabel:false,
            upperCaseLabel: false,
            activeTintColor: Colors.ActiveTintColor,
            inactiveTintColor: Colors.InactiveTintColor,
            tabStyle: {
                // marginLeft:-30,
                width: width(16.5),
                //bottomWidth:0,
                alignItems:'center',
                borderBottomWidth: 0,
                //paddingHorizontal: 30,
            },
            style: {
                backgroundColor: Colors.white,
                width: '100%',
                height: 40,
                elevation: 0,
                alignSelf: 'center',
                

            },
            labelStyle: {
                //paddingHorizontal:-10,
                fontFamily: 'Roboto-Bold',
                //fontWeight:'bold',
                textAlign: 'center',
            },
            indicatorStyle: {
                backgroundColor: Colors.ActiveTintColor,
                height: 2,
                alignSelf: 'center',
                //marginLeft: 5,
                width: width(15),
                //borderRadius: 8
            },
            'lazy': true,
        }
    }
)

export default createAppContainer(TopTabBar);