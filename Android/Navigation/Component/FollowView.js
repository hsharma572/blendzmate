import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

export default class Follow extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }



    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                activeOpacity={1}
                style={{
                    height: '100%',
                    width: 77,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: this.props.selected ? 5 : 0,
                    backgroundColor: this.props.selected ? 'white' : 'transparent'
                }}>
                <Text style={{ fontSize: 15, fontFamily: 'OpenSans-Bold', color: this.props.selected ? 'blue' : '#333' }}> 4 </Text>
                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Regular', marginTop: 5, color: 'rgba(0,0,0,0.4)' }}>{this.props.data.key}</Text>
            </TouchableOpacity>
        );
    }
}
