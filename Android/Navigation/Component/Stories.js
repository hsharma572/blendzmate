import React, { Component } from 'react';
import { View, Text, ImageBackground , TouchableOpacity} from 'react-native';
import Colors from '../../assets/Colors/Colors';
import LinearGradient from 'react-native-linear-gradient';
import Econ from 'react-native-vector-icons/FontAwesome5';

export default class Stories extends Component {
    constructor(props) {
        super(props);
        console.log('item', this.props.data.item);
        this.state = {
        };
    }

    render() {
        return (
            <View>
                {
                    this.props.data.item.key === 'a'
                        ?
                        <View style={{ height: 100, width: 75, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ height: 55, width: 55, borderRadius: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: 'grey' }}>
                                <ImageBackground source={require('../../assets/Images/placeholder.jpg')}
                                    style={{
                                        height: 50, width: 50,
                                        bordreWidth: 1, borderColor: Colors.white,
                                        alignItems: 'flex-end',
                                        justifyContent: 'flex-end'
                                    }}
                                    imageStyle={{ borderRadius: 25 }}>
                                    <View
                                        style={{
                                            height: 18,
                                            width: 18,
                                            borderRadius: 9,
                                            backgroundColor: 'white',
                                            position: 'absolute', bottom: -3, right: -3,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                        <View style={{
                                            height: 16,
                                            width: 16,
                                            borderRadius: 8,
                                            justifyContent: 'center', alignItems: 'center', backgroundColor: 'blue'
                                        }}>
                                            <Econ name="plus" size={10} color="white" />
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                            <Text style={{ fontSize: 11, marginTop: 5, fontFamily: 'Roboto-Regular' }}>Bhautik</Text>
                        </View>
                        :
                        <TouchableOpacity 
                        onPress={()=>this.props.OpenStories()}
                        style={{ height: 100, backgroundColor: 'white', width: 75, alignItems: 'center', justifyContent: 'center' }}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#c471ed', '#f64f59']}
                                style={{ height: 56, width: 56, borderRadius: 33, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ height: 52, width: 52, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.white, borderRadius: 35 }}>
                                    <ImageBackground source={{uri:this.props.data.item.avatar}}
                                        style={{ height: 50, width: 50, bordreWidth: 1, borderColor: Colors.white }}
                                        imageStyle={{ borderRadius: 25, }} />
                                </View>
                            </LinearGradient>

                            <Text style={{ fontSize: 11, marginTop: 5, fontFamily: 'Roboto-Regular' }}>{this.props.data.item.username}</Text>
                        </TouchableOpacity>
                }
            </View>
        );
    }
}
