import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, Animated, ImageBackground, TextInput, ActivityIndicator, FlatList } from 'react-native';
import Econ from 'react-native-vector-icons/FontAwesome5';
import Econ1 from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Styles from './style';
import Swiper from 'react-native-swiper';
import VideoPlayer from 'react-native-video-player';
import { width, height, totalSize } from 'react-native-dimension';
import CommentSection from '../../Component/commentSection/commentSection';
import YouTube from 'react-native-youtube';
import { WebView } from 'react-native-webview';
import {NavigationConstants} from '../../commonValues'

import url from '../../Component/url';

export default class Feed extends Component {
    constructor(props) {
        super(props);
       console.log(this.props.data.item,'feedPost');
        let len=Object.keys(this.props.data.item.comments).length;
        let comm=[];
          for(let i=0;i<len;i++){
            comm[i]=this.props.data.item.comments[Object.keys(this.props.data.item.comments)[i]];
          }
         
        this.state = {
            CommentSectionVisible: false,
            CommentText: '',
            visible: true,
            showOptions:false,
            commentData:comm,
            isBuffering:true,
            currentTime:0.0,
            lastProgress:0.0,
            likeOrnot: this.props.data.item.is_liked,
        };
    }

    getSinglePost=(postID)=> {
        console.log('functoin log:');
        const  access_Token  = this.props.access_Token;
        console.log( `access_token=${access_Token}&server_key=${url.server_key}&post_id=${postID}`,'getSinglePost');
     fetch(`${url.url}/v1/post/fetch_post_by_id`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          //timeout: 2000,
          body: `access_token=${access_Token}&server_key=${url.server_key}&post_id=${postID}`
        })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log('single post response:', responseJson);
            if (responseJson.code === '200') {
              let len=Object.keys(responseJson.data.comments).length;
              let comm=[];
              let lstcmt=0;
              if(len>5){
                len=5;
              }
                for(let i=0;i<len;i++){
                  comm[i]=responseJson.data.comments[Object.keys(responseJson.data.comments)[i]];
                  
                    lstcmt=comm[i].id;
                  
                }
               
                console.log('single post response:', comm);
              this.setState({ commentData:comm ,lastcomment:lstcmt });
            }
            else {
              this.setState({  });
              alert('something went wrong..!!');
            }
          })
          .catch((error) => {
            this.setState({ });
           console.log(error,'getSinglePost');
          });
      }

    hideSpinner = () => {
        this.setState({ visible: false });
    };
    showSpinner = () => {
        this.setState({ visible: true });
    };

    // ActivityIndicatorLoadingView() {
    //     //making a view to show to while loading the webpage
    //     return (
    //       <ActivityIndicator
    //          color="#009688"
    //          size="large"
    //          //style={}
    //       />
    //     );
    //  }
    onProgress(data){
        this.setState({lastProgress: this.state.currentTime});
        this.setState({currentTime: data.currentTime});
        if(this.state.lastProgress == data.currentTime){
        this.setState({isBuffering: true});
        }else{
        this.setState({isBuffering: false});
        }
        }

    open_close_side_menu = () =>{
        this.setState({ showOptions: !this.state.showOptions });
    }
   

    render() {
        const { instagramView, feedHeader, header, headerText, timeHeader, commentView, commentInput, LikeText, LikeView } = Styles;

        return (
            <View style={instagramView}>
            <TouchableOpacity onPress={() => PubSub.publish('_goToCommentScreen', { data: this.props.data.item.post_id })}>
                <View style={feedHeader}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                       
                        {
                            this.props.data.item.user_data&&this.props.data.item.user_data.avatar?(
                                <Image
        style={{height:27,width:27,borderRadius:20}}
        source={{
          uri: this.props.data.item.user_data.avatar,
        }}
      />
                            ):(<Econ1 name="user" size={25} color="grey" />)
                        }
                        <View>
                            <Text style={headerText}>{this.props.data.item.username}</Text>
                            <Text style={timeHeader}>{this.props.data.item.time_text}</Text>
                        </View>
                       
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        height: 30,
                        
                    }}>
                        <TouchableOpacity
                            style={{ height: 20, marginLeft: 15, width: 20, justifyContent: 'center', alignItems: 'center',paddingVertical:5  }}
                            onPress={() => this.open_close_side_menu()}
                            style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 20 }}>
                            <Entypo name="dots-three-horizontal" size={16} color="grey" />
                        </TouchableOpacity>
                    </View>
                </View>

                

                <View style={{ height: 180 }}>
                    {
                        //console.log('type:', this.props.data.item.youtube),
                        this.props.data.item.type === "youtube"
                            ?
                            <View style={{ height: '100%', width: '100%' }}>
                                <WebView
                                    onLoadStart={() => this.showSpinner()}
                                    onLoad={() => this.hideSpinner()}
                                    originWhitelist={['*']}
                                    source={{ html: `<iframe width="100%" height="100%" src=${this.props.data.item.link.replace('watch?v=', 'embed/')} frameborder="0" allow="autoplay encrypted-media" allowfullscreen></iframe>` }}
                                />
                                {
                                    this.state.visible
                                        ?
                                        <ActivityIndicator
                                            color='blue'
                                            style={{
                                                left: '50%',
                                                top: 90,
                                                position: 'absolute',
                                                alignSelf: 'center'
                                            }}
                                            size="small"
                                        />
                                        :
                                        null
                                }
                            </View>
                            :
                            <Swiper style={{ height: '100%', width: '100%' }}
                                loop={false}
                                activeDotColor='blue'
                                paginationStyle={{ bottom: -25 }}
                                activeDotStyle={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'red' }}
                                dotStyle={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'rgba(0,0,0,0.4)' }}>
                                {
                                    this.props.data.item.media_set.map((d, i) => {
                                        let str = d.file.toString();
                                        let extension = str.substring(str.lastIndexOf('.') + 1, str.length)
                                        //console.log('ext:', extension)
                                        if (extension === 'jpg' || extension === 'gif' || extension === 'png') {
                                            return <TouchableOpacity onLongPress={()=>this.props.openImage(d.file.toString())}><Image source={{ uri: d.file.toString() }} key={this.props.data.item.post_id}
                                                style={{ height: '100%', width: '100%' }}>
                                            </Image>
                                            </TouchableOpacity>
                                        }
                                        else {
                                            return <VideoPlayer
                                               disableFullscreen={false}
                                                video={{ uri: d.file.toString() }}
                                                videoWidth={width(80)}
                                                videoHeight={150}
                                                
                                            />
                                        }
                                    })
                                }
                            </Swiper>
                    }
                </View>
                </TouchableOpacity>

                <View style={commentView}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={[LikeView, { flexDirection:'row' }]}>
                            {/* (this.props.likedId === this.props.data.item.post_id && this.props.likeStatus) */}
                            <Text style={LikeText}>
                                {this.props.data.item.likes}
                                </Text>
                            {
                               this.state.likeOrnot
                                    ?
                                    <TouchableOpacity onPress={()=>{this.props.likePost();this.setState({likeOrnot:false});}}>
                                        <Econ1 name="heart" size={18} color="red" />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={()=>{this.props.likePost();this.setState({likeOrnot:true});}}>
                                        <Econ name="heart" size={18} color="grey" />
                                    </TouchableOpacity>
                            }
                            
                        </View>
                        <View style={[LikeView, { marginLeft: 10,flexDirection:'row' }]}>
                        <Text style={LikeText}>
                                {this.props.data.item.votes}
                            </Text>
                            <TouchableOpacity
                                onPress={() => PubSub.publish('_goToCommentScreen', { data: this.props.data.item.post_id })}
                                style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Econ name="comment" size={18} color="grey" />
                            </TouchableOpacity>
                           
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={() => this.props.Favourite(this.props.data.item.post_id)}
                        style={{ height: 70, width: 20, marginTop: 15 }}>
                        {
                            this.props.data.item.is_saved
                                ?
                                <Econ1 name="bookmark" size={18} color="#FF9800" />
                                :
                                <Econ name="bookmark" size={18} color="grey" />
                        }
                    </TouchableOpacity>
                </View>

                {
                    this.props.data.item.description
                        ?
                        <Text style={{ marginLeft: 15, marginBottom: 5, fontFamily: 'OpenSans-Regular', fontSize: 11, color: 'grey' }}>{this.props.data.item.description}</Text>
                        :
                        null
                }

                <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.1)' }}></View>

<View style={[feedHeader, { height: 60 }]}>
    <View style={header}>
        <Econ1 name="user" size={24} color="grey" />
        <TextInput
            defaultValue={this.props.isCommentClear ? '' : this.state.CommentText}
            onChangeText={(text) => this.setState({ CommentText: text })}
            placeholder="Write a comment"
            style={commentInput}>
        </TextInput>
        <TouchableOpacity onPress={() => {this.props.Comment(this.state.CommentText, this.props.data.item.post_id)
        this.getSinglePost(this.props.data.item.post_id);
        }}>
            {
                this.props.CommentPostLoading
                    ?
                    <ActivityIndicator size="small" color="grey" />
                    :
                    <Econ name="paper-plane" size={18} color="grey" />
            }
        </TouchableOpacity>
    </View>
</View>
 <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.1)' }}></View>

               <View style={{paddingBottom:7}}>

               
<FlatList
                contentContainerStyle={{paddingBottom:10}}
             
                 
                  data={this.state.commentData}
                  renderItem={({ item }) => {
                    
                    if (item) {
                      return <CommentSection
                        data={item}
                        access_Token={this.props.access_Token}
                        isDeletHide={true}
                        Delete={(id) => this.DeleteComment(id)} />
                    }
                    else return null
                  }}
                 
        extraData={this.state.commentData}
                />

</View>

               
                {
                    this.state.showOptions
                        ?
                        <View
                            style={{
                                width: 120,
                                borderRadius: 5,
                                elevation: 10,
                                paddingHorizontal: width(4), paddingVertical: height(0.5),
                                position: 'absolute', backgroundColor: 'white', right: 20, top: 50
                            }}>
                            {NavigationConstants.user_id==this.props.data.item.user_id? <TouchableOpacity onPress={() => this.props.DeletePost(this.props.data.item.post_id)}>
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold',marginVertical: height(1) }}>Delete post</Text>
                            </TouchableOpacity>:null}
                           
                            {NavigationConstants.user_id==this.props.data.item.user_id? <TouchableOpacity 
                                 onPress={() => PubSub.publish('GoToEdit', { data: this.props.data.item.post_id })}
                            >
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', marginVertical: height(1) }}>Edit post</Text>
                            </TouchableOpacity>:null}
                           
                            {NavigationConstants.user_id!=this.props.data.item.user_id? <TouchableOpacity 
                                 onPress={() => this.props.OpenReport(this.props.data.item.post_id,this.props.data.item.user_id,this.props.data.item.username,this.props.data.item.user_data.avatar)}
                            >
                                <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', marginVertical: height(1) }}>Report</Text>
                            </TouchableOpacity>:null}
                           
                        </View>
                        :
                        null
                }
            </View>
        );
    }
}
