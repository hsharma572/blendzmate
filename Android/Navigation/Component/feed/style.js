import Colors from '../../../assets/Colors/Colors';

const Styles = {
    instagramView:{
        width:'100%',
        //height:360,
        backgroundColor:Colors.white,
        marginTop:25,
        borderRadius:5,
        elevation:1
    },
    feedHeader:{
        height:60,
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:Colors.white,
        //padding: 30,
        //alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius:5,
        paddingHorizontal: 20,
    },
    header:{
        //backgroundColor:'grey',
        height: 30, 
        width: '100%', 
        //alignSelf: 'center', 
        flexDirection: 'row', 
        justifyContent:'space-between',
        alignItems: 'center'
    },
    headerText:{
        fontFamily: 'Roboto-Bold', 
        fontSize: 13, 
        marginLeft: 15
    },
    timeHeader:{
        //marginLeft: 150, 
        fontFamily: 'Roboto-Bold', 
        fontSize: 8, 
        color: 'grey',
        marginLeft: 15,

    },
    commentView:{
        height: 60, paddingHorizontal: 15, justifyContent: 'space-between', flexDirection: 'row'
    },
    commentInput:{
        fontFamily: 'OpenSans-Regular', fontSize: 11, marginLeft: 15, height: 35, width: 240
    },
    LikeText:{
        fontSize: 14, fontFamily: 'OpenSans-Regular', color: 'grey', paddingHorizontal:5
    },
    LikeView:{
        justifyContent: 'center', alignItems: 'flex-start', 
    }
}

export default Styles;