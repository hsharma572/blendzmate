import React,{Component} from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import MainScreen from '../Screens/MainScreen/MainScreen';
import Notifications from '../Screens/Notifications/Notifications';
import CommentScreen from '../Screens/CommentScreen/CommentScreen.1';
import VideoPlayer from '../VideoPlayer';
import Login from '../Screens/Login/Login';
import SignUp from '../Screens/SignUp/SignUp';
import AddPhoto from '../Screens/AddPhoto/AddPhoto';
import LookingFor from '../Screens/LookingFor/LookingFor';
import Locality from '../Screens/Locality/Locality';
import ForgotPassword from '../Screens/ForgotPassword/ForgotPassword';
import AsyncStorage from '@react-native-community/async-storage';
import {Constants} from '../Component/Contants';
import AllSuggestion from '../Screens/AllSuggestion/AllSuggestion';
import StoryDetail from '../Screens/StoryDetail/StoryDetail';
import ListingDetails from '../Screens/ListingDetails/ListingDetails';
import EditPost from '../Screens/EditPost/EditPost';

const loginStack = createStackNavigator({
    Login:Login,
    SignUp:SignUp,
    AddPhoto:AddPhoto, 
    Locality:Locality,
    LookingFor:LookingFor,
    ForgotPassword:ForgotPassword,
    ListingDetails:ListingDetails
},{
    headerMode:'none',
    initialRouteName:'Login'
})

const AppStack = createStackNavigator({
    MainScreen: MainScreen,
    Notifications:Notifications,
    CommentScreen:CommentScreen,
    EditPost:EditPost,
    AllSuggestion:AllSuggestion,
    StoryDetail:StoryDetail,
    Video:VideoPlayer,
}, {
    headerMode: 'none',
    initialRouteName:'MainScreen'
})

const MainStack = createSwitchNavigator({
    loginStack:loginStack,
    AppStack:AppStack
})

export default createAppContainer(MainStack);