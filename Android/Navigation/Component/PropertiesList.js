import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native';

export default class PropertiesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <TouchableOpacity
      onPress={()=>this.props.navigation.navigate('Details')}
      activeOpacity={1} 
      style={{height:220,width:'100%',marginBottom:15}}>
        <ImageBackground source={require('../../assets/Images/placeholder.jpg')} style={{height:'100%',width:'100%'}}/>
      </TouchableOpacity>
    );
  }
}
