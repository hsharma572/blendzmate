import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import Styles from './Styles';

export default class ChatUserList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { MainView, UserTitle , User, LastMsgText} = Styles;
        return (
            <TouchableOpacity style={MainView} onPress={()=>this.props.OpenWebView(this.props.data.item.username)}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <ImageBackground
                        imageStyle={{ borderRadius: 25 }}
                        source={{ uri: this.props.data.item.avatar }}
                        style={{ height: 45, width: 45 }}
                    />
                    <View style={UserTitle}>
                        <Text style={User}>{this.props.data.item.username}</Text>
                        <Text style={LastMsgText}>{this.props.data.item.last_message}</Text>
                    </View>
                </View>
                <View style={{ height: 40, paddingTop: 5 }}>
                    <Text style={{ fontSize: 10, fontFamily: 'OpenSans-Regular', color:'rgba(0,0,0,0.4)' }}> {this.props.data.item.time_text} </Text>
                </View>
            </TouchableOpacity>
        );
    }
}
