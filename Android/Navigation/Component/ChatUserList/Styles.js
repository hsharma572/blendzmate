const Styles = {
    MainView:{
        height:70,
         flexDirection:'row',alignItems:'center',padding: 10,borderBottomWidth: 1,borderBottomColor: 'rgba(0,0,0,0.1)',
         justifyContent:'space-between',
         //backgroundColor:'grey'
    },
    UserTitle:{
        height:40,width:185,justifyContent:'space-between', paddingHorizontal:10, 
        //backgroundColor:'green'
    },
    User:{
        fontSize: 15, fontFamily: 'Roboto-Regular', color: 'black' 
    },
    LastMsgText:{
        fontSize: 12, fontFamily: 'Roboto-Regular', color: 'rgba(0,0,0,0.6)'
    }
}

export default Styles;