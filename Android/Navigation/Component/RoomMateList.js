import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Colors from '../../assets/Colors/Colors';

export default class RoomMateList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => this.props.Selected(this.props.data.item.id, this.props.data.item.roomate)}
                style={{
                    height: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                    borderWidth: 1,
                    borderRadius: 15,
                    marginRight: 10,
                    borderColor: this.props.data.item.id === this.props.SelectedRoomate && this.props.SelectedRoomateColor ? Colors.ActiveTintColor : 'rgba(0,0,0,0.1)'
                }}>
                <Text 
                style={{ 
                    fontSize: 11, 
                    color: this.props.data.item.id === this.props.SelectedRoomate && this.props.SelectedRoomateColor ? Colors.ActiveTintColor : 'rgba(0,0,0,0.5)', 
                    fontFamily: 'Roboto-Bold' }}>
                        {this.props.data.item.roomate}
                </Text>
            </TouchableOpacity>
        );
    }
}
