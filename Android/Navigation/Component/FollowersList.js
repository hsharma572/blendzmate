import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

export default class FollowersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View
                style={{
                    height: 60, width: '100%',
                    backgroundColor: 'white', flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center', paddingHorizontal: 15, paddingVertical: 10
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={{ uri: this.props.data.item.avatar }} style={{ height: 50, width: 50, borderRadius: 25 }} />
                    <Text style={{ fontSize: 13, marginLeft: 10, fontFamily: 'OpenSans-Bold', color: "#333" }}>{this.props.data.item.username}</Text>
                </View>

                <TouchableOpacity
                    onPress={() => this.props.Follow(this.props.data.item.user_id)}
                    style={{ height: 30, width: 90, backgroundColor: 'red', borderRadius: 15, justifyContent: 'center', alignItems: 'center' }}>
                    {
                        this.props.isSpinner && this.props.id == this.props.data.item.user_id
                            ?
                            <ActivityIndicator size='small' color='white' />
                            :
                            <Text style={{ fontSize: 11, fontFamily: 'OpenSans-Bold', color: "#FFF" }}>{this.props.isFollower ? 'Follow' : 'Following'}</Text>
                    }
                </TouchableOpacity>
            </View>
        );
    }
}
