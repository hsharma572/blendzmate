import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native';
import Econ from 'react-native-vector-icons/FontAwesome5';

export default class BlockedList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{
                paddingVertical: 5, width: '100%', justifyContent: 'space-between',
                flexDirection: 'row', alignItems: 'center', paddingHorizontal: 5
            }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <ImageBackground
                        imageStyle={{ borderRadius: 15 }}
                        source={{uri:this.props.data.item.avatar}}
                        style={{ height: 30, width: 30, backgroundColor:'transparent' }}
                    />
                    <Text style={{ fontSize: 13, fontFamily: 'OpenSans-Regular', marginLeft: 10 }}>{this.props.data.item.username}</Text>
                </View>
                <TouchableOpacity 
                style={{height:25,width:60,borderRadius:5,backgroundColor:'white',elevation:3,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:9,fontFamily:'OpenSans-Regular'}}>Unblock</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
