import React, {Fragment,Component} from 'react';
import {
  View, Platform,PermissionsAndroid
} from 'react-native';
import MainStack from './Navigation/Component/MainStack';
import OneSignal from 'react-native-onesignal';
import {NavigationConstants} from './Navigation/commonValues';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Geolocation from 'react-native-geolocation-service';
export default class App extends Component {

  constructor(properties) {
      super(properties);
      OneSignal.init("336520a1-ea9c-4ba3-bed8-8295c3db00e8", {kOSSettingsKeyAutoPrompt : true});// set kOSSettingsKeyAutoPrompt to false prompting manually on iOS
      OneSignal.setLogLevel(7, 0);
      OneSignal.setLocationShared(true);
      OneSignal.inFocusDisplaying(2)
      OneSignal.addEventListener('received', this.onReceived);
      OneSignal.addEventListener('opened', this.onOpened);
      OneSignal.addEventListener('ids', this.onIds);
    }
  
    componentWillUnmount() {
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened);
      OneSignal.removeEventListener('ids', this.onIds);
    }
  
    onReceived(notification) {
      console.log("Notification received: ", notification);
    }
  
    onOpened(openResult) {
      console.log("Notification openResult: ", openResult);
    }
  
    onIds(device) {
      console.log('Device info: ', device);
      NavigationConstants.onesingalToken=device.pushToken;
      NavigationConstants.onesingalUserid=device.userId;

    }
    hasLocationPermission = async () => {
      if (Platform.OS === 'ios' || (Platform.OS === 'android' && Platform.Version < 23)) {
        return true;
      }
  
      const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  
      if (hasPermission) return true;
  
      const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  
      if (status === PermissionsAndroid.RESULTS.GRANTED) return true;
  
      if (status === PermissionsAndroid.RESULTS.DENIED) {
        alert('Location permission denied by user.');
      } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        alert('Location permission revoked by user.');
      }
      return false;
    };
  
    async componentDidMount(){
      const hasLocationPermission = await this.hasLocationPermission();
      if (hasLocationPermission) {
        Geolocation.getCurrentPosition(
          (position) => {
            
            console.log('lat2', position);
            NavigationConstants.lat=position.coords.latitude;
            NavigationConstants.long=position.coords.longitude;
            NavigationConstants.locationLoaded=true;
          },
          (error) => {
           
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      }
    }
    render(){
      return (
        <View style={{flex:1}}>
          {
            Platform.OS=='ios'?(
              <View style={{height:getStatusBarHeight(),backgroundColor:'white'}}>

              </View>
            ):null
          }
          <MainStack/>
        </View>
      );
    }
  }