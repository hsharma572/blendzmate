<?php
$res = $result = array();
require( './html.php' );
$url = "https://www.youtube.com/watch?v=".$_GET['id'];
//echo "<iframe src='$url'></iframe>";die;
$html  = file_get_contents($url);
$html = str_get_html( $html );
$spans = $html->find( '.related-list-item' );
$i = 0;
$res['suggestions'] = array();
$title = $html->find('#watch-headline-title #eow-title',0);
$res['title'] = html_entity_decode($title->innertext);
$thumb = $html->find('link[itemprop="thumbnailUrl"]',0);
$res['thumbnail_url'] = isset($thumb->href)?$thumb->href:"";
$desc = $html->getElementById('#watch-description-text');
$count_box = $html->getElementById('#watch8-sentiment-actions')->find('.like-button-renderer',0);
$count_box1 = !is_null($count_box)?$count_box->find('.yt-uix-button-content'):"";
if(!is_null($count_box1)){
$res['like'] = $count_box->find('.yt-uix-button-content',2);	
$res['like'] = $res['like']->innertext;	
$res['dislike'] = $count_box->find('.yt-uix-button-content',3)->innertext;	
}
$res['desc'] = isset($desc->innertext)?$desc->innertext:(isset($desc->plaintext)?$desc->plaintext:"");
$publish = $html->find('.watch-time-text',0);
$res['publish'] = isset($publish->innertext)?$publish->innertext:(isset($publish->plaintext)?$publish->plaintext:"");
$views = $html->find('.watch-view-count',0);
$res['views'] = isset($views->innertext)?$views->innertext:"";
foreach ( $spans as $li ) {
	$div = $li->find( '.content-wrapper', 0 );
	if ( !is_null( $div ) ) {
		$link = $div->find( 'a', 0 );
		$result[ 'url' ] = $link = $link->href;
		if(stripos($link,'watch?v')!==false&&stripos($link,'list')===false){
	$result['id']= str_replace('/watch?v=','',$link);
		if ( isset( $link->title ) && !empty( $link->title ) ) {
			$result[ 'title' ] = html_entity_decode($link->title);
		} else {
			$result[ 'title' ] = $div->find( 'span', 0 );
			$result[ 'title' ] = html_entity_decode($result[ 'title' ]->innertext);
		}
		$img = $div = $li->find( '.thumb-wrapper', 0 )->find( 'img', 0 );
		$result[ 'thumb' ] = $img->{"data-thumb"} != "" ? $img->{"data-thumb"} : $img->{"src"};
		$duration = $div->find( '.accessible-description', 0 );
		if ( isset( $duration->plaintext ) ) {
			$duration = $duration->plaintext;
			$result[ 'duration' ] = str_replace( '- Duration: ', "", $duration );
		}
		$meta = $div->find( '.view-count', 0 );
		if ( !is_null( $meta ) ) {
			$result[ 'views' ] = isset( $meta->plaintext ) ? $meta->plaintext : "";
			//	echo $times_ago,"<br>",$views,"<br>";
		}
	}
	array_push( $res['suggestions'], $result );
}}
echo json_encode( $res );
//echo $next->href,"<br>";
?>