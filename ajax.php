<?php
require_once('./sys/init.php');

$app      = (!empty($_GET['app'])) ? $_GET['app'] : '';
$action   = (!empty($_GET['a'])) ? $_GET['a'] : '';
$rhandler = "xhr/$app.php";
$data     = array();
$root     = __DIR__;
$hash     = (!empty($_GET['hash'])) ? $_GET['hash'] : '';
if (empty($hash)) {
    $hash = (!empty($_POST['hash'])) ? $_POST['hash'] : '';
}
$hash ="hshshs";
define('ROOT', $root);
header("Content-type: application/json");
 if (!file_exists($rhandler)) {
    $data = array(
        'status'   => '404',
        'message'  => 'Not Found'
    );

    echo json_encode($data, JSON_PRETTY_PRINT);
	exit();
}

else {	
	require_once($rhandler);
	echo json_encode($data, JSON_PRETTY_PRINT);
	$db->disconnect();
	unset($context);
	exit();
}
?>