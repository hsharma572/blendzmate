<?php
if (IS_LOGGED !== true) {
	header("Location: $site_url/welcome");
	exit;
}
$context['posts']  = array();
$posts             = new Posts();
$tag    = "deals";

$posts->limit    = 30;

$tag_id = $posts->getHtagId($tag);

$tag_id = ((is_numeric($tag_id)) ? $tag_id : 0);
$qrset  = array();

if (!empty($tag_id)) {
	$qrset = $posts->exploredeals($tag_id);
}

$qrset  = (!empty($qrset) && is_array($qrset) || 2) ? o2array($qrset) : array();
$tcount = (!empty($qrset)) ? $posts->countPostsByTag_loc($tag_id) : 0;

$follow = (!empty($follow) && is_array($follow)) ? o2array($follow) : array();

$context['page_link'] = 'deals';
$context['exjs'] = true;
$context['app_name'] = 'deals';
$context['posts'] = $qrset;
$context['page_title'] = $context['lang']['deal_posts'];
$context['follow'] = $follow;
$context['total_count'] = $tcount;
$context['content'] = $pixelphoto->PX_LoadPage('deals/templates/deals/index');
