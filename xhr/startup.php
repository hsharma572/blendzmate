<?php
//print_r($me);die;
if ( $action == 'startup_image' ) {
	$data[ 'status' ] = 400;
	if ( !empty( $_FILES[ 'photo' ] ) && file_exists( $_FILES[ 'photo' ][ 'tmp_name' ] ) ) {
		$media = new Media();
		$me[ 'user_id' ] = !empty($_POST['user_update_id'])?$_POST['user_update_id']:$me[ 'user_id' ];
		$media->setFile( array(
			'file' => $_FILES[ 'photo' ][ 'tmp_name' ],
			'name' => $_FILES[ 'photo' ][ 'name' ],
			'size' => $_FILES[ 'photo' ][ 'size' ],
			'type' => $_FILES[ 'photo' ][ 'type' ],
			'allowed' => 'jpeg,jpg,png',
			'crop' => array(
				'height' => 150,
				'width' => 150,
			),
			'avatar' => true
		) );

		$upload = $media->uploadFile();

		if ( !empty( $upload ) ) {
			$photo = $upload[ 'filename' ];
			$data[ 'status' ] = 200;
			$data[ 'photo' ] = Media::getMedia( $photo );

			$user->updateStatic( $me[ 'user_id' ], array(
				'avatar' => $photo,
				'startup_avatar' => 1
			) );
		}
	}
}
if ( $action == 'startup_info' ) {
	$data[ 'status' ] = 400;
	if ( empty( $_POST[ 'country' ] ) && empty( $_POST[ 'fname' ] ) && empty( $_POST[ 'lname' ] ) && empty( $_POST[ 'gender' ] ) && empty( $_POST[ 'dob' ] ) && empty( $_POST[ 'bio' ] ) ) {
		$data[ 'message' ] = lang( 'please_check_details' );
	} elseif ( !empty( $_POST[ 'bio' ] ) && len( $_POST[ 'bio' ] ) > 1000 ) {
		$data[ 'message' ] = "Bio Too Long";
	}
	else {
		if ( !empty( $_POST[ 'country' ] ) && in_array( $_POST[ 'country' ], array_keys( $countries_name ) ) ) {
			$up_data[ 'country_id' ] = Generic::secure( $_POST[ 'country' ] );
		}
		if ( !empty( $_POST[ 'dob' ] ) ) {
			$up_data[ 'dob' ] = Generic::secure( $_POST[ 'dob' ] );
		}
		if ( !empty( $_POST[ 'gender' ] ) ) {
			$up_data[ 'gender' ] = Generic::secure( $_POST[ 'gender' ] );
		}
		if ( !empty( $_POST[ 'phone' ] ) ) {
			$up_data[ 'phone' ] = Generic::secure( $_POST[ 'phone' ] );
		}
		if ( isset( $_POST[ 'workplace' ] ) && !empty( $_POST[ 'workplace' ] ) ) {
			$up_data[ 'workplace' ] = Generic::secure( $_POST[ 'workplace' ] );
		}
		if ( isset( $_POST[ 'work' ] ) && !empty( $_POST[ 'work' ] ) ) {
			$up_data[ 'work' ] = Generic::secure( $_POST[ 'work' ] );
		}
		if ( !empty( $_POST[ 'bio' ] ) ) {
			$up_data[ 'bio' ] = Generic::secure( $_POST[ 'bio' ] );
		}
		if ( !empty( $_POST[ 'fname' ] ) ) {
			$up_data[ 'fname' ] = Generic::secure( $_POST[ 'fname' ] );
		}
		if ( !empty( $_POST[ 'lname' ] ) ) {
			$up_data[ 'lname' ] = Generic::secure( $_POST[ 'lname' ] );
		}
		$up_data[ 'startup_info' ] = 1;
		$me[ 'user_id' ] = !empty($_POST['user_update_id'])?$_POST['user_update_id']:$me[ 'user_id' ];
		try {
			$user->updateStatic( $me[ 'user_id' ], $up_data );
		} catch ( Exception $e ) {
			//			echo $e; die;
		}
		//		echo mysqli_error(self::$db),"hshs";die;
		$data[ 'status' ] = 200;
	}
}
if ( $action == 'startup_follow' ) {
	$data[ 'status' ] = 400;
	$ids = explode( ',', $_POST[ 'ids' ] );
	if ( !empty( $_POST[ 'ids' ] ) && is_array( $ids ) ) {
		foreach ( $ids as $key => $id ) {
			if ( !empty( $id ) && is_numeric( $id ) ) {
				$follower_id = $me[ 'user_id' ];
				$following_id = Generic::secure( $id );
				$notif = new Notifications();
				$user->setUserById( $follower_id );
				$status = $user->follow( $following_id );
				$data[ 'status' ] = 400;
				if ( $status === 1 ) {
					$data[ 'status' ] = 200;
					$data[ 'code' ] = 1;

					#Notify post owner
					$notif_conf = $notif->notifSettings( $following_id, 'on_follow' );
					if ( $notif_conf ) {
						$re_data = array(
							'notifier_id' => $me[ 'user_id' ],
							'recipient_id' => $following_id,
							'type' => 'followed_u',
							'url' => un2url( $me[ 'username' ] ),
							'time' => time()
						);

						$notif->notify( $re_data );
					}
				}
			}
		}
		$user->updateStatic( $me[ 'user_id' ], array(
			'startup_follow' => 1
		) );
		$data[ 'status' ] = 200;
	}
}
if ( $action == "verify" ) {
	$data[ 'status' ] = 400;
	if ( $_POST[ 'otp' ] == $_SESSION[ 'otp' ] ) {
		unset( $_SESSION[ 'otp' ] );
		$user->updateStatic( $_SESSION[ 'user_data' ], array(
			'active' => 1
		) );
		$data[ 'status' ] = 200;
	}
}
if ( $action == "verify-mobile" ) {
	
	$data[ 'status' ] = 400;
	$hash = $_POST[ 'hashed' ];
	$timestamp = $_POST[ 'time' ];
	$otp = $_POST[ 'otp' ];
//	print_r($_POST);
	if ( $hash != md5($timestamp.$otp) ) {
		$data[ 'message' ] = "OTP incorrect";
	} else {
		$data[ 'status' ] = 200;
		$user->updateStatic( $_SESSION[ 'user_data' ], array(
			'active' => 1
		) );
	}
}
if ( $action == 'skip' ) {
	if ( $me[ 'startup_avatar' ] == 0 ) {
		$user->updateStatic( $me[ 'user_id' ], array(
			'startup_avatar' => 1
		) );
		$data[ 'status' ] = 200;
	} elseif ( $me[ 'startup_info' ] == 0 ) {
		$user->updateStatic( $me[ 'user_id' ], array(
			'startup_info' => 1
		) );
		$data[ 'status' ] = 200;
	}
	elseif ( $me[ 'startup_follow' ] == 0 ) {
		$user->updateStatic( $me[ 'user_id' ], array(
			'startup_follow' => 1
		) );
		$data[ 'status' ] = 200;
	}
	$data[ 'status' ] = 200;
}